-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 27 nov. 2020 à 11:41
-- Version du serveur :  8.0.22-0ubuntu0.20.04.2
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `geasscard`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

CREATE TABLE `abonnement` (
  `id` int NOT NULL,
  `code_abonnement` varchar(255) NOT NULL,
  `code_client` varchar(255) NOT NULL,
  `type_abonnement` varchar(255) NOT NULL,
  `montant` double NOT NULL DEFAULT '0',
  `reduction` double NOT NULL DEFAULT '0',
  `etat_abonnement` int NOT NULL DEFAULT '0',
  `chemin_facture` text NOT NULL,
  `duree` int NOT NULL DEFAULT '0',
  `date_debut` varchar(255) NOT NULL,
  `date_fin` varchar(255) NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `attente_validation`
--

CREATE TABLE `attente_validation` (
  `id` int NOT NULL,
  `code_av` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hachar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(2555) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `attente_validation`
--

INSERT INTO `attente_validation` (`id`, `code_av`, `code_user`, `hachar`, `status`, `date_register`) VALUES
(5, 'av-2612', 'client-1912', 'fe65365d4f68a4d69d6261272d816a5da7051722a3488f2686d21d1b6913efee2d362afba0096dd5417c7f5c8bd32005ed835d19325e952c99130d14900dbcec', 'noclient', '2019-12-28 13:45:28'),
(7, 'av-3690', 'client-4115', '4a6e6750db90af2404b504db6b575644c7f18ceb6653a8a212bfb3326e370758b12d1fc634d650d2028d1d60277d10af1bb79487f5dfa77afe2c4addd5df1f82', 'noclient', '2019-12-28 13:49:57'),
(8, 'av-3111', 'client-2393', '6489d3783a00af4e9f4ebe8cceffc7e4126373aa43de293e47eb3f4e55ec1b89f13d8bb2b4d794a3a573fabdd97638994087c459872c44db59cd86a55bc7cb75', 'noclient', '2019-12-28 13:53:21'),
(9, 'av-8129', 'client-4864', 'aa939c13c9bf9c5c4fea056f0de60abf3473f4c979ed6984e797422d633a5b4858e587fd02e51a4b4b385c99c17e735d30c40d389fb400687511631f9856827a', 'noclient', '2020-01-07 23:25:17'),
(10, 'av-6635', 'client-4325', '4ba7e023d7f4d544ddb43aca46ed112bd5e760f9937cee519d2fbb768a581b1dd3ad3c9a9e05c881a1e8df6e9fdddaa5806eccdff1efdc3886e8cbfddcb11f6b', 'noclient', '2020-03-22 19:05:29'),
(11, 'av-2617', 'client-2296', 'cdf1592e3d5a9dc62d8433a462c36a154005da3e14669b5055c135b5d620076d8f6dd93c366649620171b05324806db06f8120416196dd9f3768942130418819', 'noclient', '2020-03-22 19:11:41'),
(15, 'av-6317', 'client-4530', 'febdf6bb4094c6293f0f3f3aa547560069efa3886c593127c825598358431cd7898f4cfeb33ca94355d0becf11c80857d41c28c40b5f6eeba314d5cef0e5f7a9', 'noclient', '2020-08-25 22:23:34'),
(18, 'av-7687', 'client-2541', '57f86fd915cb2776fdd15a88dd8dbca741d9c9178bc21f516123218984cc5ab1bcefc557590f18cbca66ac807d09e81ff355bec1a0cf2cccc4034eb6062fbdfa', 'noclient', '2020-09-13 09:32:10'),
(19, 'av-9945', 'client-7592', 'dbaa97e818641a03181ea3a47f0792e2bb6735d46b8b3d2da57ae957bc9f58edd8608bef1d8b2b42455221f997661fcf4d928cc503e4dcd3b55843e6435657ff', 'noclient', '2020-09-13 09:34:38'),
(20, 'av-2984', 'client-4894', '298166789d4710f897bb20c53c40e0d011b23f2a90fba77f138a349abb3e84d6e94ae57a7669bff6df889a3f925b9ee25c72df4296dd75d394456623ae40aebe', 'noclient', '2020-09-13 09:35:56'),
(21, 'av-7599', 'client-6930', '87d58be9435c49933564996be119dab27dd95f010cbe64b5399ed910f221814463b49e34a32e2d70a4eef846ed0b911335055f9b8288f470dbb114ea1c058fcc', 'noclient', '2020-09-13 09:47:58'),
(22, 'av-5843', 'client-1372', '5f02610a04c43ef4e2bbd5b9641322a842b3c0769e5a40dd81410d8c6a44891431f56dce3254133e3c2b298c7e0583598b002dd9e8966150fa89a4c2cc1f348e', 'noclient', '2020-09-13 09:50:41'),
(23, 'av-1455', 'client-5032', 'e1270198bfbef6c43dc9d182d5f49acb6c714d8640feee992beb2806f5726c93b629ff631bffc47dd4b346b310b2aaed0b1c74ea387b1a549967c5fa1c91b137', 'noclient', '2020-09-13 09:52:45'),
(24, 'av-9302', 'client-3946', 'eb4a1dfb54a22c3fc682619eadbfba34edc99139878fc2936eb20382aebb86f2eab66fa510ab89831888811d8a85e4bb64374b7b9cae2a5269fe2b339abc742b', 'noclient', '2020-09-13 10:01:52'),
(27, 'av-3484', 'client-8037', '71a71eb894b718f6bf0410a58c71b2f143c981668946844a27343617a43e6cc67629439dff26d6e55f34fb65d3d32bff2b85be9432e9debba118e6283ef58525', 'noclient', '2020-09-13 20:19:38'),
(28, 'av-9263', 'client-3011', 'a27dda131463b376fdc3b381d49ede811b89450ed71a07b252c7f8bdf2828af5c9ee8021844304f2319d2f3a68a5c2124f432b1d6c3f5b6c5541df8257e14d43', 'noclient', '2020-09-13 20:22:30'),
(35, 'av-5841', 'client-3231', 'c4f3c36aace89968b9fac5a5af6c82c6c617f9cd073b8144987c09a4cbcb4e7b8d16bc01572c86a3250c74a34991a24a6a7be0f59cafcce3a5907fd0c4d0bea5', 'noclient', '2020-11-27 11:01:25');

-- --------------------------------------------------------

--
-- Structure de la table `bon_de_reduction`
--

CREATE TABLE `bon_de_reduction` (
  `id` int NOT NULL,
  `code_bdr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_reduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `somme_reduction` double NOT NULL,
  `active_reduction` int NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `bon_de_reduction`
--

INSERT INTO `bon_de_reduction` (`id`, `code_bdr`, `code_reduction`, `somme_reduction`, `active_reduction`, `date_register`) VALUES
(2, 'BonDeReduction-21484', 'JANVIER2019', 10000, 1, '2020-01-02 15:14:56'),
(4, 'BonDeReduction-13104', 'test', 5000, 0, '2020-01-02 18:09:18'),
(5, 'BonDeReduction-33472', 'wayou', 2500, 1, '2020-02-15 23:39:18');

-- --------------------------------------------------------

--
-- Structure de la table `citation`
--

CREATE TABLE `citation` (
  `id` int NOT NULL,
  `code_citation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_naissance` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` int NOT NULL,
  `phone2` int NOT NULL,
  `phone3` int NOT NULL,
  `site_internet` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `auto_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_vcard` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_template` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `est_boss` int NOT NULL DEFAULT '0',
  `hashcc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `code_client`, `nom`, `prenom`, `sexe`, `date_naissance`, `email`, `adresse`, `phone1`, `phone2`, `phone3`, `site_internet`, `photo`, `auto_description`, `lien_card`, `lien_vcard`, `lien_template`, `est_boss`, `hashcc`, `date_update`, `date_register`) VALUES
(36, 'client-4981', 'MEDI', 'YANN MICHAEL', 'masculin', '2001-05-08', 'hyperformix3@gmail.com', 'Abidjan , Cote d’ivoire', 62397167, 0, 0, '', '', 'Gars Très Cool', 'http://127.0.1.1:3000/portfolio/1c90efc5cf0b53eba33f666ca027496f5f52a08256a67aaa5dfc9026c3bd0441a4dec8e194644d798f254ce7e916971093ef1c9b9367dab67d928ac04604f847', '', 'light 6', 0, '1c90efc5cf0b53eba33f666ca027496f5f52a08256a67aaa5dfc9026c3bd0441a4dec8e194644d798f254ce7e916971093ef1c9b9367dab67d928ac04604f847', '2020-11-12 14:17:11', '2020-11-12 14:17:11'),
(37, 'client-2630', 'Kouassi', 'Nanan', 'masculin', '', 'nanankouassi9@gmail.com', '', 0, 0, 0, '', '', '', 'http://127.0.1.1:3000/portfolio/c757fdef0c1e627213aa1a83bf3c158f8dd654012a4b1caef1af1e5569706ae27aa2e27ad9f83c6aa8ef4c828b8b43f3b66a0d4cc095ad5ecb4094f3a4c3722f', '', 'light 1', 0, 'c757fdef0c1e627213aa1a83bf3c158f8dd654012a4b1caef1af1e5569706ae27aa2e27ad9f83c6aa8ef4c828b8b43f3b66a0d4cc095ad5ecb4094f3a4c3722f', '2020-11-12 14:31:39', '2020-11-12 14:31:39');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int NOT NULL,
  `code_commande` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_panier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cout_total` double NOT NULL,
  `statut` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL,
  `code_presta` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `moyen_paiement` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmation_client` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `code_commande`, `code_client`, `code_panier`, `cout_total`, `statut`, `date_register`, `code_presta`, `moyen_paiement`, `confirmation_client`) VALUES
(66, 'client-1724-commande-7348244', 'client-1724', 'commande-panier-5712726', 8000, '4', '2020-02-28 21:04:29', 'Aucun Prestataire Choisi', 'À La Livraison', 1),
(67, 'client-1724-commande-8091711', 'client-1724', 'commande-panier-2748533', 29250, '1', '2020-02-28 22:07:55', 'Aucun Prestataire Choisi', 'À La Livraison', 1),
(69, 'client-1724-commande-6573947', 'client-1724', 'commande-panier-8980268', 8250, '1', '2020-10-16 01:39:47', 'Aucun Prestataire Choisi', 'À La Livraison', 1),
(73, 'client-4981-commande-1988153', 'client-4981', 'commande-panier-40113115265', 19750, '2', '2020-11-24 13:15:50', 'Aucun Prestataire Choisi', 'À La Livraison', 1);

-- --------------------------------------------------------

--
-- Structure de la table `contact_client`
--

CREATE TABLE `contact_client` (
  `id` int NOT NULL,
  `code_contact` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int NOT NULL,
  `commentaire` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contact_client`
--

INSERT INTO `contact_client` (`id`, `code_contact`, `code_client`, `nom`, `prenom`, `email`, `phone`, `commentaire`, `date_register`) VALUES
(1, 'ContactClient-6211219', 'client-5195', 'az', 'ze', 'yannmicke@gmail.com', 0, '', '2019-10-17 22:41:46'),
(2, 'ContactClient-7691073', 'client-5195', 'YANN', 'MEDI', '', 67623971, '', '2019-10-17 22:42:10'),
(3, 'ContactClient-9680672', 'client-1921', 'Suini', 'Koffi Adammo Olive', 'suiniolive58@gmail.com', 58048155, '', '2019-11-07 11:51:49'),
(4, 'ContactClient-6831611', 'client-4787', 'Kouassi', 'Ckeick', 'ti.dev99@gmail.com', 47372856, '', '2019-11-10 01:41:06'),
(5, 'ContactClient-7394204', 'client-6042', 'BAKAYOKO', 'OLIVIA', '', 51148600, '', '2019-12-16 17:09:05'),
(6, 'ContactClient-8843568', 'client-8785', 'Beda ', 'Christ', '', 40654980, '', '2019-12-28 14:00:44'),
(9, 'ContactClient-2615447', 'client-1724', 'YANN', 'MEDI', 'hyperformix3@gmail.com', 0, '', '2020-08-25 15:01:28'),
(10, 'ContactClient-7253383', 'client-1724', 'YANN', 'MEDI', 'hyperformix3@gmail.com', 0, '', '2020-08-25 15:01:28'),
(11, 'ContactClient-6229886', 'client-1724', 'Gbama', 'Wilfried', 'gbamawilfrief@gmail.com', 0, '', '2020-10-14 11:28:23'),
(12, 'ContactClient-2937064', 'client-4981', 'Michael', 'Medi', 'yannmicke@gmail.com', 0, '', '2020-11-20 09:32:06');

-- --------------------------------------------------------

--
-- Structure de la table `cursus`
--

CREATE TABLE `cursus` (
  `id` int NOT NULL,
  `code_cursus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_debut` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_fin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `diplome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `divers`
--

CREATE TABLE `divers` (
  `id` int NOT NULL,
  `code_divers` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_etudiant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `passion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hobbie` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lv1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lv2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lv3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `association` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int NOT NULL,
  `code_etudiant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

CREATE TABLE `experience` (
  `id` int NOT NULL,
  `code_experience` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_etudiant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_debut` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_fin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `filleul`
--

CREATE TABLE `filleul` (
  `id` int NOT NULL,
  `code_filleul` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_parrain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `filleul`
--

INSERT INTO `filleul` (`id`, `code_filleul`, `code_parrain`, `email`) VALUES
(6, 'client-4981-filleul-4836752', 'client-4981', 'yannmicke@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `info_fichier_client`
--

CREATE TABLE `info_fichier_client` (
  `id` int NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_photo_profil` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_cv` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_qr` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `info_fichier_client`
--

INSERT INTO `info_fichier_client` (`id`, `code_client`, `chemin_logo`, `chemin_photo_profil`, `chemin_cv`, `chemin_qr`) VALUES
(20, 'client-8785', '', '', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAAE0CAYAAACigc+fAAAAAklEQVR4AewaftIAABanSURBVO3BQY7cWhLAQFLo+1+Z42WuHiCoqu2vyQj7g7XWeoGLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeokfHlL5TRWTyknFicodFScqU8WkMlVMKlPFpDJVTCqfVHGi8kTFpDJVTConFf8lKlPFpPKbKiaV31TxxMVaa73ExVprvcTFWmu9xA8fVvFJKndUnKhMFZPKHSpTxUnFHSonKr9J5Y6KJ1ROKiaVqWJSmSomld9UcVLxL6n4JJVPulhrrZe4WGutl7hYa62X+OHLVO6o+KaKSeUOlaliUpkqTlSmikllqphUpopJ5aTiRGWqOFGZVKaKk4oTlUnlROVE5aTiROWkYlK5Q2WqmFSmikllqvgklTsqvulirbVe4mKttV7iYq21XuKH/7iKO1SmiidUTlSmiqniTSpOVKaKSeWJiknljopJ5Y6KSeWJiicqTlSmiv+yi7XWeomLtdZ6iYu11nqJH/7PVEwqU8WJyknFicodFScVJxWfpPJJKk9UnFTcoXJHxaQyVUwqd6icVEwqJxVvdrHWWi9xsdZaL3Gx1lov8cOXVXyTylQxqUwVT1RMKicqU8Wk8k0qn1QxqUwVd6g8oXJSMalMFScVJypTxSdVnKg8UfFExb/kYq21XuJirbVe4mKttV7ihw9TeROVqeKbKiaVqWJSmSpOKiaVqWJSeUJlqjipmFSmikllqphUpopJZaqYVKaKO1SmikllqphUpoqTiknlRGWqOFH5l12stdZLXKy11ktcrLXWS9gf/IepTBWTyh0VJyrfVHGHylRxojJVTCpTxYnKVHGHylRxojJVTConFZPKVPGEylRxh8onVfw/uVhrrZe4WGutl7hYa62X+OHLVO6omFSmit+kckfFicpUMamcVEwVk8pUMVXcoTJVnKj8TRVPqJxUTCpTxaRyR8UdKlPFpHJSMancUXGickfFExdrrfUSF2ut9RIXa631Ej98WcWkcqIyVUwqU8UdFXdUTCp3qDxRMan8TSonFXeonKicqEwVk8pUcVJxR8VJxR0qd1RMKlPFExUnKicVk8o3Xay11ktcrLXWS1ystdZL/PBhKlPFScWJyh0qT6hMFScVJxWTyhMVk8o3VdyhMlVMKicqJxWTyknFpPKEyknFpPJJFZPKHSpPqEwVT1R80sVaa73ExVprvcTFWmu9hP3BB6ncUXGHyh0Vk8odFScqd1RMKndUTCpTxYnKHRUnKicVJyonFXeoTBWfpHJScaJyUjGpnFR8kspUMancUTGpnFQ8cbHWWi9xsdZaL3Gx1lov8cM/RuWk4g6VqWJSmSpOVKaKO1SmikllqphUpopJ5Y6KSeWOijtUpopJ5URlqpgqJpWTiknlCZU7KiaVqeJE5Y6KSWWqmFSmijtUpopvulhrrZe4WGutl7hYa62XsD94QGWq+CaVqeJE5ZsqnlCZKu5QOamYVKaKSeWJihOVOyomlaliUjmp+E0qU8WkMlVMKlPFHSp3VNyhMlVMKndUPHGx1lovcbHWWi9xsdZaL/HDQxUnKlPFpHJHxYnKVDGpTBWTyjepnKhMFXdUTCpTxUnFEyonFZPKVPGbVD6pYqqYVE5UPqniROVE5aTijopvulhrrZe4WGutl7hYa62XsD94QGWqmFTuqDhRmSpOVJ6omFROKu5QmSomlZOKE5WTijtUpopJZaqYVKaKSeWOijtUpooTlaniCZXfVHGHylQxqZxUTCp3VDxxsdZaL3Gx1lovcbHWWi/xw4epfJLKVDGpnFTcoXJHxaTyhMpUcYfK31RxUjGpnFRMKpPKScVUcaLyhModFU+oTBWTyh0Vk8pJxR0V33Sx1lovcbHWWi9xsdZaL/HDQxWTylQxqdxRcUfFHSpTxRMVJyrfVHGiMqlMFXeoPFFxR8WJyh0qU8WkMqncUXGi8kTFpDJVTCpTxUnFicpU8TddrLXWS1ystdZLXKy11kv88JDKicpUMamcqJxUnKicVNxRcaIyVUwVJyqTylRxh8pUcYfKScWk8oTKScUTKicqU8WJylRxonJSMalMFZPKEypTxaRyUjGpnKhMFZ90sdZaL3Gx1lovcbHWWi9hf/BFKicVn6RyUjGp3FHxhMpJxaQyVUwqJxUnKlPFpDJVPKFyUnGiclIxqUwVd6g8UTGpTBUnKndUfJPKVDGpTBWTyknFExdrrfUSF2ut9RIXa631Ej88pHJSMamcqNxRMVU8UXGHyknFScWkcqJyUvFNKndU3KEyVUwVJypTxR0qJxV3qEwVJypTxaRyonJHxaRyh8pUcVLxTRdrrfUSF2ut9RIXa631EvYHf5HKScWkMlVMKndUnKicVJyoTBWTylRxh8pvqvgklScq7lCZKk5U7qg4UZkqJpVPqrhDZaqYVKaKf8nFWmu9xMVaa73ExVprvcQPH6YyVUwqU8WJylQxqdxRcUfFpDKpPFExqUwVd1RMKlPFb1KZKqaKSeVvUpkqnlB5ouJE5UTlpOJE5UTliYpPulhrrZe4WGutl7hYa62X+OGXVdxRcUfFicpUMancUfE3VUwqJyonFU+ofFPFpDJVnFRMKlPFEyp3qEwVk8pUcVJxonJScaJyR8WJylTxxMVaa73ExVprvcTFWmu9hP3BX6TyRMWkMlXcofJExYnKScWJylRxovJExaQyVUwqU8WkclJxh8pUcaLyRMUdKk9U3KFyR8WkckfFicpJxSddrLXWS1ystdZLXKy11kv88GEqJxVTxR0qJxV3qDxRMamcVEwqd1RMKk9UTCpPVJxUnKj8poo7VJ6oOFGZVKaKOyomlUnlpOJE5Y6Kb7pYa62XuFhrrZe4WGutl/jhwyruUJkqJpUnVKaKk4pJ5UTlpGJS+ZeoTBW/SWWqOFGZKk5UPqliUpkqTlSmipOKOypOKiaVqWJS+SSVqeKTLtZa6yUu1lrrJS7WWuslfvhlKndUTCqTylQxVfymijsqJpWTiqniRGWqmFQmlaliqjhRmSqeUJkqTlSmikllUjmpuEPlk1ROKiaVqWJSOVG5o+IOlUllqnjiYq21XuJirbVe4mKttV7C/uCLVE4qTlSmijtUpooTlTsqTlQ+qeKTVKaKT1KZKr5J5aTiDpWpYlKZKiaVk4pvUrmjYlL5pIpvulhrrZe4WGutl7hYa62X+OEhlanikyomlaliUpkqJpWp4o6KE5U7Kj5J5Y6KSWWqmFROKqaKE5Wp4kRlqpgqTlSmipOKO1Q+SeWk4qTiRGVSOam4Q+U3Xay11ktcrLXWS1ystdZL/PDLKiaVT6qYVE5UvqniRGWqeKLimyqeUJkqTlTuUJkqnlCZKk4qTlROVKaKT1I5qThRmSomlaniRGWqeOJirbVe4mKttV7iYq21XuKHhyq+qWJSeaLiRGWqmFSmijtU7lD5pIo7VKaKE5WTihOVqeKOiidUpopJZao4UZkqJpVPUpkqpopJ5URlqphUpopJ5TddrLXWS1ystdZLXKy11kvYHzyg8i+pmFSeqJhUpopJ5aRiUpkq7lC5o+IOlaliUvmkiknlkyq+SWWqOFGZKj5J5TdV/E0Xa631EhdrrfUSF2ut9RL2Bw+onFScqEwVd6hMFU+oTBUnKlPFicoTFScqJxWTyknFicpUcYfKVPGEyh0Vk8pUcaJyR8WJylTxSSonFXeoTBWTylQxqUwVT1ystdZLXKy11ktcrLXWS/zwUMUdKneoTBUnKk9UPKEyVXySyh0VJxWTyiepTBVPqEwVT6hMFScqU8Wk8kTFpDJVnKhMFVPFpHKiMlWcqJyoTBWfdLHWWi9xsdZaL3Gx1lov8cNDKicVU8WkclJxR8UdKpPKVDGpTBUnKlPFpDJVPKEyqdxR8UkVT6hMFScVk8pUcaIyVUwVk8pUMancoTJVTCpTxVTxSRV3VJyoTCpTxRMXa631EhdrrfUSF2ut9RI//GNUnlCZKk4qJpWTiknlDpUTlTsqfpPKiconVUwqU8UTKlPFN6lMFVPFScWJyhMqT6icVHzTxVprvcTFWmu9xMVaa73EDw9VTConKlPFHSpTxYnKJ6lMFZPKExUnKpPKScU3VZyonFTcUXGiMlXcoTJV3KFyUnGiMlWcqEwVJyonFScqU8UTKlPFExdrrfUSF2ut9RIXa631EvYHD6hMFZPKVHGiclJxojJVnKicVEwqf1PFHSpTxYnKVHGi8kTFHSp3VEwqU8UTKicVk8pUcaJyUvGEyknFicodFd90sdZaL3Gx1lovcbHWWi/xw0MVJxVPVDyhMlWcVJxUTCpTxYnKVPGbVE4qTlTuqDhRuaPiRGVSmSomlanimyr+S1ROKu5QOal44mKttV7iYq21XuJirbVe4ocPU5kqJpWpYqqYVJ6omFSeUJkqTlSmikllqphUTipOKk5U7qiYVO5QmSomlaliUvmkiknlpGKqmFROVKaKT1I5qZgqJpWTiicqvulirbVe4mKttV7iYq21XuKHh1SmiidUpopJ5Q6VqeJEZap4omJSmSomlaliUjlRmSq+qWJSmVSmijtUpoo7VCaVT1KZKiaVqeKTVKaKE5U7Kk5Upoo7VKaKJy7WWuslLtZa6yUu1lrrJX74MpWpYlKZKiaVk4onVKaKSWWqmFSmijtUTlQ+SWWqOFGZKiaVqeJE5aRiUrlDZaq4Q+VEZaqYVE5UpoqTijtUpoqpYlKZKiaVk4pJZar4TRdrrfUSF2ut9RIXa631EvYHH6QyVZyonFRMKk9UTConFZ+kclLxhMpU8YTKN1VMKlPFEypTxR0qd1ScqJxUTCp3VEwqU8VvUjmp+KSLtdZ6iYu11nqJi7XWeokfHlKZKj5J5aTiiYo7VKaKE5U7VO6omComlanijopJ5aRiUjlRmSomlZOKO1SmijsqJpUTlanikyomlTtU7qiYVKaKk4pvulhrrZe4WGutl7hYa62XsD/4IJWp4kRlqjhROamYVKaKSWWqOFGZKj5J5YmKJ1SeqJhU/ssqJpWTikllqphUpopJ5Y6KSeWkYlKZKiaVT6r4pIu11nqJi7XWeomLtdZ6iR8eUnmi4kRlqjhRmSpOKiaVT1I5qTipmFROVJ6oeEJlqjhROak4UTmpuENlUpkqJpU7VKaKOypOVKaKE5UTlaniX3ax1lovcbHWWi9xsdZaL/HDh1VMKlPFicpUMalMFVPFpDJVTCpTxaQyVUwqJxWTyqTyRMWkMlVMKlPFpDJV3FFxR8UdKicVn1QxqTxRcaIyVdxR8UTFicpUcaJyR8UTF2ut9RIXa631EhdrrfUS9gcfpHJSMalMFZPKScWkMlVMKlPFJ6lMFZPKScWJyh0Vk8q/pOJE5aTiCZU7KiaVJyomlTsqJpWp4gmVqeJfdrHWWi9xsdZaL3Gx1lov8cM/ruJE5ZtUnlCZKu5QmSomlanijopJZaqYVKaKSWWqmFQmlaliqphUnlCZKk5UJpWTijtUpoo7VKaKE5U7Kp5QOan4pIu11nqJi7XWeomLtdZ6iR8eUpkqJpVJZao4UZkqpooTlTtUpopJ5aRiUrlD5QmVk4qTiicqJpWpYlI5UfmkihOVqeJE5UTlpGJSmSomlaniRGWqmFSmiknlpGJSmSp+08Vaa73ExVprvcTFWmu9hP3Bf4jKVHGHyt9UMalMFZPKVDGpTBWTylQxqZxUTCpTxYnKVHGHyknFHSpTxW9S+U0Vk8pU8YTKVDGpnFR80sVaa73ExVprvcTFWmu9hP3BAyp3VEwqU8Wk8kTFHSqfVDGpfFLFpHJSMalMFZPKVHGiMlXcofJExaQyVZyoTBWTyh0Vk8pUcaJyUvGEylRxonJS8TddrLXWS1ystdZLXKy11kvYH/wilaniDpWTikllqphUpopJZaqYVO6oOFGZKp5QmSomlZOKSeWOihOVqeJE5aRiUrmj4pNU7qg4UTmpmFROKiaVqeIOlZOKb7pYa62XuFhrrZe4WGutl7A/eEBlqrhDZaqYVE4qJpWTiidUpopJZaqYVL6pYlI5qZhUpoo7VKaKSeWbKiaVv6niDpWp4gmVqeJE5Y6KO1ROKp64WGutl7hYa62XuFhrrZewP3hA5YmKSWWq+E0qJxWTyh0Vd6hMFZPKHRUnKlPFicpUcYfKVHGHyhMVv0nlpOJE5Y6KE5Wp4pNUpopvulhrrZe4WGutl7hYa62X+OGhim9SOam4Q2WqeKJiUjlR+ZtUTiomlZOKO1SmihOVOyomlW9SuaPiROWOin+JyonKScUTF2ut9RIXa631EhdrrfUSP3yYyidVnKhMFZPKN6lMFZ+kckfFpDJVTCqTyknFicoTKlPFicqkclIxqZxUTCpTxR0qJxUnKpPKExWTylQxqUwVU8Wk8psu1lrrJS7WWuslLtZa6yV+eEhlqphUpopJ5UTlpGJSOamYVE4qJpWp4kRlqjhRuaNiUpkqJpVPUrmj4g6Vk4oTlU+qmFROKqaKE5WpYqr4popJZao4UZkqftPFWmu9xMVaa73ExVprvcQPX1ZxUvGEyknFN6ncofJJKlPFExWTyqTymyruUDlRmSqeqJhUTlSmiqliUnmiYlKZKk4qJpWp4kRlqvimi7XWeomLtdZ6iYu11nqJH36ZyknFpHJHxaRyR8UdFXeoTBWTyonK31QxqdyhcofKHRV3qNxRMalMFScVJyp3VEwqJxWTyhMqU8Wk8psu1lrrJS7WWuslLtZa6yXsD/7DVKaKE5WTikllqphUTiomlTsqJpWpYlKZKk5UPqniDpWpYlJ5ouIOlaliUpkqTlSeqDhROamYVE4q7lC5o2JSmSqeuFhrrZe4WGutl7hYa62X+OEhld9UMVWcqNyh8kTFScWkMlVMKt9UMalMFScqJypTxSdVnKjcUXFSMalMFScVd6g8ofKEylRxUnGiMlV80sVaa73ExVprvcTFWmu9xA8fVvFJKk9UnKhMFZPKpHKiclJxonKHyhMq31TxRMWkcqIyVUwqd6hMFXdUTConFd9UMamcVNyhclLxTRdrrfUSF2ut9RIXa631Ej98mcodFXeoTBV3VEwqU8WkMlXcoTJV3KEyVZyonFRMKk+ofJLKicpUcVIxqUwqT6jcUTGpnFTcoXKHyhMVf9PFWmu9xMVaa73ExVprvcQPL6NyR8VUcYfKScVUMak8oTJVTBV3VEwqU8VJxR0qd1RMKneoTBUnKpPKVDGpTBWTyqQyVZyo3FFxR8UnqUwV33Sx1lovcbHWWi9xsdZaL/HDy1VMKneonFQ8UTGpnFScqJxUTConFXeoTBUnFZPKicoTFScqU8WJyonKVHGHylQxqdyhcqJyR8WkMlX8pou11nqJi7XWeomLtdZ6CfuDB1Smik9SmSpOVKaKE5WTihOVqeJE5Y6KJ1Q+qWJSeaJiUjmpOFG5o+JE5YmKSWWqmFROKk5UTiomlaniROWJim+6WGutl7hYa62XuFhrrZf44cNU/qaKSeWOiknlCZWp4gmVk4qpYlKZKu5QOamYVE5UTiomlZOKSWWqOFF5omJSmSruqDhRmSo+SWWqOFE5UZkqPulirbVe4mKttV7iYq21XsL+YK21XuBirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJ/9b40H+fVs2EAAAAASUVORK5CYII='),
(26, 'superviseur-1239', '', '', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAAE0CAYAAACigc+fAAAAAklEQVR4AewaftIAABbtSURBVO3BQY7cWhLAQFKo+1+Z42WuHiCouu2vyQj7g7XWeoGLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeokPD6n8pooTlTsq7lCZKk5UTiruUHmiYlKZKiaVk4oTlTsqnlCZKr5JZao4UZkqJpWTihOVqWJSuaNiUvlNFU9crLXWS1ystdZLXKy11kt8+LKKb1I5UTmpuEPlCZWTiknliYpJ5URlqjipOFGZKp5QmSomlaliqnhC5Q6Vk4pJZaq4Q2WqOKn4popvUvmmi7XWeomLtdZ6iYu11nqJDz9M5Y6KOypOVJ6oOFG5Q2WqmFSmikllUrmjYlL5TRUnKicVk8pUMalMFZPKVDGpfFPFicpUMVXcoTJVfJPKHRU/6WKttV7iYq21XuJirbVe4sP/uYpJZVKZKqaKSWWquKNiUpkqJpWTikllqjhRmSomlZOKSWWquENlqphUTlSmikllqnhC5QmVqWJSmSqmihOVqeK/7GKttV7iYq21XuJirbVe4sPLqEwV36QyVZyoTBUnKlPFpDJV3FExqUwVU8WkcqJyh8pUcVJxR8WJylRxovKTKiaVO1ROKt7sYq21XuJirbVe4mKttV7iww+r+EkqJypPVJyonFRMKlPFHRUnKlPFpHKiMlXcUXGHyqQyVTyhMlVMFZPKExVPqEwVk8pJxaRyUvFExb/kYq21XuJirbVe4mKttV7iw5ep/E0Vk8pUMalMFZPKVHFSMalMFZPKVDGpTBWTylQxqUwVk8pUMalMFZPKicpUcVIxqUwVk8pUMamcqEwVk8pUMamcqEwVk8pUMalMFZPKEypTxYnKv+xirbVe4mKttV7iYq21XuLDQxX/sopJ5URlqjipuEPlCZUTlanipGJS+aaKf4nKVPGTKiaVE5WpYlKZKp6oOKn4L7lYa62XuFhrrZe4WGutl/jwl6k8UTGpnFTcoTJVnKhMFScqd1ScqPxNKk+o3FFxR8WJyk+qeKLim1SmiknlpOIOlanimy7WWuslLtZa6yUu1lrrJewPvkhlqrhDZaqYVKaKSeWOiknlpGJSuaPiRGWqmFSmihOVqeIOlaliUpkqJpWp4kTlpOJE5Y6KSWWqeEJlqrhD5Y6KE5WpYlI5qZhUpopJZaqYVKaKJy7WWuslLtZa6yUu1lrrJT48pHKiclIxVUwqU8Wk8pMqJpWp4kRlUjmpmFSmihOVE5UnVE5UfpLKScWk8k0qU8UTKlPFScWkcqLyN1WcVHzTxVprvcTFWmu9xMVaa72E/cEDKlPFpDJVTConFZPKScWkMlWcqEwV36QyVdyhckfFpHJScaIyVdyhMlWcqJxUTCp3VJyoTBWTylQxqZxUTCpTxRMqd1Q8oTJVTCpTxTddrLXWS1ystdZLXKy11kvYH/wglaniROWOikllqrhD5Y6KSWWqOFGZKu5QeaLiRGWqmFROKiaVqeIJlZOKSWWqmFSmihOVn1QxqUwVk8pUcaIyVUwqU8WJyhMVT1ystdZLXKy11ktcrLXWS3x4SOWk4omKE5Wp4kRlqpgqnqg4UZkq7lD5JpWTikllqphUvkllqjipmFSmipOKn1RxojKpTBWTylRxR8UdKlPFVDGpTBU/6WKttV7iYq21XuJirbVe4sOXVUwqd1RMKlPFHSpTxYnKHRWTyknFpHJHxaRyUnGickfFScWkcofKVHFSMan8JJWpYlI5UZkq7lCZKv4lKlPFb7pYa62XuFhrrZe4WGutl7A/+CKVqeJE5aTiRGWqOFG5o2JSOamYVO6omFROKiaVqWJS+UkVk8pUMalMFScqT1ScqHxTxYnKN1VMKicV36QyVUwqU8U3Xay11ktcrLXWS1ystdZLfPjHqUwVU8UdFZPKVDGpTBUnKicVT1RMKk9UPKHymypOVO5QOamYVO5QmSpOKiaVk4qTikllUjmpuKNiUpkqJpWp4omLtdZ6iYu11nqJi7XWeokPD6lMFZPKVHFHxaTyRMVUMamcqEwVd6hMFd9UcVIxqZxUTCpTxRMVJyp3VEwqk8pUcUfFpHKHylQxqZxUTCpTxR0Vk8qkclJxh8pU8U0Xa631EhdrrfUSF2ut9RL2B1+kclJxovJNFZPKScWkMlXcoXJHxR0qJxVPqJxUnKg8UfGEyknFicpJxaTykyomlaniCZUnKk5UpopvulhrrZe4WGutl7hYa62XsD/4IpU7Ku5QuaNiUpkqJpXfVHGHylQxqUwVJyonFT9J5Zsq7lB5omJSmSomlZOKE5WfVDGpTBV3qNxR8cTFWmu9xMVaa73ExVprvcSHh1TuqJhU7qiYVE5UporfVDGpfJPKHSpTxaQyqUwVJypTxaQyVdyhcofKVDFVTCpTxaRyh8pUMamcqEwVd6icVEwq31Qxqfyki7XWeomLtdZ6iYu11nqJD19WMalMKlPFHSp3VJyoTBUnKlPFicpUcaIyVTyh8kTFpPJNKlPFpDJVTCqTyonKScVJxaQyVUwqk8pUMal8U8U3qTxR8ZMu1lrrJS7WWuslLtZa6yU+PFRxUjGp3KEyVUwqU8UTKt9U8U0VT6hMKlPFpDJVnKhMKicVk8pUcVIxqZxU3KHyTRWTyhMqU8UdKndUTCpTxd90sdZaL3Gx1lovcbHWWi/x4ctUpoqpYlI5qTipmFSmikllqphU7lD5poonVKaKqWJSOam4o2JSeUJlqjipOFGZKu6ouKNiUpkqJpVvUpkq7qiYVO5QuaPiiYu11nqJi7XWeomLtdZ6iQ9fVnGicofKVDGpTBU/qWJSmSr+popJZaq4Q2WqmFROKiaVSeUJlZOKE5WpYqqYVKaKE5WpYlKZKiaVSeWOikllqphUJpWTin/JxVprvcTFWmu9xMVaa73Eh4dUnqi4Q+VEZap4ouKkYlK5o+JEZao4UZkqTipOKn5TxYnKVHGicofKVDFVTCp3qEwVk8pvUpkqJpWp4o6K33Sx1lovcbHWWi9xsdZaL/HhoYpJ5QmVb1KZKu5QeaJiUplUpooTlaniDpWp4kTlCZWpYlK5o2JSmSruqJhUTlSmikllqphUJpU7Kr6p4gmVk4rfdLHWWi9xsdZaL3Gx1lov8eHLKk5U7qg4UTmpmFSmim+quKNiUpkqTlTuqLij4kTlpGJSuUNlqpgqTiomlUllqphUpoqTin+JylRxR8UdFZPKHRVPXKy11ktcrLXWS1ystdZLfHhI5ZsqJpWp4g6VqeKk4jepTBWTyknFicoTFZPKHSpTxYnKicpUMalMFU9UTCpTxYnKVHGi8oTKVHGiMlVMKlPFicrfdLHWWi9xsdZaL3Gx1lovYX/wgMpU8S9TmSqeULmj4kRlqrhD5aTiRGWquENlqjhRuaPiDpWpYlI5qThRuaPiROWOijtUpopJ5Y6KSeWOim+6WGutl7hYa62XuFhrrZf48FDFpHJScYfKScWJylQxqUwVJyonFZPKHRWTyhMVJypPqJyoTBUnFZPKicpUcaIyVUwqd1RMKlPFpPJNKlPFHSonFScqU8WJyk+6WGutl7hYa62XuFhrrZewP3hA5aRiUpkq7lA5qZhUTiomlanim1ROKk5UpopJZao4Ubmj4kTlpGJSmSomlaniROWOiidUpopJZao4UZkqnlCZKiaVqWJS+aaKn3Sx1lovcbHWWi9xsdZaL2F/8IDKv6RiUpkqJpWp4ptUpopJ5aTiROWkYlKZKiaVqWJS+UkVT6g8UTGpTBV3qHxTxYnKScWk8i+peOJirbVe4mKttV7iYq21XsL+4AepTBWTylRxh8pU8YTKVHGiclJxojJVTCpTxYnKHRUnKndU3KFyUnGickfFpHJHxaQyVZyoTBUnKicVk8pUcaIyVdyhckfFT7pYa62XuFhrrZe4WGutl/jwy1TuUJkqTlSeqJhUpoqp4omKSWWqeKLiRGWq+CaVqeKk4kRlqrhD5SepnFRMKlPFVDGpTCp3qNyhMlXcUfGbLtZa6yUu1lrrJS7WWuslPjykMlVMFZPKHRV3VEwqU8WkMqmcqNxRMalMFVPFpPKEylRxR8WJyknFN1VMKicVd1ScqEwVJyqTyonKVPE3VdxRMancUfHExVprvcTFWmu9xMVaa73Eh4cqTlSmikllUvlNFZPKVHGi8oTKVDFVTCpTxR0qU8Wk8oTKb6o4UZkqJpUTlaliUjmpmFTuUPlNKv9lF2ut9RIXa631EhdrrfUSH75M5YmKb6o4UXmiYlKZVKaKSeVE5URlqphUTlSmihOVqeJE5Q6VOyomlaniDpUnKk4qJpWpYlI5qZhUflLFpHJS8Zsu1lrrJS7WWuslLtZa6yU+fFnFpDKpTBWTyh0VJyp/U8WkMlWcVEwq31RxonKiclJxovJNFZPKVDFVTConKlPFpDJVnFScVPxNFf8lF2ut9RIXa631EhdrrfUSH35ZxaRyUjGpTCpTxUnFpDJVnKicVNyhclLxTRWTyh0Vk8qJyknFpDJVnKicVJyoTBWTyjep3FExqZxUnFScqEwqU8WJyh0V33Sx1lovcbHWWi9xsdZaL/HhIZU7VO5QmSpOVH5TxRMVd1ScqJyonFScqJxUnKicVEwqU8VJxYnKExUnFZPKHRWTyk9SmSruqDhRmSomlaniiYu11nqJi7XWeomLtdZ6iQ+/rGJSOak4UTmpuEPlm1SmikllqphUTiqmiknlDpWpYqq4Q2WqOFE5UZkqJpWp4qTiCZWTihOVJyp+kspUMalMFX/TxVprvcTFWmu9xMVaa73Ehy+rmFQmlaliUplUpoqpYlKZVKaKqeJE5UTlpGJSOVGZKiaVSWWqmCpOVJ5QmSq+qeJEZap4QmWqeEJlqpgqJpWTihOVk4o3u1hrrZe4WGutl7hYa62X+PBQxaQyVUwqk8pUcYfKScU3VUwq/zKVqWKqmFSeUJkqTlTuULlDZaqYVKaKE5WpYlI5UZkqTiomlW+qOKm4Q2WqmFSmim+6WGutl7hYa62XuFhrrZf48JDKExWTyh0Vk8odKicVk8pUcUfFicqkMlV8k8pUcaIyVUwqk8odFZPKScUTFZPK31QxqTxRcaIyVZyoPFExqUwVT1ystdZLXKy11ktcrLXWS3x4qOJE5Y6KE5WTikllqrhD5Q6VqWJSOak4UTmpeELlDpVvUpkq7lA5UTmpeKLiN1XcoTJVnKhMFZPKicpU8ZMu1lrrJS7WWuslLtZa6yU+PKRyUjGp3KEyVZyoTBWTyh0Vd1TcUfFExaTykypOVKaKn6QyVZyonKhMFScqU8UdKicVT6jcofJExaQyqfyki7XWeomLtdZ6iYu11noJ+4MHVE4qvkllqvgmlZOKE5UnKiaVqWJSOal4QmWqOFE5qZhUpopJ5Y6KSWWqOFGZKiaVqWJSmSqeULmjYlKZKiaVqeJE5aTib7pYa62XuFhrrZe4WGutl/jww1ROKk5UpopJ5aTijooTlaliqphUpopJ5YmKSWVSmSpOVE5UTipOVKaKk4oTlSdUTlSeUJkqTlS+qeIOlanipOJfcrHWWi9xsdZaL3Gx1lovYX/wi1TuqLhD5aTiROWkYlKZKu5QmSomlaliUjmpuENlqphUpooTlW+qOFGZKiaVk4rfpDJVPKEyVUwqU8WJyknFpHJSMalMFU9crLXWS1ystdZLXKy11kt8+GUVk8qJyknFScUTFZPKVDGpTBXfpPKTKiaVqWJSmSqmiknlpGJSOVH5JpWp4kTlpGJSuUNlqjipuENlqrhDZao4UflJF2ut9RIXa631EhdrrfUS9gcPqPykiknlpOKbVE4qJpWpYlK5o+JEZao4UTmpeELljopJZaqYVKaKSeWkYlK5o2JSOam4Q+WkYlK5o+JEZaq4Q2Wq+E0Xa631EhdrrfUSF2ut9RIfHqqYVKaKSeWkYlI5qZhU7qi4o2JSmSpOKn6Syh0Vk8pUcUfFb1KZKiaVSeWkYlJ5QmWq+E0Vd1TcofKEylTxxMVaa73ExVprvcTFWmu9xIeHVE5UpopJ5aRiUrmj4ptUnlB5ouKOijsqnlCZKk5UflLFHSonKlPFHSpTxVRxonKHylRxh8pJxaQyqUwVP+lirbVe4mKttV7iYq21XuLDP07ljopJZaqYVKaKJ1SmipOKE5VvUpkqJpWpYlI5qXii4qRiUvmXqEwVU8WkclJxUnGHyknFHSpTxd90sdZaL3Gx1lovcbHWWi9hf/CAylQxqUwVT6jcUfGEylTxhMpJxYnKVDGpfFPFicpJxYnKExUnKlPFpDJV3KEyVfwmlTsqvkllqjhRmSq+6WKttV7iYq21XuJirbVewv7gi1SmijtUTiomlaliUjmpmFSmihOVqeI3qdxRcaJyUjGpTBWTyknFicodFXeo3FExqdxRcaJyUjGpnFQ8oTJV/Msu1lrrJS7WWuslLtZa6yU+fFnFpHJSMVWcqEwVk8oTFScqU8UdKlPFpDJVnFRMKv+SiknlROWbVKaKk4o7Kk5UTlSmihOVk4pJ5aRiUjlReaLiJ12stdZLXKy11ktcrLXWS3x4SGWqOKmYVH5SxaRyonJScYfKVDGpnKhMFZPKT6q4Q+WJijtUJpWp4g6VqeIOlaliUnmiYlL5myomlaniRGWqeOJirbVe4mKttV7iYq21XsL+4AGVqeI3qUwVk8pJxYnKVDGpnFRMKlPFT1I5qThROamYVKaKSWWqmFSmiknlmypOVKaKSWWq+CaVJypOVKaKSWWqOFGZKn7TxVprvcTFWmu9xMVaa73Eh1+mclIxqUwVU8VJxaQyqUwV/xKVb6o4UZkqJpVJ5UTlRGWqmFTuqDhRmVROKu5QmSruUHmi4gmVE5UnVE4qnrhYa62XuFhrrZe4WGutl7A/+A9TmSpOVP6miknliYpJZaq4Q+WJijtU7qiYVE4qJpU7Kk5UvqniCZUnKu5QmSomlZOKb7pYa62XuFhrrZe4WGutl/jwkMpvqnii4kRlqphU7qiYVE4qTlROKiaVqeI3qUwVJxWTyqQyVUwqJxWTylRxovJfVjGpnKhMFXdUTCo/6WKttV7iYq21XuJirbVe4sOXVXyTyhMqJxV3VEwqJypTxaTyhMpUMVVMKicVk8pUMamcVNyhMlVMKneoTBUnKicVk8pUcaIyVUwqU8XfVPFNFT/pYq21XuJirbVe4mKttV7iww9TuaPiCZWp4kRlqvibVJ5QmSpOKiaVE5UTlW9SmSruqJhUTiomld9UcaJyUvGEyk9SOal44mKttV7iYq21XuJirbVe4sN/XMWkcqIyVUwqT1ScqNxRcaLyhMpJxaQyVdyhMlVMKneoTBUnFZPKScUdKicVJyp3VEwqJxVTxaRyUvEvu1hrrZe4WGutl7hYa62X+PByKicqJxWTyonKVHFScaJyR8WkMlVMKv9lFXeoTBUnKlPFVDGpnKh8k8pUMancUXGiclJxUvGTLtZa6yUu1lrrJS7WWuslPvywin9ZxU9SmSomlanipGJSuUNlqvgmlaliqjipOFG5o+JE5Q6Vk4oTlaliUpkq7lD5m1Smit90sdZaL3Gx1lovcbHWWi/x4ctU/ktUvqliUplUpopJZao4qZhUpopJZVI5qThRmSruUHmi4kRlqrhD5W9SmSpOKk5U7qiYKk5U7lCZKp64WGutl7hYa62XuFhrrZewP1hrrRe4WGutl7hYa62XuFhrrZe4WGutl7hYa62XuFhrrZe4WGutl7hYa62XuFhrrZe4WGutl7hYa62XuFhrrZe4WGutl7hYa62XuFhrrZf4H0IFZkegema+AAAAAElFTkSuQmCC'),
(32, 'client-4981', '', '/upload/photoprofil/photoprofil-1606213243948.jpeg', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAAE0CAYAAACigc+fAAAAAklEQVR4AewaftIAABdFSURBVO3BQW7s2pLAQFLw/rfM9jBHBxBU5fu+OiPsF2ut9QIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RI/PKTylyomlaliUjmpmFSmijtUpoonVKaKSeWk4pNUpooTlZOKE5WTikllqphUpooTlaliUnmiYlKZKu5QeaJiUvlLFU9crLXWS1ystdZLXKy11kv88GEVn6TyRMUTKlPFpDJVTCrfVHGiclJxR8WJyknFpDJVnFTcoTJV3FExqZxUnKicVEwqU8UdFZ9U8Ukqn3Sx1lovcbHWWi9xsdZaL/HDl6ncUfFfpnKiclJxojJVnKhMFVPFpPJJKlPFpHJSMamcqEwVU8UdKlPFX1KZKk5UpoqpYlKZKj5J5Y6Kb7pYa62XuFhrrZe4WGutl/jhf1zFX6q4Q+UJlaniRGWqmComlTsqpoo7VKaKk4pJZVI5qZhUpoonKu6omFQmlTtUpoqp4kRlqvhfdrHWWi9xsdZaL3Gx1lov8cP/cyonFZPKVDGp3KFyUjGpTBWTyqRyUnGHyiepnKicVPyliknliYpPUjmpeLOLtdZ6iYu11nqJi7XWeokfvqzim1SmiknljopJZap4QuWk4g6VqWJSmSomlU+quENlqrhD5ZNUTiruqDhR+Zcqnqj4L7lYa62XuFhrrZe4WGutl/jhw1T+yyomlSdUpopJZaqYVE5UpopJ5ZMqJpWpYlI5UZkq7lCZKk4qJpWpYlKZKiaVOyomlanipGJSmSomlaliUjlRmSpOVP7LLtZa6yUu1lrrJS7WWuslfnio4n+JyonKVHFS8S9VnFRMKlPFpDJVPFHxSSpPqJyoTBWTyh0VJxVPVJxUPFHxv+RirbVe4mKttV7iYq21XuKHL1O5o2JSmSqeqDhROVGZKv6Syh0VU8VJxRMqT6g8oXJSMalMFXdUTCpTxYnKVHGickfFHSp3VJyo3FHxxMVaa73ExVprvcTFWmu9xA9fVjGp3FHxSSonFScVk8oTFZPKVDGpnFScqJxUfFLFicpJxaQyVZyofFLFpDJVTConFZPKVPGEylQxVUwq31QxqXzSxVprvcTFWmu9xMVaa73ED/8xKicVT1ScqEwVk8odFd9UcaIyVZyonFRMFZPKpDJVTBVPqEwVU8UdKp9U8YTKScWkMlVMKicVk8pUMancoTJVfNLFWmu9xMVaa73ExVprvcQPH6YyVUwVJypTxUnFX6o4UZlUTipOVKaKE5UTlaliqphUJpWpYqqYVO5QuaNiUrmj4gmVv1RxUvFJFZPKVHGiMlV808Vaa73ExVprvcTFWmu9xA8fVjGpPKFyUjGpTBV3VJyo3FFxonJS8UkVk8pUcVJxR8Wk8kTFScWkcofKHRVPqEwVk8qJyknFN6ncoTJVfNLFWmu9xMVaa73ExVprvcQPD6mcVEwqd1RMKneoTBWTyjep3FHxTSpTxR0qU8WkclIxqZxUTCp3VDxRcaIyVZyoTBWTylQxqUwVJypTxRMVd6icqEwVT1ystdZLXKy11ktcrLXWS9gvPkhlqjhRuaNiUjmpmFS+qeIJlaliUpkqJpVPqjhRmSpOVP5SxaQyVZyoTBWTyknFN6lMFScqJxWTylRxh8pU8U0Xa631EhdrrfUSF2ut9RI/PKRyh8pJxYnKScUdFZPKVHGHylRxR8VfqphUJpWp4g6VqeJE5Y6KSeUOlZOKSeVfUjlROamYVE4qJpWp4gmVqeKJi7XWeomLtdZ6iYu11noJ+8UDKk9UnKhMFScqn1Rxh8onVTyhMlWcqNxRMan8pYonVKaKE5WpYlJ5omJSmSomlaniROWJikllqphUTio+6WKttV7iYq21XuJirbVewn7xh1ROKj5J5Y6KE5VPqnhCZaqYVKaKSeWOihOVqWJS+aaKE5WpYlKZKk5UTiruUJkqJpWpYlK5o+IOlaniROWOiicu1lrrJS7WWuslLtZa6yV++DKVqWJSmVROKp6oOFE5qZhUTipOVKaKE5WpYlKZKk4qJpWpYlKZKj6p4kRlqjhRmSpOKk5UTiomlaliUjlRmSomlaniRGVSuaNiUrmj4psu1lrrJS7WWuslLtZa6yXsFx+k8kkVd6hMFZPKHRWTylRxh8onVUwqd1RMKicVJyonFZPKHRUnKp9UMamcVJyoTBWTyidV3KEyVUwqU8WkMlWcqEwVT1ystdZLXKy11ktcrLXWS9gv/pDKExWTylRxonJScaIyVUwqU8UTKlPFpPJJFScqn1QxqTxRMalMFU+oTBV3qHxTxYnKVHGHyknFicpU8UkXa631EhdrrfUSF2ut9RL2iwdUTio+SWWqmFSmihOVqeIOlaliUpkqTlSmihOVqWJSmSomlaniCZWTiknlpGJSmSomlaniDpWp4kRlqnhCZaqYVJ6o+CaVqWJSmSo+6WKttV7iYq21XuJirbVewn7xQSpTxaTyRMWkMlWcqEwVk8pUMal8UsWkclIxqdxRcaIyVTyhclIxqUwVk8pUcaIyVUwqU8UTKicVJyonFZPKVHGiMlVMKicVJyonFZPKVPHExVprvcTFWmu9xMVaa72E/eJ/iMo3VfwllaliUpkqJpWTiknljoo7VKaKSeWTKu5QuaNiUjmpOFE5qZhUpopJ5aRiUpkqTlROKiaVqWJSmSo+6WKttV7iYq21XuJirbVe4oeHVO6omFTuqJhUpooTlU9SmSqeUJkqJpWp4kTlk1SmiqnijoonVE4qTiomlUnlpGJSmSruUJkqTiq+qeJEZaqYVE5UpoonLtZa6yUu1lrrJS7WWusl7BcfpDJV/CWVk4oTlaniDpVvqjhRmSomlaliUrmj4g6VT6qYVE4qJpWTihOVqeKTVKaKE5UnKk5UpopJ5Y6KT7pYa62XuFhrrZe4WGutl7BffJDKExWTylRxh8pJxaTyRMWJyknFN6l8U8WkMlVMKlPFHSp3VEwqU8WkMlWcqEwVk8pUMalMFd+kckfFicpJxaQyVTxxsdZaL3Gx1lovcbHWWi9hv3hA5aRiUjmpOFGZKiaVqeIvqUwVk8pUMancUTGpnFRMKlPFHSpTxYnKVHGi8i9VnKhMFScqU8WJylTxhMpJxYnKVHGHylTxSRdrrfUSF2ut9RIXa631EvaLB1SeqDhR+aaKO1SmijtUPqniCZWp4pNUTiruUJkqJpWp4g6VqWJSmSomlaliUpkqJpVPqjhROamYVE4qJpWp4psu1lrrJS7WWuslLtZa6yV+eKjiROWJihOVqeIOlSdUPqniCZWpYlKZKk5U7qiYKiaVJyomlaliUjmpuKNiUpkqvqliUpkqJpWTijsq7qiYVKaKT7pYa62XuFhrrZe4WGutl/jhIZWTihOVOyqmikllqphUpoo7VE4qJpWp4pNU7qi4o+IOlaniCZWpYqqYVKaKSeWkYlKZKqaKSWWquKPiROWJihOVqeJEZaqYVKaKSWWqeOJirbVe4mKttV7iYq21XuKHf6ziCZUTlaniROWTKiaVqeJEZar4SyqfVHFHxaRyUvFNKlPFVDGpTBWTylQxqUwVJypTxaQyVUwVk8pJxaQyVUwqU8UnXay11ktcrLXWS1ystdZL2C8eUJkqJpW/VHGHyknFpDJVTConFZPKVHGHylQxqUwVd6j8pYo7VO6oOFE5qThRmSpOVP4/qfimi7XWeomLtdZ6iYu11noJ+8UHqUwVJypTxR0qd1RMKlPFpHJScaIyVUwqd1R8k8pUMamcVNyhMlVMKicVk8odFScqU8WkckfFHSpTxaRyUjGpnFTcoTJVnKhMFZ90sdZaL3Gx1lovcbHWWi/xw4dVfJLKVHFScaIyVdxRcUfFHRUnKndUPKEyVUwqJypTxYnKX1I5qZhUpopJ5QmVE5WTikllqphUTlSmihOVO1Smiicu1lrrJS7WWuslLtZa6yXsFw+oTBWTylQxqUwVd6icVJyoPFHxhMpJxR0qJxWfpDJV3KFyUvGEyh0Vk8pUMalMFZPKScWJylQxqZxUnKhMFXeoTBX/0sVaa73ExVprvcTFWmu9xA8PVZxUTConKk9UnKhMFScqd6hMFXdUTConFU+oTBWTylRxovJExaRyUvFExaTyTRWTyh0qU8WkMqlMFScqn6QyVUwqU8UTF2ut9RIXa631EhdrrfUS9osHVO6omFROKu5QmSomlTsqJpWTikllqnhC5Y6KSWWqOFGZKiaVOyq+SWWqmFSmihOVqWJS+aSKSWWqOFGZKr5JZao4UZkqPulirbVe4mKttV7iYq21XuKHL6s4qZhUJpWp4omKSWWqmFS+SWWqOKm4Q+UOlaliUpkqJpUTlaliUvmmihOVqeKJihOVSeUOlaliUpkqTlSmikllqjhROVGZKp64WGutl7hYa62XuFhrrZf44Y+p3FExqXyTylTxRMVJxaRyUvFExUnFScWkclJxonJHxYnKExXfpDJVnKhMKndUTCp3qEwVJypTxV+6WGutl7hYa62XuFhrrZewX3yRylRxonJHxR0qd1TcoXJSMalMFd+kMlXcoTJVTCpTxaQyVZyo/JdUnKhMFScqU8WkMlXcoTJVnKh8U8WkMlU8cbHWWi9xsdZaL3Gx1lov8cMfU7mjYlKZVE4qpopJ5UTlpGKqOFGZKk5UTiomlaliqjhRmSqeUJkqnqg4UTmpOFG5Q2WqmFQ+SWWqmFSmihOVk4oTlZOKSWWq+KSLtdZ6iYu11nqJi7XWeokfPkxlqjhROVH5poo7VD5JZao4qfgklaliUpkqTipOVE4qvknlCZWpYlI5qZhUTipOVO5QOamYVE4qJpVJZaqYVKaKJy7WWuslLtZa6yUu1lrrJX74MpWTikllqnhCZaqYVE4q7lB5QuWJihOVqeIJlScqTiomlanipGJSmSpOVKaKSeWbVKaKqWJSuaPipOJEZao4UZkqPulirbVe4mKttV7iYq21XsJ+8YDKScWJylQxqdxRcaIyVZyoTBWTyidVnKicVNyhMlV8ksonVXySylQxqZxUTCpTxaQyVZyo3FHxhMpJxaQyVUwqJxWfdLHWWi9xsdZaL3Gx1lovYb94QGWq+CaVOypOVE4qTlTuqLhD5ZMqTlROKiaVqWJS+aaKE5UnKp5QmSomlaliUpkqJpWTihOVk4pJ5aTiRGWq+KSLtdZ6iYu11nqJi7XWeokfvkzlpGJSeaLiROUJlaliUpkqJpWpYlKZKiaVb6r4lyomlaniROWkYlI5UTmpmFSmikllqnii4g6VqWJSmVSmihOVO1Smiicu1lrrJS7WWuslLtZa6yV++DCVO1ROKiaVE5U7KiaVO1SeUJkqnqi4Q+WkYlK5o2JSmSomlTtUTiomlaniRGWqmFSmikllqphUTio+qWJSmSpOVKaKqeJfulhrrZe4WGutl7hYa62XsF98kMpJxaQyVUwqU8WkMlVMKk9UfJLKVDGpnFRMKlPFpPJNFZPKVDGpTBUnKlPFpPJExR0qU8UdKlPFpPJJFZPKVDGpTBVPqEwVk8pU8cTFWmu9xMVaa73ExVprvcQPH1YxqTxRMancUfFJKt9UMamcVNxRMamcVJyoTBV3qNyhclIxqUwVd6hMFScqd6hMFf9SxV+q+KSLtdZ6iYu11nqJi7XWeokfHlKZKu6omFTuqJhUpopJ5Y6KT6qYVKaKqeJEZaqYKiaVqeJEZao4UZkqpooTlaliUrmjYlKZKiaVqWJSmSpOKu5QmSpOVKaKSWWqmFSeqJhU7lCZKp64WGutl7hYa62XuFhrrZf44csqJpWTihOVO1TuqPikiknlRGWqOKmYVKaKE5WpYqq4o+JE5Q6VJ1SmijtU7qiYVE4qnqiYVKaKSWWqeEJlqjhRmSo+6WKttV7iYq21XuJirbVewn7xQSonFZPKN1XcoTJVTConFZPKVDGpTBWTyknFHSpTxYnKExWTyknFpHJScYfKVHGiMlVMKicVk8pUcaJyR8WJyr9U8U0Xa631EhdrrfUSF2ut9RI/PKTyRMWkMlWcqEwVk8pJxYnKVDGpTCpTxaQyVfwvq5hUTiruqLhDZao4UTmpuKNiUrlDZap4QmWqOFGZKp5Q+UsXa631EhdrrfUSF2ut9RI/PFQxqXySylRxovJExYnKVHGiMlVMKlPFVDGpnKhMFVPFpHJScUfFpHJSMancUXFHxaRyonKHylTxhMpJxRMqU8WkclJxh8pJxRMXa631EhdrrfUSF2ut9RI/fFjFicodFZPKVDGpTBWTyqRyR8WkMlWcqEwVk8pUMVVMKk9UTCqTylQxqdxRcUfFicpU8UTFpDJV3KEyVdxRcYfKVDGpTBWTylRxh8pU8Zcu1lrrJS7WWuslLtZa6yV++GMVJyqTyhMqJxWTylQxqdxRMancoTJVTBUnKlPFHRWTyknFicpUcaIyVZyoTBWTylRxUjGp3FFxojJVTCp3VNyhcofKVHGickfFExdrrfUSF2ut9RIXa631Ej98mMpUcUfFHSqfVDGpTBV3qEwVd1TcoTJVTConFZPKVHGHylRxUjGp3FFxUnGiclIxqUwVk8pJxaQyVdyhclLxRMWkckfFN12stdZLXKy11ktcrLXWS9gvHlCZKiaVqWJSeaLiCZWp4g6VqeIOlaliUjmpOFE5qZhUTipOVE4qJpWTihOVk4pJ5Y6KSeWk4pNUpoonVL6pYlKZKiaVqeKJi7XWeomLtdZ6iYu11noJ+8UDKlPFX1K5o2JSmSr+JZWp4kTlpOJE5YmKSWWqmFSeqJhU7qiYVKaKSWWqOFGZKp5QOal4QmWquEPliYpPulhrrZe4WGutl7hYa62X+OGPqZxUTCp3VJyonKhMFZPKHRUnKn9JZao4UZkqJpUTlW+qmFSmijtUpopJ5aTiDpWp4qRiUpkqTlROVJ6omFSmim+6WGutl7hYa62XuFhrrZf44aGKOyruqDhRmVSmiidUpooTlROVk4o7KiaVb1I5qbhDZaqYVCaVqeJEZaq4Q2WqOFG5o2JSmSomlaliUnmi4g6VJ1Smiicu1lrrJS7WWuslLtZa6yV+eEjlL1VMFScqd1RMKpPK/7KKE5Wp4kTlRGWq+CSVqWJSOam4Q2WqmCqeqHii4g6VE5Wp4kTlRGWq+KSLtdZ6iYu11nqJi7XWeokfPqzik1SeqDhRmVSmijtUTiomlUllqjhRmSpOVKaKE5UnKj6p4kRlqjhROak4UTmp+KSKSeWbKj6p4psu1lrrJS7WWuslLtZa6yV++DKVOyruUJkq/stUpoo7VKaKOypOKk5UTlQ+qeKOikllqpgq7qg4UblDZaqYVO6oOFE5UXmi4l+6WGutl7hYa62XuFhrrZf44f8ZlaliUplUTiqmipOKSWWqmFTuUDmpmFTuqJhU7qi4Q+Wk4g6Vk4oTlZOKSeWkYlKZKu5QOamYVKaKSWWqmFQmlZOKb7pYa62XuFhrrZe4WGutl/jhZVSmiqnipGJSmSpOVE4q7qh4ouKkYlJ5ouJE5V+qmFSeqDipmFTuUJkq7qg4qZhUpopJZaqYVKaKv3Sx1lovcbHWWi9xsdZaL/HDl1V8U8WkcofKVPFNKneoTBWTyonKVHFSMalMKicqJxWTylTxSRVPqJyoTBWTyh0VJypTxYnKHRWTyonKicpU8U0Xa631EhdrrfUSF2ut9RL2iwdU/lLFHSpTxSepTBUnKlPFpDJVTCpTxYnKScWkclJxojJVTCpTxRMqU8WkclJxonJScYfKX6qYVE4qvkllqviki7XWeomLtdZ6iYu11noJ+8Vaa73AxVprvcTFWmu9xMVaa73ExVprvcTFWmu9xMVaa73ExVprvcTFWmu9xMVaa73ExVprvcTFWmu9xMVaa73ExVprvcTFWmu9xMVaa73E/wGUWhqDvjwXeQAAAABJRU5ErkJggg=='),
(33, 'client-2630', '', '/upload/photoprofil/photoprofil-1605191920881.jpg', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAAE0CAYAAACigc+fAAAAAklEQVR4AewaftIAABcWSURBVO3BQW7s2pLAQFLw/rfM9jBHBxBU5Xe/OiPsF2ut9QIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RI/PKTylyomlaliUpkqJpWp4g6VqeJEZaqYVJ6omFROKk5U7qg4UTmpmFSmijtUnqg4UTmpOFE5qThRmSomlTsqJpW/VPHExVprvcTFWmu9xMVaa73EDx9W8Ukq/0tUnqiYVE4qJpWpYlKZVKaKk4o7VKaKSWVSmSomlaliUpkq7lCZVE4q/ksVk8pU8UkVn6TySRdrrfUSF2ut9RIXa631Ej98mcodFXdUTConKicqn1QxqdxRcUfFScWkclLxRMVJxYnKJ6mcVJyoTConFXdUTCpTxR0qU8UnqdxR8U0Xa631EhdrrfUSF2ut9RI//I9TmSruUDmpOFE5UblDZaqYVJ6oOFGZKp5QOak4qZhU7lC5Q2WqmComlanikypOVKaKO1Smiv9lF2ut9RIXa631EhdrrfUSP7yMylQxqUwVT1RMKlPFpDKpnKhMFZPKScWkMlU8ofKEylQxqTxRMalMFZPKpDJVnKhMFScVd6jcoTJVvNnFWmu9xMVaa73ExVprvcQPX1bxlyomlaliUrmj4qTipGJSmSpOVE4q7lC5o+Kk4g6Vk4oTlROVOyruqDhRuaPipOJE5Y6KJyr+JRdrrfUSF2ut9RIXa631Ej98mMr/sopJ5URlqphUpoo7VKaKSeVEZaqYVKaKSeUJlaniDpWp4qRiUpkqJpWpYlKZKiaVqeKkYlI5UZkqJpWpYlI5UZkqTlT+ZRdrrfUSF2ut9RIXa631EvaL/0dUTipOVKaKE5WTiknlpGJSeaLiROWOijtUTiqeUJkqJpWpYlI5qbhD5YmKO1TuqPhfdrHWWi9xsdZaL3Gx1lovYb/4QypTxYnKVHGiMlU8ofJJFU+ofFLFEyp/qWJSmSomlaliUnmiYlKZKp5QuaNiUrmj4kTljopJ5aTiiYu11nqJi7XWeomLtdZ6CfvFB6mcVEwqU8WJyknFpHJSMalMFZ+kckfFicpUMalMFZPKVDGpTBUnKndU3KEyVUwqU8Wk8kTFicpJxYnKHRUnKlPFpHJHxR0qJxWfdLHWWi9xsdZaL3Gx1lov8cNDKicVT6h8k8pUMamcVNxRMalMFZPKScVJxUnFpDJVTCpTxUnFicpJxVQxqUwVJxUnKlPFpDJVPKEyVUwqU8WkclLxTSpTxVTxly7WWuslLtZa6yUu1lrrJX74YypTxaQyVUwqU8WkclIxqXyTylRxR8UdKlPFicpUcVJxUjGpTBVTxSepnFScVDxRcaJyojJV3FFxonJHxUnFpPJExRMXa631EhdrrfUSF2ut9RI/fFjFpDJV3KHyRMWkMlV8ksqJylQxqUwVJyonKlPFEypTxUnFpHJSMalMFU+onFRMKneo3FExqUwqU8VUMak8UTGpTBUnFZPKScUnXay11ktcrLXWS1ystdZL/PBhKlPFpDJVnFRMKpPKicpUMal8U8UTKicVk8oTKlPFVDGpnFScVJxUTConFZPKExWTyknFpHKicofKVHFSMamcqNyhclIxqXzTxVprvcTFWmu9xMVaa73EDx9WMak8oXJSMan8JZU7VKaKqWJSuaNiUjmpmFROVKaKSWVSmSomlZOKO1SmikllqphUTiomlaliqphUTiomlaniDpWpYlKZKiaVk4pJZVKZKiaVqeKJi7XWeomLtdZ6iYu11noJ+8UDKlPFicodFXeoTBWTylTxSSonFScqU8Wk8l+quEPlpGJSOamYVJ6oOFE5qbhDZap4QmWqmFSmiknlpGJSuaPiL12stdZLXKy11ktcrLXWS/zwYSpTxVRxojKp3FExqUwVk8pUMalMFU+oTBVTxaQyVXySylRxh8o3VXyTylRxUnGiclIxqTxRcVIxqZxUTCp3VEwqU8WkMlU8cbHWWi9xsdZaL3Gx1lovYb94QGWqmFROKiaVqeIvqZxUTCpTxaRyUjGpPFExqUwVd6jcUTGpnFRMKndUTConFXeonFR8k8pUMalMFZPKExWTylRxh8pJxRMXa631EhdrrfUSF2ut9RI/PFQxqUwVk8qkcqIyVTyh8k0qU8WkclJxh8qkMlVMKt+kclLxX1KZKu6omFT+SxXfpDJVnKicVHzTxVprvcTFWmu9xMVaa72E/eKLVJ6omFSeqPgmlZOKSWWqmFROKu5QmSomlZOKJ1ROKp5QeaLiROWkYlK5o2JSeaJiUvmmiknlpOKTLtZa6yUu1lrrJS7WWusl7BcPqJxUnKhMFZPKScWkclLxhMpUcYfKVPGEylTxhMonVZyonFScqJxU/EtUpooTlTsqJpWp4kRlqrhDZao4UZkqnrhYa62XuFhrrZe4WGutl/jhoYpJ5ZMqJpWTiknlRGWqeEJlqpgqTlSmikllqphUTipOKiaVk4pJZVK5o2JSmSruUJkq7lA5qThRuUPlpGJSuUPlDpWp4g6Vv3Sx1lovcbHWWi9xsdZaL2G/eEBlqphUpoo7VE4qJpWTikllqphUpopJ5Y6KT1KZKiaVqeIJlZOKE5Wp4g6Vk4oTlanik1ROKiaVJyomlaliUpkqJpVvqvimi7XWeomLtdZ6iYu11noJ+8UDKndUPKFyUnGickfFEypPVEwqU8WJyhMVk8pJxaQyVUwqU8WJyh0Vk8odFZ+k8kkVd6icVDyh8kTFExdrrfUSF2ut9RIXa631EvaLD1K5o2JSeaJiUrmj4kRlqnhC5Y6KSWWqmFSmihOVT6o4UZkqJpWTiidUpopJZar4SypTxaQyVUwqJxUnKicVJypTxTddrLXWS1ystdZLXKy11kvYLz5I5aRiUjmpuEPlpGJSmSruUJkq7lCZKiaVOypOVO6omFSmiknlmypOVKaKv6RyUjGpTBWTylTxTSpPVEwqU8U3Xay11ktcrLXWS1ystdZL2C8eUDmpmFSmiknliYonVKaKE5Wp4kTliYpJ5aTiDpU7KiaVOyr+JSonFZPKVHGHyh0VT6icVNyhckfFpDJVPHGx1lovcbHWWi9xsdZaL/HDh1XcoXJSMamcqEwVk8pJxaRyUvEvqbhD5aRiUjmpuEPliYoTlaliUjmpmFSmihOVk4pPUjmpuEPlpOJE5aTiky7WWuslLtZa6yUu1lrrJX74MJWp4qTiRGWquEPlpOIJlScqTlQmlaniDpWTiknlROWkYlKZKiaVk4pJ5Q6Vb1KZKp5QuaPiROWOihOVf8nFWmu9xMVaa73ExVprvcQPH1bxSRV3qNyhclJxh8pUMancUTGpnKjcUTGpnFT8yyomlaliUjlRmSpOKk5UpopJ5aRiUjlRmSo+qWJSmSomlZOKJy7WWuslLtZa6yUu1lrrJX74x6h8UsWJylTxRMWkcofKScVJxaRyonJSMalMFZPKScWkMlU8oTJVTConFZPKpDJVnKhMFZPKJ6mcqJxUTConFVPFpDJVfNPFWmu9xMVaa73ExVprvYT94oNUTiq+SWWq+CaVk4pvUrmjYlJ5omJSmSomlaniROWbKiaVk4pJZaqYVKaKO1SmihOVOypOVO6omFROKj7pYq21XuJirbVe4mKttV7CfvFFKn+p4g6VqeJEZaqYVP6XVUwqU8Wk8k0VT6hMFScqd1RMKlPFpDJVTCpTxR0q/6WKSeWk4pMu1lrrJS7WWuslLtZa6yV+eEjlpGJSOam4Q+VE5aRiUnmi4kRlqphUnqiYVKaKSeWk4o6KO1TuUDmpmCqeqJhUTiomlanipGJSOan4pIo7VCaVqeIvXay11ktcrLXWS1ystdZL/PBhFScVk8qJylTxlypOVKaKJyomlU9SmSpOVJ5QmSpOKiaVk4pJ5aTiL1VMKlPFJ1WcqNyhMlWcVEwqU8U3Xay11ktcrLXWS1ystdZL2C/+QypTxR0qd1RMKk9UTConFXeoTBWTylRxonJScaJyUnGHyh0VT6jcUXGHylRxh8odFZPKVDGpnFTcofJJFU9crLXWS1ystdZLXKy11kv88JDKScWkcqLyTSpTxR0qd1ScqEwVJypTxaQyVdyh8oTKExWTyonKVDGpTBUnKpPKVDGpTBV3qEwVJyonFZPKVDGpTCpPVEwqf+lirbVe4mKttV7iYq21XuKH/1jFJ6k8oXKHyonKVHFHxSdV3KFyR8X/EpU7VKaKSeWk4g6VqeKOikllqvgklaniL12stdZLXKy11ktcrLXWS9gvHlB5ouJE5ZMqJpWTiknljopJ5ZMqTlTuqDhRmSr+l6hMFZPKJ1WcqNxRcYfKVHGiMlWcqEwVk8odFU9crLXWS1ystdZLXKy11kvYLx5QeaLim1SmikllqjhRmSpOVP4lFScqn1QxqUwVn6QyVZyoTBUnKlPFJ6lMFZPKVDGpPFExqXxTxSddrLXWS1ystdZLXKy11kv88GEVk8oTKicVJxV3qPylihOVk4pJ5Q6VqeIOlanik1SmikllqphUpoqp4kRlqrhDZao4qZhUnqiYVO6omFSmiknlv3Sx1lovcbHWWi9xsdZaL2G/+CKVqeJEZaq4Q2WqOFGZKu5QuaNiUjmpmFROKiaVk4oTlaniROWkYlI5qXhCZaqYVO6oOFGZKr5J5Y6KO1SmiknlpOJEZap44mKttV7iYq21XuJirbVe4oeHVKaKE5Wp4kTlCZWp4g6VOypOVKaKSeWOipOKSeUJlSdU7lC5o+KJihOVqeIJlaniRGWqmFROVKaKk4onVKaKqeKTLtZa6yUu1lrrJS7WWusl7BcfpDJVTCp3VEwqJxUnKndUTCpTxYnKExWTyl+qmFTuqDhRmSruULmj4l+iclLxSSonFU+o3FHxSRdrrfUSF2ut9RIXa631Ej98mcpUcaIyqUwVJypTxRMqU8WkclIxqZxUnFScqJxUTCr/EpWTik9SOamYVO6oOKmYVCaVOypOKiaVSeWOijsqvulirbVe4mKttV7iYq21XsJ+8YDKVDGpnFTcoXJScYfKScWkckfFpDJVTCpTxYnKScWkclJxonJSMalMFScqn1RxonJScYfKScWJylQxqUwVJyonFXeonFScqJxUPHGx1lovcbHWWi9xsdZaL2G/+CCVqWJSOamYVE4qTlSmikllqphUpoo7VKaKSWWqmFSmihOVk4pJZao4UZkqJpWp4kTlX1IxqUwVk8odFXeo3FFxh8pUcaIyVZyoTBXfdLHWWi9xsdZaL3Gx1lovYb/4D6lMFScqU8WkMlXcoTJVnKhMFd+kMlXcofJJFU+oTBUnKlPFicpU8YTKVHGi8kkVk8pU8YTKHRWTyhMVT1ystdZLXKy11ktcrLXWS/zwZSonFZPKScWkMlXcoTJVnKg8oTJVTCpTxYnKVHFScaJyh8oTFZPKEyonKlPFHRWTylRxR8WkckfFicpJxRMq/5KLtdZ6iYu11nqJi7XWegn7xRepTBVPqJxUnKicVJyoTBWTyknFJ6lMFScqU8WJylRxojJVfJPKHRWTyknFHSp/qeJE5aTiCZWp4kRlqviki7XWeomLtdZ6iYu11noJ+8V/SGWqmFSeqJhUpopJZaq4Q+Wk4g6VqeJEZao4UTmpOFGZKk5Upoo7VE4qJpWTiknliYo7VKaKJ1SmiknliYpJ5Y6KSWWqeOJirbVe4mKttV7iYq21XuKHh1SmiidUpopJZao4UZkqJpWpYlL5JJWTiqliUpkqTlROKv5LKndUTConFZPKHRWTylQxqUwVk8pUcYfKVPFExSdVTConFZ90sdZaL3Gx1lovcbHWWi/xw4epTBUnFScqU8WkMlWcqEwVJxUnKlPFpHJSMamcVEwqd1ScqNxRcaLyRMWkcqJyUjGpTBUnFScVk8qJyr9EZar4JpWp4omLtdZ6iYu11nqJi7XWeokfHqqYVCaVqeKOijtUpopJ5URlqvikiknlpOKkYlI5UZkq7qiYVKaKk4oTlUllqnhC5QmVqWJSmSomlaniRGWqmFSmikllqphUTlSmiqliUjlR+aaLtdZ6iYu11nqJi7XWeokfHlKZKiaVE5WpYlKZKqaKSeWJikllqrij4qRiUrlDZar4pIqTik+qmFROVJ6oOFF5QmWqOFE5UZkqJpWp4omKE5WpYlKZKr7pYq21XuJirbVe4mKttV7ih4cqJpWpYlKZKiaVqeJE5Y6KE5U7Kk5UpopJ5UTlpOKTKu5Q+SSVqeKOiknlROWkYlK5o+JEZaqYVJ5Q+ZeoTBWfdLHWWi9xsdZaL3Gx1lov8cOHVdyhcqJyUjGpfFLFpHJScaIyVUwqT6icVJyonFTcUTGpnFTcUXFScaLySRUnKlPFEyp3VEwqU8Wk8k0qU8UTF2ut9RIXa631EhdrrfUSPzykMlVMKndUnKhMKk+oTBWTylQxqUwqJxWTylRxojKpTBWTyhMVk8pJxaQyVXySylTxTRV3qNyhckfFpHKickfFpDJVTCpTxaTyTRdrrfUSF2ut9RIXa631Ej/8sYpJZVI5qZhUpoq/VDGpnKhMFZ+kMlVMKicVk8onqdxRcVIxqUwVk8pUMalMKndUnFRMKlPFpHKiclJxh8pU8YTKX7pYa62XuFhrrZe4WGutl7BfPKAyVXyTylTxSSpTxaRyUjGpfFPFpHJHxaQyVUwqJxUnKlPFicpJxYnKVHGickfFicoTFZPKExWTylQxqZxUnKicVHzSxVprvcTFWmu9xMVaa72E/eIBlaniROWkYlI5qbhD5aTiDpWpYlKZKk5U/mUVk8onVZyoTBWTylQxqZxUTConFZPKHRUnKicVk8p/qWJSmSomlaniiYu11nqJi7XWeomLtdZ6iR8eqrij4o6KE5U7Kj6pYlI5UZkqTio+SeWk4omKO1QmlaliqniiYlI5qZhUTiomlROVqeKk4o6KSeWk4g6Vk4q/dLHWWi9xsdZaL3Gx1lov8cNDKn+pYqqYVKaKE5WpYlL5pIpJZaqYVD6p4kRlqphU7lCZKk4qnqiYVKaKO1TuUJkqTlSeUDlReUJlqjhRuaPiky7WWuslLtZa6yUu1lrrJewXD6hMFZ+kMlU8oTJVTCpTxYnKJ1VMKlPFpDJV3KHySRV3qNxR8YTKHRWTyhMVT6hMFZPKScWkMlXcoTJVTConFZ90sdZaL3Gx1lovcbHWWi/xw5ep3FHxhMpUMVXcoXJHxaQyVZyoTBV3qEwVk8o3qXxSxaRyUnFScaIyqZxUTCp3qEwVk8pUcVLxhMpfUpkqnrhYa62XuFhrrZe4WGutl/jhf5zKicodFZPKX6qYVKaKqWJSmVSmihOVqeJE5aTiROVEZaqYVCaVk4qTiicq7qiYVE5UTiomlZOKSWWquENlqphUvulirbVe4mKttV7iYq21XuKH/3EVJyonFZPKScWkclIxqXxTxaTySSp3qEwVJxWTyqQyVUwqU8WkclIxqZxUTCpPVEwqU8WkckfFScWkclJxojJVTCqfdLHWWi9xsdZaL3Gx1lov8cOXVfwllScqPkllqphUpopPqphUTiruqJhUpopJ5YmKSeW/pDJVTCpTxaRyUnFScVJxh8odKk9UfNLFWmu9xMVaa73ExVprvcQPH6byL6mYVO5QOak4qZhUTlSmikllqjhROamYVKaKE5WpYlJ5omJSOan4pIonKk4qTlSmiknljopJ5aRiUpkqJpUTlaniky7WWuslLtZa6yUu1lrrJewXa631AhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631Ev8HMfyo+wCravIAAAAASUVORK5CYII=');

-- --------------------------------------------------------

--
-- Structure de la table `info_perso`
--

CREATE TABLE `info_perso` (
  `id` int NOT NULL,
  `code_info_perso` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_etudiant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int NOT NULL,
  `sexe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` int NOT NULL,
  `phone2` int NOT NULL,
  `phone3` int NOT NULL,
  `statut_matri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_enfant` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `info_resocial_client`
--

CREATE TABLE `info_resocial_client` (
  `id` int NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_twitter` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_twitter` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_facebook` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_facebook` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_skype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_skype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_linkedin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_linkedin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_google_plus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_google_plus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_instagram` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_instagram` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_viadeo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_viadeo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_register` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `info_resocial_client`
--

INSERT INTO `info_resocial_client` (`id`, `code_client`, `nom_profil_twitter`, `lien_twitter`, `nom_profil_facebook`, `lien_facebook`, `nom_profil_skype`, `lien_skype`, `nom_profil_linkedin`, `lien_linkedin`, `nom_profil_google_plus`, `lien_google_plus`, `nom_profil_instagram`, `lien_instagram`, `nom_profil_viadeo`, `lien_viadeo`, `date_update`, `date_register`) VALUES
(19, 'client-8785', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL),
(27, 'client-4981', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL),
(28, 'client-2630', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `info_societe_client`
--

CREATE TABLE `info_societe_client` (
  `id` int NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `poste` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` int NOT NULL,
  `phone2` int NOT NULL,
  `phone3` int NOT NULL,
  `site_internet` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_register` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `info_societe_client`
--

INSERT INTO `info_societe_client` (`id`, `code_client`, `nom`, `poste`, `email`, `adresse`, `phone1`, `phone2`, `phone3`, `site_internet`, `date_update`, `date_register`) VALUES
(15, 'client-8785', '', '', '', '', 0, 0, 0, '', NULL, NULL),
(23, 'client-4981', '', '', '', '', 0, 0, 0, '', NULL, NULL),
(24, 'client-2630', '', '', '', '', 0, 0, 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ligne_panier`
--

CREATE TABLE `ligne_panier` (
  `id` int NOT NULL,
  `code_panier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_lp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_modele` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_fichier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite` int NOT NULL,
  `prix` double NOT NULL,
  `nbcarte` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ligne_panier`
--

INSERT INTO `ligne_panier` (`id`, `code_panier`, `code_lp`, `code_modele`, `nom`, `type_carte`, `lien_fichier`, `quantite`, `prix`, `nbcarte`) VALUES
(98, 'panier-294980677971', 'lignepanier-7574972', 'modele-5319855', 'carte cool', 'carton', 'a1.jpeg', 1, 8250, 50),
(99, 'panier-294980677971', 'lignepanier-4234057', 'modele-3099496', 'carte cool', 'pvc', 'a5.jpeg', 1, 9750, 50);

-- --------------------------------------------------------

--
-- Structure de la table `ligne_panier_commande`
--

CREATE TABLE `ligne_panier_commande` (
  `id` int NOT NULL,
  `code_panier_commande` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_lp_commande` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_modele` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_fichier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite` int NOT NULL,
  `prix` double NOT NULL,
  `nbcarte` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ligne_panier_commande`
--

INSERT INTO `ligne_panier_commande` (`id`, `code_panier_commande`, `code_lp_commande`, `code_modele`, `nom`, `type_carte`, `lien_fichier`, `quantite`, `prix`, `nbcarte`) VALUES
(4, 'commande-panier-5712726', 'commande-lignepanier-7414417', 'modele-4339618', 'carte cool', 'pvc', '/modelecarte/pvc/pvc_111.jpg', 1, 13000, 100),
(5, 'commande-panier-2748533', 'commande-lignepanier-2628660', 'modele-6259036', 'carte cool', 'carton', '/modelecarte/carton/carton_111.jpg', 1, 8250, 50),
(6, 'commande-panier-2748533', 'commande-lignepanier-6309229', 'modele-4339618', 'carte cool', 'pvc', '/modelecarte/pvc/pvc_111.jpg', 2, 13000, 100),
(7, 'commande-panier-8980268', 'commande-lignepanier-4886582', 'modele-5319855', 'carte cool', 'carton', 'a1.jpeg', 1, 8250, 50),
(8, 'commande-panier-40113115265', 'commande-lignepanier-3702155', 'modele-5319855', 'carte cool', 'carton', 'a1.jpeg', 1, 10000, 100),
(9, 'commande-panier-40113115265', 'commande-lignepanier-4252250', 'modele-5784255', 'carte cool', 'pvc', 'a2.jpeg', 1, 9750, 50);

-- --------------------------------------------------------

--
-- Structure de la table `message_contact`
--

CREATE TABLE `message_contact` (
  `id` int NOT NULL,
  `code_message` varchar(255) NOT NULL,
  `nom_prenom` varchar(255) NOT NULL,
  `destinateur` varchar(255) NOT NULL,
  `destinataire` varchar(255) NOT NULL,
  `sujet` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `message_contact`
--

INSERT INTO `message_contact` (`id`, `code_message`, `nom_prenom`, `destinateur`, `destinataire`, `sujet`, `message`, `date_register`) VALUES
(1, 'MessageContact-31298686', 'Medi Michael', 'yannmicke@gmail.com', 'hyperformix3@gmail.com', 'Transfert de nom de domaine', 'Salut', '2020-11-25 10:04:27');

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191007114020', '2019-10-07 11:40:38');

-- --------------------------------------------------------

--
-- Structure de la table `modele_carte`
--

CREATE TABLE `modele_carte` (
  `id` int NOT NULL,
  `code_modele` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_fichier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `modele_carte`
--

INSERT INTO `modele_carte` (`id`, `code_modele`, `nom`, `type_carte`, `lien_fichier`) VALUES
(101, 'modele-5319855', 'Modèle CARTON A1', 'carton', 'a1.jpeg'),
(102, 'modele-2484513', 'Modèle CARTON A2', 'carton', 'a2.jpeg'),
(103, 'modele-9733637', 'Modèle CARTON A3', 'carton', 'a3.jpeg'),
(104, 'modele-7925338', 'Modèle CARTON A4', 'carton', 'a4.jpeg'),
(105, 'modele-2548441', 'Modèle CARTON A5', 'carton', 'a5.jpeg'),
(106, 'modele-1585983', 'Modèle CARTON A6', 'carton', 'a6.jpeg'),
(107, 'modele-9604967', 'Modèle PVC A1', 'pvc', 'a1.jpeg'),
(108, 'modele-5784255', 'Modèle PVC A2', 'pvc', 'a2.jpeg'),
(109, 'modele-4648631', 'Modèle PVC A3', 'pvc', 'a3.jpeg'),
(110, 'modele-3313353', 'Modèle PVC A4', 'pvc', 'a4.jpeg'),
(111, 'modele-3099496', 'Modèle PVC A5', 'pvc', 'a5.jpeg'),
(112, 'modele-9453906', 'Modèle PVC A6', 'pvc', 'a6.jpeg'),
(113, 'modele-8677604', 'Modèle CARTON B1', 'carton', 'b1.jpeg'),
(114, 'modele-1649458', 'Modèle CARTON B2', 'carton', 'b2.jpeg'),
(115, 'modele-3290392', 'Modèle CARTON B3', 'carton', 'b3.jpeg'),
(116, 'modele-9267650', 'Modèle CARTON B4', 'carton', 'b4.jpeg'),
(117, 'modele-8443430', 'Modèle CARTON B5', 'carton', 'b5.jpeg'),
(118, 'modele-1136646', 'Modèle CARTON B6', 'carton', 'b6.jpeg'),
(119, 'modele-2747186', 'Modèle PVC B1', 'pvc', 'b1.jpeg'),
(120, 'modele-5523502', 'Modèle PVC B2', 'pvc', 'b2.jpeg'),
(121, 'modele-7492186', 'Modèle PVC B3', 'pvc', 'b3.jpeg'),
(122, 'modele-6114186', 'Modèle PVC B4', 'pvc', 'b4.jpeg'),
(123, 'modele-6795892', 'Modèle PVC B5', 'pvc', 'b5.jpeg'),
(124, 'modele-9781749', 'Modèle PVC B6', 'pvc', 'b6.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE `panier` (
  `id` int NOT NULL,
  `code_panier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix_total` double NOT NULL,
  `reduction` double NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`id`, `code_panier`, `prix_total`, `reduction`, `code_client`, `date_register`) VALUES
(50, 'panier-294980677971', 18000, 0, 'client-4981', '2020-11-27 11:26:37');

-- --------------------------------------------------------

--
-- Structure de la table `panier_commande`
--

CREATE TABLE `panier_commande` (
  `id` int NOT NULL,
  `code_panier_commande` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix_total` double NOT NULL,
  `reduction` double NOT NULL,
  `code_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `panier_commande`
--

INSERT INTO `panier_commande` (`id`, `code_panier_commande`, `prix_total`, `reduction`, `code_client`, `date_register`) VALUES
(3, 'commande-panier-5712726', 13000, 5000, 'client-1724', '2020-02-28 21:04:27'),
(4, 'commande-panier-2748533', 34250, 5000, 'client-1724', '2020-02-28 22:07:29'),
(5, 'commande-panier-8980268', 8250, 0, 'client-1724', '2020-10-16 01:39:10'),
(6, 'commande-panier-40113115265', 19750, 0, 'client-4981', '2020-11-24 13:13:15');

-- --------------------------------------------------------

--
-- Structure de la table `parrain`
--

CREATE TABLE `parrain` (
  `id` int NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_parrain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hachac` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `parrain`
--

INSERT INTO `parrain` (`id`, `nom`, `prenom`, `code_parrain`, `hachac`) VALUES
(18, '', '', 'client-4981', 'c31fc0deb647af076c1a4b47f97632b8e917ff98c7e4684312dc8b282ada36790c490f452d85b13d6bd66648dc298c51030fa2af42dddfca20630a5b7bc1cb79'),
(19, '', '', 'client-2630', '4c4d4d8ffa660caf8e0307c5dbf9303bd035bf2dacf197245c82e9721829d122e78e23f40338a1c273dd21e1cb63b5d592740573275806d74debd548c33ccf99');

-- --------------------------------------------------------

--
-- Structure de la table `prestataire`
--

CREATE TABLE `prestataire` (
  `id` int NOT NULL,
  `code_presta` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reduction`
--

CREATE TABLE `reduction` (
  `id` int NOT NULL,
  `code_reduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte_reduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant_reduction` double NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `relation_panier_reduction`
--

CREATE TABLE `relation_panier_reduction` (
  `id` int NOT NULL,
  `code_rpr` varchar(255) NOT NULL COMMENT 'codeRelationPanierReduction',
  `code_panier` varchar(255) NOT NULL,
  `code_bdr` varchar(255) NOT NULL,
  `code_reduction` varchar(255) NOT NULL,
  `somme_reduction` float NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `relation_panier_reduction`
--

INSERT INTO `relation_panier_reduction` (`id`, `code_rpr`, `code_panier`, `code_bdr`, `code_reduction`, `somme_reduction`, `date_register`) VALUES
(7, 'RelationPanierReduction-3514247', 'panier-2805536', 'BonDeReduction-13104', 'test', 5000, '2020-01-16 00:07:54'),
(8, 'RelationPanierReduction-9203188', 'panier-4727375', 'BonDeReduction-13104', 'test', 5000, '2020-01-16 00:32:12'),
(9, 'RelationPanierReduction-9724395', 'panier-9433424', 'BonDeReduction-13104', 'test', 5000, '2020-01-16 01:56:07'),
(12, 'RelationPanierReduction-7109674', 'panier-3146317', 'BonDeReduction-13104', 'test', 5000, '2020-02-28 19:03:55'),
(13, 'RelationPanierReduction-4757427', 'panier-5712726', 'BonDeReduction-13104', 'test', 5000, '2020-02-28 21:04:33'),
(14, 'RelationPanierReduction-9978249', 'panier-2748533', 'BonDeReduction-13104', 'test', 5000, '2020-02-28 22:08:00');

-- --------------------------------------------------------

--
-- Structure de la table `root`
--

CREATE TABLE `root` (
  `id` int NOT NULL,
  `code_root` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `passpecial` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `root`
--

INSERT INTO `root` (`id`, `code_root`, `nom`, `prenom`, `passpecial`, `date_register`) VALUES
(1, 'root-9', 'medi', 'yann michael', 'carion', '2019-10-17 10:15:08');

-- --------------------------------------------------------

--
-- Structure de la table `superviseur`
--

CREATE TABLE `superviseur` (
  `id` int NOT NULL,
  `code_superviseur` varchar(255) NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `superviseur`
--

INSERT INTO `superviseur` (`id`, `code_superviseur`, `photo`, `nom`, `prenom`, `genre`, `email`, `telephone`, `date_register`) VALUES
(2, 'superviseur-9808', '', 'YANN', 'MEDI', 'masculin', 'mygeasscorp@gmail.com', '0', '2020-08-26 00:01:34');

-- --------------------------------------------------------

--
-- Structure de la table `tarif_carte`
--

CREATE TABLE `tarif_carte` (
  `id` int NOT NULL,
  `code_tarif` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_carte` int NOT NULL,
  `tarif_carte` double NOT NULL,
  `tarif_perso` double NOT NULL,
  `date_register` datetime NOT NULL,
  `tarif_pack` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tarif_carte`
--

INSERT INTO `tarif_carte` (`id`, `code_tarif`, `type_carte`, `nb_carte`, `tarif_carte`, `tarif_perso`, `date_register`, `tarif_pack`) VALUES
(17, 'tarif-2565029', 'carton', 50, 65, 5000, '2019-11-16 11:33:49', 8250),
(18, 'tarif-2730151', 'carton', 100, 50, 5000, '2019-11-16 11:33:49', 10000),
(19, 'tarif-8193096', 'carton', 250, 40, 5000, '2019-11-16 11:33:49', 15000),
(20, 'tarif-2433030', 'carton', 500, 35, 5000, '2019-11-16 11:33:49', 22500),
(21, 'tarif-8317822', 'pvc', 50, 75, 6000, '2019-11-16 11:33:49', 9750),
(22, 'tarif-7222448', 'pvc', 100, 70, 6000, '2019-11-16 11:33:49', 13000),
(23, 'tarif-7204073', 'pvc', 250, 60, 6000, '2019-11-16 11:33:49', 21000),
(24, 'tarif-7623904', 'pvc', 500, 55, 6000, '2019-11-16 11:33:49', 33500),
(25, 'tarif-9111909', 'carton', 50, 65, 5000, '2020-01-03 12:34:20', 8250),
(26, 'tarif-9297171', 'carton', 100, 50, 5000, '2020-01-03 12:34:20', 10000),
(27, 'tarif-4064820', 'carton', 250, 40, 5000, '2020-01-03 12:34:20', 15000),
(28, 'tarif-5015419', 'carton', 500, 35, 5000, '2020-01-03 12:34:20', 22500),
(29, 'tarif-6641846', 'pvc', 50, 75, 6000, '2020-01-03 12:34:20', 9750),
(30, 'tarif-9653175', 'pvc', 100, 70, 6000, '2020-01-03 12:34:20', 13000),
(31, 'tarif-1555398', 'pvc', 250, 60, 6000, '2020-01-03 12:34:20', 21000),
(32, 'tarif-5742881', 'pvc', 500, 55, 6000, '2020-01-03 12:34:20', 33500);

-- --------------------------------------------------------

--
-- Structure de la table `template`
--

CREATE TABLE `template` (
  `id` int NOT NULL,
  `code_template` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `chemin` varchar(255) NOT NULL,
  `nb_utillisation` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `template`
--

INSERT INTO `template` (`id`, `code_template`, `nom`, `chemin`, `nb_utillisation`) VALUES
(1, '', 'BreezyCv Dark 1', '/template/breezycv/dark/1', 0),
(2, '', 'BreezyCv Dark 2', '/template/breezycv/dark/2', 0),
(3, '', 'BreezyCv Dark 3', '/template/breezycv/dark/3', 0),
(4, '', 'BreezyCv Dark 4', '/template/breezycv/dark/4', 0),
(5, '', 'BreezyCv Dark 5', '/template/breezycv/dark/5', 0),
(6, '', 'BreezyCv Dark 6', '/template/breezycv/dark/6', 0),
(13, '', 'BreezyCv Light 1', '/template/breezycv/light/1', 0),
(14, '', 'BreezyCv Light 2', '/template/breezycv/light/2', 0),
(15, '', 'BreezyCv Light 3', '/template/breezycv/light/3', 0),
(16, '', 'BreezyCv Light 4', '/template/breezycv/light/4', 0),
(17, '', 'BreezyCv Light 5', '/template/breezycv/light/5', 0),
(18, '', 'BreezyCv Light 6', '/template/breezycv/light/6', 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `code_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lienperso` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `autorisation` int NOT NULL DEFAULT '1',
  `permission_modification_data` int NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `code_user`, `username`, `password`, `nom`, `prenom`, `genre`, `lienperso`, `status`, `autorisation`, `permission_modification_data`, `date_register`) VALUES
(6, 'root-9', 'root@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', 'medi', 'yann michael', '', '', 'root', 1, 1, '2019-10-17 10:15:08'),
(47, 'superviseur-9808', 'mygeasscorp@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', 'YANN', 'MEDI', '', '', 'superviseur', 1, 0, '2020-08-26 00:01:34'),
(58, 'client-4981', 'hyperformix3@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', 'MEDI', 'YANN MICHAEL', 'masculin', '', 'client', 1, 1, '2020-11-12 14:15:55'),
(59, 'client-2630', 'nanankouassi9@gmail.com', '3e908a3c1b805bc28e2ca399e5ac43d9b16bf91e4cde4241f39d732e8224383853018e31097e5fe4e2dae736fda54754be5fead95832f3a83ede11287c76ca27', 'Kouassi', 'Nanan', 'masculin', '', 'client', 0, 0, '2020-11-12 14:31:17');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `abonnement`
--
ALTER TABLE `abonnement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `attente_validation`
--
ALTER TABLE `attente_validation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bon_de_reduction`
--
ALTER TABLE `bon_de_reduction`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `citation`
--
ALTER TABLE `citation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact_client`
--
ALTER TABLE `contact_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cursus`
--
ALTER TABLE `cursus`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `divers`
--
ALTER TABLE `divers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `filleul`
--
ALTER TABLE `filleul`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_fichier_client`
--
ALTER TABLE `info_fichier_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_perso`
--
ALTER TABLE `info_perso`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_resocial_client`
--
ALTER TABLE `info_resocial_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_societe_client`
--
ALTER TABLE `info_societe_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ligne_panier`
--
ALTER TABLE `ligne_panier`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ligne_panier_commande`
--
ALTER TABLE `ligne_panier_commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message_contact`
--
ALTER TABLE `message_contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `modele_carte`
--
ALTER TABLE `modele_carte`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `panier`
--
ALTER TABLE `panier`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `panier_commande`
--
ALTER TABLE `panier_commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `parrain`
--
ALTER TABLE `parrain`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prestataire`
--
ALTER TABLE `prestataire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reduction`
--
ALTER TABLE `reduction`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `relation_panier_reduction`
--
ALTER TABLE `relation_panier_reduction`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `root`
--
ALTER TABLE `root`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `superviseur`
--
ALTER TABLE `superviseur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tarif_carte`
--
ALTER TABLE `tarif_carte`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `abonnement`
--
ALTER TABLE `abonnement`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `attente_validation`
--
ALTER TABLE `attente_validation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `bon_de_reduction`
--
ALTER TABLE `bon_de_reduction`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `citation`
--
ALTER TABLE `citation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT pour la table `contact_client`
--
ALTER TABLE `contact_client`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `cursus`
--
ALTER TABLE `cursus`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `divers`
--
ALTER TABLE `divers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `experience`
--
ALTER TABLE `experience`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `filleul`
--
ALTER TABLE `filleul`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `info_fichier_client`
--
ALTER TABLE `info_fichier_client`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT pour la table `info_perso`
--
ALTER TABLE `info_perso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `info_resocial_client`
--
ALTER TABLE `info_resocial_client`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `info_societe_client`
--
ALTER TABLE `info_societe_client`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `ligne_panier`
--
ALTER TABLE `ligne_panier`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT pour la table `ligne_panier_commande`
--
ALTER TABLE `ligne_panier_commande`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `message_contact`
--
ALTER TABLE `message_contact`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `modele_carte`
--
ALTER TABLE `modele_carte`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT pour la table `panier`
--
ALTER TABLE `panier`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `panier_commande`
--
ALTER TABLE `panier_commande`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `parrain`
--
ALTER TABLE `parrain`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `prestataire`
--
ALTER TABLE `prestataire`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reduction`
--
ALTER TABLE `reduction`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `relation_panier_reduction`
--
ALTER TABLE `relation_panier_reduction`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `root`
--
ALTER TABLE `root`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `superviseur`
--
ALTER TABLE `superviseur`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tarif_carte`
--
ALTER TABLE `tarif_carte`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `template`
--
ALTER TABLE `template`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;