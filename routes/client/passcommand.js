const express = require('express')
const router = express.Router()

const ModeleCarte = require('../../models/ModeleCarte')
const TarifCarte = require('../../models/TarifCarte')
const Panier = require('../../models/Panier')
const LignePanier = require('../../models/LignePanier')
const PanierCommande = require('../../models/PanierCommande')
const LignePanierCommande = require('../../models/LignePanierCommande')
const Commande = require('../../models/Commande')
const Client = require('../../models/Client')

const env = require('../../config/config').env
const Carte = require('../../config/carte')
const BonDeReduction = require('../../models/BonDeReduction')
const RelationPanierReduction = require('../../models/RelationPanierReduction')
//const Panier= require('../../config/panier')

const { activeSidebare } = require('../../config/sidebare')
const { dclient } = require('../../config/tabsidebase')
const idpage = 4
const tabside = dclient()
activeSidebare(tabside, idpage)

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let panier = sess.panier
        let page = "Passer Commande"

        Client.findByOneField('code_client', sess.codeUser, (futclient) => {
            let client = futclient[0]

            if (client.nom !== "" && client.prenom !== "" && client.email !== "" && client.adresse !== "" && client.phone1 !== 0) {
                let peutcommander = "yes"
                res.render('client/passcommand', {
                    title: 'GeassCard: Compte Client => Passer Commande',
                    active: 5,
                    ident: ident,
                    role: role,
                    page: page,
                    peutcommander: peutcommander,
                    tabside: tabside
                })
            } else {
                let peutcommander = "no"
                res.render('client/passcommand', {
                    title: 'GeassCard: Compte Client => Passer Commande',
                    active: 5,
                    ident: ident,
                    role: role,
                    page: page,
                    peutcommander: peutcommander,
                    tabside: tabside
                })
            }
        })



    } else {
        res.redirect('/')
    }

})

router.get('/panier', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        Panier.findByOneField('code_client', sess.codeUser, (futpanier) => {
            console.log(futpanier);

            if (futpanier.length === 1) {
                let panier = futpanier[0]
                LignePanier.findByOneField('code_panier', panier.codePanier, (futlignepanier) => {
                    if (futlignepanier.length > 0) {
                        let info = "yes"

                        Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', panier.codePanier, (futcommande) => {
                            if (futcommande.length !== 0) {
                                console.log(futcommande[0])

                                futcommande.forEach(commande => {
                                    Commande.remove(commande, (msg) => {
                                        console.log(msg)
                                    })
                                });


                            }
                        })

                        res.render('client/pcmd/panier', {
                            title: 'GeassCard: Compte Client => Passer Commande => Mon Panier',
                            active: 5,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            panier: panier,
                            lignepaniers: futlignepanier,
                            tabside: tabside
                        })
                    } else {
                        let info = "no"
                        res.render('client/pcmd/panier', {
                            title: 'GeassCard: Compte Client => Passer Commande => Mon Panier',
                            active: 5,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            tabside: tabside
                        })
                    }
                })
            } else {
                let info = "no"
                res.render('client/pcmd/panier', {
                    title: 'GeassCard: Compte Client => Passer Commande => Mon Panier',
                    active: 5,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    tabside: tabside
                })
            }
        })
    } else {
        res.redirect('/')
    }
})

router.get('/mescommandes', (req, res, next) => {
    let sess = req.session
    let ident = sess.nom
    let role = sess.status
    let page = "Passer Commande"

    Commande.findByOneField("code_client", sess.codeUser, (futcommande) => {
        if (futcommande.length >= 1) {
            let commandes = []

            futcommande.forEach(commande => {
                console.log(commande.statut)
                if (commande.statut === '1' || commande.statut == "2" || commande.statut == "3") {

                    commandes.push(commande)

                }
            })

            console.log(commandes)

            if (commandes.length >= 1) {
                let info = "yes"
                res.render('client/pcmd/mescommandes', {
                    title: 'GeassCard: Compte Client => Passer Commande => Mes Commandes En Cours...',
                    active: 5,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    commandes: commandes,
                    tabside: tabside
                })
            } else {
                let info = "no"
                res.render('client/pcmd/mescommandes', {
                    title: 'GeassCard: Compte Client => Passer Commande => Mes Commandes En Cours...',
                    active: 5,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    tabside: tabside
                })
            }

        } else {
            let info = "no"
            res.render('client/pcmd/mescommandes', {
                title: 'GeassCard: Compte Client => Passer Commande => Mes Commandes',
                active: 5,
                ident: ident,
                role: role,
                page: page,
                info: info,
                tabside: tabside
            })
        }
    })
})

router.get('/mescommandes/details/:codecommande', (req, res, next) => {
    let sess = req.session
    let ident = sess.nom
    let role = sess.status
    let page = "Passer Commande"

    let codecommande = req.params.codecommande

    Commande.findByOneField('code_commande', codecommande, (futcommande) => {
        if (futcommande.length === 1) {

            let commande = futcommande[0]

            PanierCommande.findByOneField('code_panier_commande', commande.codePanier, (futpanier) => {
                let panier = futpanier[0]
                console.log(commande.codePanier)
                console.log(panier)
                
                //let panier = futpanier[0]

                LignePanierCommande.findByOneField('code_panier_commande', panier.codePanierCommande, (futlignepanier) => {
                    let touteslignes = futlignepanier
                    console.log(touteslignes);

                    Client.findByOneField('code_client', commande.codeClient, (futclient) => {
                        let client = futclient[0]

                        res.render('client/pcmd/info_commande', {
                            title: 'GeassCard: Compte Client => Gestion Commandes',
                            //tabside: tabside,
                            ident: ident,
                            role: role,
                            page: page,
                            infoclient: client,
                            panier: panier,
                            commande: commande,
                            touteslignes: touteslignes,
                            tabside: tabside
                        })
                    })
                })
            })

        } else {
            res.redirect('/compte/root/gestion_commande')
        }
    })

})

router.get('/panier/:codepanier/resume', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let codepanier = req.params.codepanier

        Panier.findByOneField('code_panier', codepanier, (futpanier) => {
            if (futpanier.length === 1) {
                let panier = futpanier[0]
                Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futcommande) => {
                    if (futcommande.length === 1) {
                        let commande = futcommande[0]
                        console.log(commande);

                        if (commande.moyenPaiement !== "") {
                            LignePanier.findByOneField('code_panier', codepanier, (futlignepanier) => {
                                RelationPanierReduction.findByOneField('code_panier', codepanier, (futrpr) => {
                                    let disporeduct = "yes"

                                    res.render('client/pcmd/resume', {
                                        title: 'GeassCard: Compte Client => Passer Commande',
                                        active: 5,
                                        ident: ident,
                                        role: role,
                                        page: page,
                                        panier: panier,
                                        lignepaniers: futlignepanier,
                                        reductions: futrpr,
                                        commande: commande,
                                        tabside: tabside
                                    })
                                })
                            })
                        } else {
                            res.redirect('/compte/client/passcommand/panier/modepaiement/' + codepanier)
                        }
                    } else {
                        res.redirect('/compte/client/passcommand/panier')
                    }
                })
            } else {
                res.redirect('/compte/client/passcommand')
            }

        })



    } else {
        res.redirect('/')
    }
})

router.get('/panier/:codepanier/confirm', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let panier = sess.panier
        let page = "Passer Commande"



        Client.findByOneField('code_client', sess.codeUser, (futclient) => {
            let client = futclient[0]

            if (client.nom !== "" && client.prenom !== "" && client.email !== "" && client.adresse !== "" && client.phone1 !== 0) {
                let peutcommander = "yes"
                res.render('client/passcommand', {
                    title: 'GeassCard: Compte Client => Passer Commande',
                    active: 5,
                    ident: ident,
                    role: role,
                    page: page,
                    peutcommander: peutcommander,
                    tabside: tabside
                })
            } else {
                let peutcommander = "no"
                res.render('client/passcommand', {
                    title: 'GeassCard: Compte Client => Passer Commande',
                    active: 5,
                    ident: ident,
                    role: role,
                    page: page,
                    peutcommander: peutcommander,
                    tabside: tabside
                })
            }
        })



    } else {
        res.redirect('/')
    }
})

router.get('/panier/validreduction/:codepanier', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let codepanier = req.params.codepanier

        Panier.findByOneField('code_panier', codepanier, (futpanier) => {
            let panier = futpanier[0]
            if (futpanier.length === 1) {
                LignePanier.findByOneField('code_panier', panier.codePanier, (futligne) => {
                    let nbligne = futligne.length

                    Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futcommande) => {
                        if (futcommande.length === 0) {
                            let codec = Commande.genCodeCommande(sess.codeUser)
                            let commande = {
                                codeCommande: codec,
                                codeClient: sess.codeUser,
                                codePanier: codepanier,
                                coutTotal: panier.prixTotal
                            }
                            Commande.create(commande, (msg) => {
                                console.log(msg)
                                console.log(commande)
                            })
                        }
                    })

                    RelationPanierReduction.findByOneField('code_panier', codepanier, (futrpr) => {
                        let disporeduct = "yes"

                        res.render('client/pcmd/validreduction', {
                            title: 'GeassCard: Compte Client => Passer Commande',
                            active: 5,
                            ident: ident,
                            role: role,
                            page: page,
                            nbligne: nbligne,
                            panier: panier,
                            lignepaniers: futligne,
                            reductions: futrpr,
                            disporeduct: disporeduct,
                            tabside: tabside
                        })

                    })

                })

            } else {
                res.redirect('/compte/client/passcommand/panier')
            }
        })
    } else {
        res.redirect('/')
    }
})

router.post('/panier/validreduction/:codepanier', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let panier = sess.panier
        let page = "Passer Commande"

        let codepanier = req.params.codepanier

        Panier.findByOneField('code_panier', codepanier, (futpanier) => {
            if (futpanier.length === 1) {
                let panier = futpanier[0]
                let data = req.body
                let codereduction = data.code

                let error = ""
                let info = data


                BonDeReduction.findByOneField('code_reduction', codereduction, (futreduction) => {
                    if (futreduction.length === 1) {
                        let reduction = futreduction[0]
                        if (reduction.activeReduction === 1) {
                            RelationPanierReduction.findByTwoField('code_bdr', reduction.codeBdr, 'code_panier', codepanier, (futrelationpanierreduction) => {
                                if (futrelationpanierreduction.length === 0) {
                                    LignePanier.findByOneField('code_panier', panier.codePanier, (futligne) => {
                                        let nbligne = futligne.length

                                        let sommeReduction = panier.reduction + reduction.sommeReduction
                                        Panier.replaceByOneField(panier.codePanier, 'reduction', sommeReduction, (msg) => {
                                            console.log(msg)
                                        })



                                        let rpr = {
                                            codeRpr: RelationPanierReduction.genCodeRpr(),
                                            codePanier: panier.codePanier,
                                            codeBdr: reduction.codeBdr,
                                            codeReduction: reduction.codeReduction,
                                            sommeReduction: reduction.sommeReduction
                                        }

                                        RelationPanierReduction.create(rpr, (msg) => {
                                            console.log(msg)
                                        })

                                        Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futcommande) => {
                                            if (futcommande.length === 0) {
                                                let codec = Commande.genCodeCommande(sess.codeUser)
                                                let coutcmd = panier.prixTotal - panier.reduction
                                                let commande = {
                                                    codeCommande: codec,
                                                    codeClient: sess.codeUser,
                                                    codePanier: codepanier,
                                                    coutTotal: coutcmd
                                                }
                                                Commande.create(commande, (msg) => {
                                                    console.log(msg)
                                                    console.log(commande)
                                                })
                                            }
                                        })

                                        res.redirect('/compte/client/passcommand/panier/validreduction/' + codepanier)
                                    })
                                } else {
                                    LignePanier.findByOneField('code_panier', panier.codePanier, (futligne) => {
                                        let nbligne = futligne.length

                                        Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futcommande) => {
                                            if (futcommande.length === 0) {
                                                let codec = Commande.genCodeCommande(sess.codeUser)
                                                let commande = {
                                                    codeCommande: codec,
                                                    codeClient: sess.codeUser,
                                                    codePanier: codepanier,
                                                    coutTotal: panier.prixTotal
                                                }
                                                Commande.create(commande, (msg) => {
                                                    console.log(msg)
                                                    console.log(commande)
                                                })
                                            }
                                        })

                                        let error = "Desolé, Ce bon de réduction a déja été utilisé ici"

                                        res.render('client/pcmd/validreduction', {
                                            title: 'GeassCard: Compte Client => Passer Commande',
                                            active: 5,
                                            ident: ident,
                                            role: role,
                                            page: page,
                                            nbligne: nbligne,
                                            panier: panier,
                                            lignepaniers: futligne,
                                            info: info,
                                            error: error,
                                            tabside: tabside
                                        })
                                    })
                                }
                            })
                        } else {
                            LignePanier.findByOneField('code_panier', panier.codePanier, (futligne) => {
                                let nbligne = futligne.length

                                Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futcommande) => {
                                    if (futcommande.length === 0) {
                                        let codec = Commande.genCodeCommande(sess.codeUser)
                                        let commande = {
                                            codeCommande: codec,
                                            codeClient: sess.codeUser,
                                            codePanier: codepanier,
                                            coutTotal: panier.prixTotal
                                        }
                                        Commande.create(commande, (msg) => {
                                            console.log(msg)
                                            console.log(commande)
                                        })
                                    }
                                })

                                let error = "Desolé, Ce bon de réduction n'a pas encore été activé"

                                res.render('client/pcmd/validreduction', {
                                    title: 'GeassCard: Compte Client => Passer Commande',
                                    active: 5,
                                    ident: ident,
                                    role: role,
                                    page: page,
                                    nbligne: nbligne,
                                    panier: panier,
                                    lignepaniers: futligne,
                                    info: info,
                                    error: error,
                                    tabside: tabside
                                })
                            })
                        }
                    } else {
                        LignePanier.findByOneField('code_panier', panier.codePanier, (futligne) => {
                            let nbligne = futligne.length

                            Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futcommande) => {
                                if (futcommande.length === 0) {
                                    let codec = Commande.genCodeCommande(sess.codeUser)
                                    let commande = {
                                        codeCommande: codec,
                                        codeClient: sess.codeUser,
                                        codePanier: codepanier,
                                        coutTotal: panier.prixTotal
                                    }
                                    Commande.create(commande, (msg) => {
                                        console.log(msg)
                                        console.log(commande)
                                    })
                                }
                            })

                            let error = "Désolé, Ce bon de réduction n'existe pas"

                            res.render('client/pcmd/validreduction', {
                                title: 'GeassCard: Compte Client => Passer Commande',
                                active: 5,
                                ident: ident,
                                role: role,
                                page: page,
                                nbligne: nbligne,
                                panier: panier,
                                lignepaniers: futligne,
                                info: info,
                                error: error,
                                tabside: tabside
                            })
                        })
                    }
                })

            } else {
                res.redirect('/compte/client/passcommand/panier')
            }
        })

    } else {
        res.redirect('/')
    }
})

router.get('/panier/modepaiement/:codepanier', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let codepanier = req.params.codepanier

        Panier.findByOneField('code_panier', codepanier, (futpanier) => {
            let panier = futpanier[0]
            if (futpanier.length === 1) {
                LignePanier.findByOneField('code_panier', panier.codePanier, (futligne) => {
                    let nbligne = futligne.length

                    let message = "Désolé Pas Encore Disponible, Merci!!!"

                    res.render('client/pcmd/modepaiement', {
                        title: 'GeassCard: Compte Client => Passer Commande',
                        active: 5,
                        ident: ident,
                        role: role,
                        page: page,
                        nbligne: nbligne,
                        panier: panier,
                        lignepaniers: futligne,
                        message: message,
                        tabside: tabside
                    })
                })

            } else {
                res.redirect('/compte/client/passcommand/panier')
            }
        })
    } else {
        res.redirect('/')
    }
})

router.get('/panier/modepaiement/:codepanier/:modepaiement', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let codepanier = req.params.codepanier
        let modepaiement = req.params.modepaiement

        Panier.findByOneField('code_panier', codepanier, (futpanier) => {
            let panier = futpanier[0]
            if (futpanier.length === 1) {
                switch (modepaiement) {
                    case "pay_livraison":
                        Commande.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futcommande) => {
                            console.log(futcommande);

                            if (futcommande.length === 1) {
                                console.log(futcommande[0]);

                                Commande.replaceByOneField(futcommande[0].codeCommande, 'moyen_paiement', "À La Livraison", (msg) => {
                                    console.log(msg)
                                })
                                res.redirect('/compte/client/passcommand/panier/' + codepanier + '/resume')
                            } else {
                                res.redirect('/compte/client/passcommand/panier/modepaiement/' + codepanier)
                            }
                        })
                        break;
                    case "pay_mobile":
                        res.redirect('/compte/client/passcommand/panier/modepaiement/' + codepanier)
                        break;
                    case "pay_bank":
                        res.redirect('/compte/client/passcommand/panier/modepaiement/' + codepanier)
                        break;

                    default:
                        res.redirect('/compte/client/passcommand/panier/modepaiement/' + codepanier)
                        break;
                }

            } else {
                res.redirect('/compte/client/passcommand/panier')
            }
        })
    } else {
        res.redirect('/')
    }
})

router.get('/panier/:codepanier/validcommande/:codecommande', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        let codepanier = req.params.codepanier
        let codecommande = req.params.codecommande

        Panier.findByOneField('code_panier', codepanier, (futpanier) => {
            if (futpanier.length === 1) {
                Commande.findByThreeField('code_commande', codecommande, 'code_panier', codepanier, 'confirmation_client', 0, (futcommande) => {
                    if (futcommande.length === 1) {
                        LignePanier.findByOneField('code_panier', codepanier, (futlignepanier) => {
                            if (futlignepanier.length > 0) {
                                futlignepanier.forEach(lignepanier => {
                                    LignePanierCommande.create({
                                            codeLpCommande: "commande-" + lignepanier.codeLp,
                                            codePanierCommande: "commande-" + codepanier,
                                            codeModele: lignepanier.codeModele,
                                            lienFichier: lignepanier.lienFichier,
                                            nom: lignepanier.nom,
                                            typeCarte: lignepanier.typeCarte,
                                            quantite: lignepanier.quantite,
                                            nbcarte: lignepanier.nbcarte,
                                            prix: lignepanier.prix
                                        },
                                        (msg) => {
                                            console.log(msg)
                                        }
                                    )

                                    LignePanier.remove(lignepanier, (msg) => {
                                        console.log(msg)
                                    })
                                });
                            }
                        })
                        Panier.findByOneField('code_panier', codepanier, (futurpanier) => {
                            PanierCommande.create({
                                codePanierCommande: "commande-" + codepanier,
                                codeClient: futurpanier[0].codeClient,
                                reduction: futurpanier[0].reduction,
                                prixTotal: futurpanier[0].prixTotal,
                                dateRegister: futurpanier[0].dateRegister
                            }, (msg) => {
                                console.log(msg)
                            })
                            Commande.replaceByOneField(codecommande, 'cout_total', futurpanier[0].prixTotal - futurpanier[0].reduction, (msg) => {
                                console.log(msg)
                            })
                            Panier.remove(futurpanier[0], (msg) => {
                                console.log(msg)
                            })
                        })
                        Commande.replaceByOneField(codecommande, 'code_panier', "commande-" + codepanier, (msg) => {
                            console.log(msg)
                        })
                        Commande.replaceByOneField(codecommande, 'confirmation_client', 1, (msg) => {
                            console.log(msg)
                        })


                        res.redirect('/compte/client/passcommand/panier')



                    } else {
                        res.redirect('/compte/client/passcommand')
                    }
                })
            } else {
                res.redirect('/compte/client/passcommand')
            }
        })




    } else {
        res.redirect('/')
    }
})

router.get('/panier/delpan/:codepanier', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let codepanier = req.params.codepanier

        Panier.findByTwoField('code_client', sess.codeUser, 'code_panier', codepanier, (futpanier) => {
            console.log(futpanier)
            if (futpanier.length === 1) {
                let panier = futpanier[0]
                LignePanier.findByOneField('code_panier', panier.codePanier, (futlignepanier) => {
                    futlignepanier.forEach(lignepanier => {
                        LignePanier.remove(lignepanier, (msg) => {
                            console.log(msg)
                        })
                    });
                })
                RelationPanierReduction.findByOneField('code_panier', panier.codePanier, (futrpr) => {
                    if (futrpr.length > 0) {
                        futrpr.forEach(rpr => {
                            RelationPanierReduction.remove(rpr, (msg) => {
                                console.log(msg)
                            })
                        });
                    }
                })
                Panier.remove(panier, (msg) => {
                    console.log(msg)
                })
                res.redirect('/compte/client/passcommand/panier')
            } else {
                res.redirect('/compte/client/passcommand')
            }
        })
    } else {
        res.redirect('/')
    }
})

router.get('/panier/deline/:codeligne', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let codeligne = req.params.codeligne

        Panier.findByOneField('code_client', sess.codeUser, (futpanier) => {
            if (futpanier.length === 1) {
                let panier = futpanier[0]
                LignePanier.findByOneField('code_panier', panier.codePanier, (futligne) => {
                    if (futligne.length > 1) {
                        LignePanier.findByOneField('code_lp', codeligne, (futlignepanier) => {
                            if (futlignepanier.length === 1) {

                                let panier = futpanier[0]
                                let ligne = futlignepanier[0]
                                let prixligne = ligne.prix
                                let prixtotal = panier.prixTotal - prixligne

                                console.log(panier);
                                console.log(ligne);


                                Panier.replaceByOneField(panier.codePanier, 'prix_total', prixtotal, (msg) => {
                                    console.log(msg)
                                })
                                LignePanier.remove(ligne, (msg) => {
                                    console.log(msg)
                                })

                                res.redirect('/compte/client/passcommand/panier')
                            } else {
                                res.redirect('/compte/client/passcommand/panier')
                            }
                        })

                    } else {
                        let ligne = futligne[0]
                        let panier = futpanier[0]

                        console.log(panier);
                        console.log(ligne);

                        LignePanier.remove(ligne, (msg) => {
                            console.log(msg)
                        })
                        Panier.remove(panier, (msg) => {
                            console.log(msg)
                        })

                        res.redirect('/compte/client/passcommand/panier')
                    }

                })
            } else {
                res.redirect('/compte/client/passcommand/panier')
            }
        })
    } else {
        res.redirect('/')
    }
})


router.get('/:typecarte', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let typecarte = req.params.typecarte

        switch (typecarte) {
            case "typecarton":

                ModeleCarte.findByOneField('type_carte', 'carton', (modeles) => {
                    if (modeles.length >= 0) {
                        let info = "yes"
                        res.render('client/pcmd/typecarton', {
                            title: 'GeassCard: Compte Client => Passer Commande',
                            active: 5,
                            ident: ident,
                            role: role,
                            page: page,
                            modeles: modeles,
                            info: info,
                            tabside: tabside
                        })
                    } else {
                        let info = "no"
                        res.render('client/pcmd/typecarton', {
                            title: 'GeassCard: Compte Client => Passer Commande',
                            active: 5,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            tabside: tabside
                        })
                    }

                })
                break;

            case "typepvc":

                ModeleCarte.findByOneField('type_carte', 'pvc', (modeles) => {
                    if (modeles.length >= 0) {
                        let info = "yes"
                        res.render('client/pcmd/typepvc', {
                            title: 'GeassCard: Compte Client => Passer Commande',
                            active: 5,
                            ident: ident,
                            role: role,
                            page: page,
                            modeles: modeles,
                            info: info,
                            tabside: tabside
                        })
                    } else {
                        let info = "no"
                        res.render('client/pcmd/typepvc', {
                            title: 'GeassCard: Compte Client => Passer Commande',
                            active: 5,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            tabside: tabside
                        })
                    }

                })

                break;

            default:
                res.redirect('/compte/client/passcommand')
                break;
        }




    } else {
        res.redirect('/')
    }
})


router.get('/:typecarte/:modelecarte', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let typecarte = req.params.typecarte
        let modelecarte = req.params.modelecarte

        if (typecarte === "typecarton" || typecarte === "typepvc") {
            ModeleCarte.findByOneField('code_modele', modelecarte, (modeles) => {
                if (modeles.length === 1) {
                    res.render('client/pcmd/nbcarte', {
                        title: 'GeassCard: Compte Client => Passer Commande',
                        active: 5,
                        ident: ident,
                        role: role,
                        page: page,
                        modelecarte: modelecarte,
                        typecarte: typecarte,
                        tabside: tabside
                    })
                } else {
                    res.redirect('/compte/client/passcommand')
                }
            })
        } else {
            res.redirect('/compte/client/passcommand')
        }
    } else {
        res.redirect('/')
    }
})


router.get('/:typecarte/:modelecarte/:nbcarte', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Passer Commande"

        let typecarte = req.params.typecarte
        let modelecarte = req.params.modelecarte
        let nbcarte = req.params.nbcarte
        /*
                console.log(typecarte);
                console.log(modelecarte);
                console.log(nbcarte);
        */

        if (typecarte === "typecarton" || typecarte === "typepvc") {
            if (typecarte === "typecarton") {
                ModeleCarte.findByOneField('code_modele', modelecarte, (futmodele) => {
                    if (futmodele.length === 1) {
                        let modele = futmodele[0]
                        if (modele.typeCarte === "carton") {
                            if (nbcarte === "add50" || nbcarte === "add100" || nbcarte === "add250" || nbcarte === "add500") {
                                let nbpack = Carte.nbcarte(nbcarte)
                                Panier.findByOneField('code_client', sess.codeUser, (futpanier) => {
                                    if (futpanier.length === 0) {

                                        let coef = Carte.coefTarifCarte(modele.typeCarte, nbpack)

                                        TarifCarte.findByOneField('tarif_carte', env.val.tarifcarte[coef.coefTypeCarte][coef.coefNbPack], (tarifs) => {
                                            let prix = tarifs[0].tarifPack
                                            let codepanier = Panier.genCodePanier()
                                            let produit = {
                                                codePanier: codepanier,
                                                codeLp: LignePanier.genCodeLignePanier(),
                                                codeModele: modelecarte,
                                                typeCarte: modele.typeCarte,
                                                lienFichier: modele.lienFichier,
                                                prix: prix,
                                                quantite: 1,
                                                nbcarte: nbpack
                                            }

                                            const TVA = env.val.tva
                                            let total = prix

                                            let panier = {
                                                codePanier: codepanier,
                                                codeClient: sess.codeUser,
                                                prixTotal: total,
                                                reduction: 0
                                            }
                                            console.log(panier);

                                            Panier.create(panier, (msg) => {
                                                console.log(msg)
                                            })
                                            console.log(produit);
                                            LignePanier.create(produit, (msg) => {
                                                console.log(msg)
                                            })

                                        })
                                    } else {
                                        let panier = futpanier[0]

                                        let coef = Carte.coefTarifCarte(modele.typeCarte, nbpack)

                                        LignePanier.findByThreeField('code_panier', panier.codePanier, 'code_modele', modelecarte, ' nbcarte', nbpack, (futlignepanier) => {
                                            if (futlignepanier.length === 1) {
                                                let lignepanier = futlignepanier[0]
                                                let quantite = lignepanier.quantite + 1

                                                TarifCarte.findByOneField('tarif_carte', env.val.tarifcarte[coef.coefTypeCarte][coef.coefNbPack], (tarifs) => {
                                                    let prix = tarifs[0].tarifPack
                                                    let codepanier = Panier.genCodePanier()

                                                    const TVA = env.val.tva
                                                    let total = prix + panier.prixTotal

                                                    console.log(panier);
                                                    Panier.replaceByOneField(panier.codePanier, 'prix_total', total, (msg) => {
                                                        console.log(msg);
                                                    })

                                                    LignePanier.replaceByThreeField('code_panier', panier.codePanier, 'code_modele', modelecarte, ' nbcarte', nbpack, 'quantite', quantite, (msg) => {
                                                        console.log(msg);
                                                    })

                                                })


                                            } else {
                                                TarifCarte.findByOneField('tarif_carte', env.val.tarifcarte[coef.coefTypeCarte][coef.coefNbPack], (tarifs) => {
                                                    let prix = tarifs[0].tarifPack
                                                    let codepanier = Panier.genCodePanier()
                                                    let produit = {
                                                        codePanier: panier.codePanier,
                                                        codeLp: LignePanier.genCodeLignePanier(),
                                                        codeModele: modelecarte,
                                                        typeCarte: modele.typeCarte,
                                                        lienFichier: modele.lienFichier,
                                                        prix: prix,
                                                        quantite: 1,
                                                        nbcarte: nbpack
                                                    }

                                                    const TVA = env.val.tva
                                                    let total = prix + panier.prixTotal




                                                    Panier.replaceByOneField(panier.codePanier, 'prix_total', total, (msg) => {
                                                        console.log(msg);
                                                    })
                                                    console.log(produit);
                                                    LignePanier.create(produit, (msg) => {
                                                        console.log(msg)
                                                    })

                                                })
                                            }

                                        })
                                    }
                                })
                            } else {
                                console.log("pack inconnu")
                                res.redirect('/compte/client/passcommand')
                            }
                        } else {
                            console.log("type du modele de carte incorrecte");
                            res.redirect('/compte/client/passcommand')
                        }
                    } else {
                        console.log("modele de carte invalide");
                        res.redirect('/compte/client/passcommand')
                    }
                })
            } else {
                ModeleCarte.findByOneField('code_modele', modelecarte, (futmodele) => {
                    if (futmodele.length === 1) {
                        let modele = futmodele[0]
                        if (modele.typeCarte === "pvc") {
                            if (nbcarte === "add50" || nbcarte === "add100" || nbcarte === "add250" || nbcarte === "add500") {
                                let nbpack = Carte.nbcarte(nbcarte)
                                Panier.findByOneField('code_client', sess.codeUser, (futpanier) => {
                                    if (futpanier.length === 0) {

                                        let coef = Carte.coefTarifCarte(modele.typeCarte, nbpack)

                                        TarifCarte.findByOneField('tarif_carte', env.val.tarifcarte[coef.coefTypeCarte][coef.coefNbPack], (tarifs) => {
                                            let prix = tarifs[0].tarifPack
                                            let codepanier = Panier.genCodePanier()
                                            let produit = {
                                                codePanier: codepanier,
                                                codeLp: LignePanier.genCodeLignePanier(),
                                                codeModele: modelecarte,
                                                typeCarte: modele.typeCarte,
                                                lienFichier: modele.lienFichier,
                                                prix: prix,
                                                quantite: 1,
                                                nbcarte: nbpack
                                            }

                                            const TVA = env.val.tva
                                            let total = prix

                                            let panier = {
                                                codePanier: codepanier,
                                                codeClient: sess.codeUser,
                                                prixTotal: total,
                                                reduction: 0
                                            }
                                            console.log(panier);

                                            Panier.create(panier, (msg) => {
                                                console.log(msg)
                                            })
                                            console.log(produit);
                                            LignePanier.create(produit, (msg) => {
                                                console.log(msg)
                                            })

                                        })
                                    } else {
                                        let panier = futpanier[0]

                                        let coef = Carte.coefTarifCarte(modele.typeCarte, nbpack)

                                        LignePanier.findByThreeField('code_panier', panier.codePanier, 'code_modele', modelecarte, ' nbcarte', nbpack, (futlignepanier) => {
                                            if (futlignepanier.length === 1) {
                                                let lignepanier = futlignepanier[0]
                                                let quantite = lignepanier.quantite + 1

                                                TarifCarte.findByOneField('tarif_carte', env.val.tarifcarte[coef.coefTypeCarte][coef.coefNbPack], (tarifs) => {
                                                    let prix = tarifs[0].tarifPack
                                                    let codepanier = Panier.genCodePanier()

                                                    const TVA = env.val.tva
                                                    let total = prix + panier.prixTotal

                                                    console.log(panier);
                                                    Panier.replaceByOneField(panier.codePanier, 'prix_total', total, (msg) => {
                                                        console.log(msg);
                                                    })

                                                    LignePanier.replaceByThreeField('code_panier', panier.codePanier, 'code_modele', modelecarte, ' nbcarte', nbpack, 'quantite', quantite, (msg) => {
                                                        console.log(msg);
                                                    })

                                                })


                                            } else {
                                                TarifCarte.findByOneField('tarif_carte', env.val.tarifcarte[coef.coefTypeCarte][coef.coefNbPack], (tarifs) => {
                                                    let prix = tarifs[0].tarifPack
                                                    let codepanier = Panier.genCodePanier()
                                                    let produit = {
                                                        codePanier: panier.codePanier,
                                                        codeLp: LignePanier.genCodeLignePanier(),
                                                        codeModele: modelecarte,
                                                        typeCarte: modele.typeCarte,
                                                        lienFichier: modele.lienFichier,
                                                        prix: prix,
                                                        quantite: 1,
                                                        nbcarte: nbpack
                                                    }

                                                    const TVA = env.val.tva
                                                    let total = prix + panier.prixTotal




                                                    Panier.replaceByOneField(panier.codePanier, 'prix_total', total, (msg) => {
                                                        console.log(msg);
                                                    })
                                                    console.log(produit);
                                                    LignePanier.create(produit, (msg) => {
                                                        console.log(msg)
                                                    })

                                                })
                                            }

                                        })
                                    }
                                })
                            } else {
                                console.log("pack inconnu")
                                res.redirect('/compte/client/passcommand')
                            }
                        } else {
                            console.log("type du modele de carte incorrecte");
                            res.redirect('/compte/client/passcommand')
                        }
                    } else {
                        console.log("modele de carte invalide");
                        res.redirect('/compte/client/passcommand')
                    }
                })
            }
        } else {
            console.log("type de carte invalide");
            res.redirect('/compte/client/passcommand')
        }





        res.redirect('/compte/client/passcommand/panier')



        /*

                res.render('client/pcmd/nbcarte', 
                    { 
                        title: 'GeassCard: Compte Client => Passer Commande',
                        active: 5,
                        ident : ident,
                        role : role,
                        page : page
                    })*/
    } else {
        res.redirect('/')
    }
})




module.exports = router