const express = require('express')
const router = express.Router()
const html_specialchars = require('html-specialchars')
const emailValidator = require('email-validator')
const validator = require('validator')

const mailer = require('nodemailer')

const User = require('../../models/User')
const ContactClient = require('../../models/ContactClient')

const {
    activeSidebare
} = require('../../config/sidebare')
const {
    dclient
} = require('../../config/tabsidebase')
const idpage = 2
const tabside = dclient()
activeSidebare(tabside, idpage)

const Gvcard = require('../../config/gvcard')

const Client = require('../../models/Client')
const InfoResocialClient = require('../../models/InfoResocialClient')
const InfoSocieteClient = require('../../models/InfoSocieteClient')
const InfoFichierClient = require('../../models/InfoFichierClient')
const { informationEmail } = require('../../config/constante')

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Contacts"
        ContactClient.findByOneField('code_client', sess.codeUser, (contacts) => {
            console.log(contacts);

            if (contacts.length > 0) {
                let info = "yes"
                res.render('client/gestion_contact', {
                    title: 'GeassCard: Compte Client => Gestion Contacts',
                    active: 3,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    contacts: contacts,
                    tabside: tabside
                })
            } else {
                let info = "no"
                res.render('client/gestion_contact', {
                    title: 'GeassCard: Compte Client => Gestion Contacts',
                    active: 3,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    tabside: tabside
                })
            }
        })

    } else {
        res.redirect('/')
    }

})

router.get('/supp/:code', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let codecontact = req.params.code
        ContactClient.findByOneField('code_contact', codecontact, (futcontact) => {
            if (futcontact.length === 1) {
                let user = futcontact[0]
                let contact = {
                    codeContact: codecontact
                }

                ContactClient.remove(contact, (msg) => {
                    console.log(msg);
                })

                res.redirect('/compte/client/gestion_contact')

            } else {
                res.redirect('/compte/client/gestion_contact')
            }
        })
    } else {
        res.redirect('/')
    }
})

router.get('/new', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Contacts"
        res.render('client/new_contact', {
            title: 'GeassCard: Compte Client => Gestion Contacts',
            active: 3,
            ident: ident,
            role: role,
            page: page,
            tabside: tabside
        })
    } else {
        res.redirect('/')
    }
})

router.post('/new', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let data = req.body
        let nom = html_specialchars.escape(data.nom)
        let prenom = html_specialchars.escape(data.prenom)
        let email = html_specialchars.escape(data.email)
        let phone = html_specialchars.escape(data.phone)

        let error = ""
        let info = data


        if ((nom && prenom && email) || (nom && prenom && phone) || (nom && prenom && email && phone)) {
            if (email && !phone) {
                if (emailValidator.validate(email)) {
                    let phone = 0
                    let commentaire = ""
                    let code = ContactClient.genCodeContact()
                    let contact = {
                        codeContact: code,
                        codeClient: sess.codeUser,
                        nom: nom,
                        prenom: prenom,
                        email: email,
                        phone: phone,
                        commentaire: commentaire
                    }
                    ContactClient.create(contact, (msg) => {
                        console.log(msg)
                    })
                    res.redirect('/compte/client/gestion_contact')
                } else {
                    error = "Format Email Incorrecte"
                    let ident = sess.nom
                    let role = sess.status
                    let page = "Gestion Contacts"
                    res.render('client/new_contact', {
                        title: 'GeassCard: Compte Client => Gestion Contacts',
                        active: 3,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info,
                        tabside: tabside
                    })
                }
            }
            if (!email && phone) {
                if (validator.isInt(phone)) {
                    if (phone.length >= 8) {
                        let email = ""
                        let commentaire = ""
                        let code = ContactClient.genCodeContact()
                        let contact = {
                            codeContact: code,
                            codeClient: sess.codeUser,
                            nom: nom,
                            prenom: prenom,
                            email: email,
                            phone: phone,
                            commentaire: commentaire
                        }
                        ContactClient.create(contact, (msg) => {
                            console.log(msg)
                        })

                        res.redirect('/compte/client/gestion_contact')
                    } else {
                        error = "Au moins 8 chiffres pour le numéro de telephone"
                        res.render('client/new_contact', {
                            title: 'GeassCard: Compte Client => Gestion Contacts',
                            active: 3,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            tabside: tabside
                        })
                    }
                } else {
                    error = "Format Numéro Incorrecte"
                    let ident = sess.nom
                    let role = sess.status
                    let page = "Gestion Contacts"
                    res.render('client/new_contact', {
                        title: 'GeassCard: Compte Client => Gestion Contacts',
                        active: 3,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info,
                        tabside: tabside
                    })
                }
            }
            if (email && phone) {
                if (emailValidator.validate(email)) {
                    if (validator.isInt(phone)) {
                        if (phone.length >= 8) {
                            let commentaire = ""
                            let code = ContactClient.genCodeContact()
                            let contact = {
                                codeContact: code,
                                codeClient: sess.codeUser,
                                nom: nom,
                                prenom: prenom,
                                email: email,
                                phone: phone,
                                commentaire: commentaire
                            }

                            ContactClient.create(contact, (msg) => {
                                console.log(msg)
                            })
                            res.redirect('/compte/client/gestion_contact')
                        } else {
                            error = "Au moins 8 chiffres pour le numéro de telephone"
                            let ident = sess.nom
                            let role = sess.status
                            let page = "Gestion Contacts"
                            res.render('client/new_contact', {
                                title: 'GeassCard: Compte Client => Gestion Contacts',
                                active: 3,
                                ident: ident,
                                role: role,
                                page: page,
                                info: info,
                                tabside: tabside
                            })
                        }
                    } else {
                        error = "Format Numéro Incorrecte"
                        let ident = sess.nom
                        let role = sess.status
                        let page = "Gestion Contacts"
                        res.render('client/new_contact', {
                            title: 'GeassCard: Compte Client => Gestion Contacts',
                            active: 3,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            tabside: tabside
                        })
                    }
                } else {
                    error = "Format Email Incorrecte"
                    let ident = sess.nom
                    let role = sess.status
                    let page = "Gestion Contacts"
                    res.render('client/new_contact', {
                        title: 'GeassCard: Compte Client => Gestion Contacts',
                        active: 3,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info,
                        tabside: tabside
                    })
                }
            }

        } else {
            error = "Les champs 'Nom' et 'Prenom' sont obligatoires." +
                " PS: vous pouvez soit remplir le champ 'Email' ou le champ 'Telephone' soit remplir les deux"
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Contacts"
            res.render('client/new_contact', {
                title: 'GeassCard: Compte Client => Gestion Contacts',
                active: 3,
                ident: ident,
                role: role,
                page: page,
                info: info,
                tabside: tabside
            })
        }


    } else {
        res.redirect('/')
    }
})

router.get('/alerte/all', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {


        Client.findByOneField('code_client', sess.codeUser, (futclient) => {
            if (futclient.length == 1) {
                let client = futclient[0]
                InfoSocieteClient.findByOneField('code_client', client.codeClient, (futinfosociete) => {
                    let societeclient = futinfosociete[0]
                    InfoResocialClient.findByOneField('code_client', client.codeClient, (futinfosocial) => {
                        let socialclient = futinfosocial[0]
                        InfoFichierClient.findByOneField('code_client', client.codeClient, (futfinfofichier) => {
                            let fichierclient = futfinfofichier[0]

                            ContactClient.findByOneField('code_client', sess.codeUser, (contacts) => {
                                console.log(contacts);

                                const infoEmail = informationEmail()


                                if (contacts.length > 0) {

                                    contacts.forEach(contact => {

                                        email = contact.email

                                        //Gvcard.create(client, socialclient, societeclient, './public/upload/vcardprofil/')
                                        Gvcard.create(client, socialclient, societeclient, './')

                                        const emailSubject = 'GeassCard: Alerte Client'
                                        let smtpTransport = mailer.createTransport({
                                            //service: 'gmail',
                                            host: "node9-fr.n0c.com",
                                            secure: true,
                                            auth: {
                                                user: infoEmail.username,
                                                pass: infoEmail.password
                                            }
                                        })

                                        fs.readFile("./MEDI.vcf", function (err, data) {

                                            smtpTransport.sendMail({
                                                    from: infoEmail.username,
                                                    to: email,
                                                    subject: emailSubject,
                                                    html: "VCARD EN BAS",
                                                    attachments: [{
                                                        'filename': 'MEDI.vcf',
                                                        'content': data
                                                    }]
                                                },
                                                (error, response) => {
                                                    if (error) {
                                                        console.log("Erreur lors de l'envoi du mail!")
                                                        console.log(error)
                                                    } else {
                                                        console.log("Mail envoyé avec succès!")
                                                    }
                                                    smtpTransport.close()
                                                })

                                        });

                                    });


                                } else {
                                    res.redirect('/compte/client/gestion_contact')
                                }
                            })

                            res.redirect('/compte/client/gestion_contact')

                            //console.log(client)
                        })
                    })
                })
            } else {
                res.redirect('/')
            }
        })



    } else {
        res.redirect('/')
    }
})

module.exports = router