const express = require('express')
const router = express.Router()
const emailValidator = require('email-validator')
const validator = require('validator')
const html_specialchars = require('html-specialchars')


const dossier2basePhoto = '/upload/photoprofil/'
const dossierPhoto = './public' + dossier2basePhoto

const dossier2baseCv = '/upload/cvprofil/'
const dossierCv = './public' + dossier2baseCv

const { activeSidebare } = require('../../config/sidebare')
const { dclient } = require('../../config/tabsidebase')
const idpage = 1
const tabside = dclient()
activeSidebare(tabside, idpage)


const path = require('path')
const multer = require('multer')
const storagePhoto = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, dossierPhoto)
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' +
            Date.now() + path.extname(file.originalname))
    }
})
const storageCv = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, dossierCv)
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' +
            Date.now() + path.extname(file.originalname))
    }
})

const uploadPhotoProfil = multer({
    storage: storagePhoto,
    fileFilter: (req, file, cb) => {
        checkFileTypeImage(file, cb)
    }
}).single('photoprofil')

const uploadCvProfil = multer({
    storage: storageCv,
    fileFilter: (req, file, cb) => {
        checkFileTypeCv(file, cb)
    }
}).single('cvprofil')

function checkFileTypeCv(file, cb) {
    const filetypes = /pdf|docx|doc/

    console.log(path.extname(file.originalname.toLowerCase()));

    const extname = filetypes.test(path.extname(file.originalname.toLowerCase()))

    const mimetype = filetypes.test(file.mimetype)

    if (mimetype && extname) {
        return cb(null, true)
    } else {
        cb('Attention : Document Pdf ou Word Seulement!!!')
    }
}
function checkFileTypeImage(file, cb) {
    const filetypes = /jpeg|jpg|png|gif/

    console.log(path.extname(file.originalname.toLowerCase()));

    const extname = filetypes.test(path.extname(file.originalname.toLowerCase()))

    const mimetype = filetypes.test(file.mimetype)

    if (mimetype && extname) {
        return cb(null, true)
    } else {
        cb('Attention : Images Seulement!!!')
    }
}

const Client = require('../../models/Client')
const InfoResocialClient = require('../../models/InfoResocialClient')
const InfoSocieteClient = require('../../models/InfoSocieteClient')
const InfoFichierClient = require('../../models/InfoFichierClient')

const User = require('../../models/User')

const Initial = require('../../config/initial')
const Constante = require('../../config/constante')

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Données"
        res.render('client/gestion_data', {
            title: 'GeassCard: Compte Client => Gestion Données',
            active: 2,
            ident: ident,
            role: role,
            page: page,
            tabside: tabside
        })
    } else {
        res.redirect('/')
    }

})

router.get('/voirdata', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Données"
        Client.findByOneField('code_client', sess.codeUser, (futclient) => {
            client = futclient[0]
            let hashcc = client.hashcc
            liencard = '/portfolio/' + hashcc
            res.redirect(liencard)
            console.log(liencard);

        })
    } else {
        res.redirect('/')
    }
})

router.get('/modifdata/photoprofil', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {

            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"

            Client.findByOneField('code_client', sess.codeUser, (futclient) => {
                if (futclient.length > 0) {
                    let client = futclient[0]
                    console.log(client);

                    InfoFichierClient.findByOneField('code_client', sess.codeUser, (futfichier) => {
                        if (futfichier.length > 0 && futfichier.length == 1) {
                            let fichierclient = futfichier[0]

                            res.render('client/gdata/modata/photoprofil', {
                                title: 'GeassCard: Compte Client => Gestion Données',
                                active: 2,
                                ident: ident,
                                role: role,
                                page: page,
                                actuclient: client,
                                infofichier: fichierclient,
                                tabside: tabside
                            })

                        } else {
                            res.redirect('/')
                        }
                    })
                } else {
                    res.redirect('/')
                }
            })
        } else {
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"
            res.render('client/gdata/modata/photoprofil', {
                title: 'GeassCard: Compte Client => Gestion Données',
                active: 2,
                ident: ident,
                role: role,
                page: page,
                tabside: tabside
            })
        }

    } else {
        res.redirect('/')
    }
})

router.post('/modifdata/photoprofil', (req, res, next) => {
    let sess = req.session
    let ident = sess.nom
    let role = sess.status
    let page = "Gestion Données"
    Client.findByOneField('code_client', sess.codeUser, (futclient) => {
        if (futclient.length > 0) {
            let client = futclient[0]
            console.log(client);

            InfoFichierClient.findByOneField('code_client', sess.codeUser, (futfichier) => {
                if (futfichier.length > 0 && futfichier.length == 1) {
                    let fichierclient = futfichier[0]

                    uploadPhotoProfil(req, res, (err) => {
                        if (err) {
                            let error = err
                            console.log(req.file)
                            res.render('client/gdata/modata/photoprofil', {
                                title: 'GeassCard: Compte Client => Gestion Données',
                                active: 2,
                                ident: ident,
                                role: role,
                                page: page,
                                error: error,
                                tabside: tabside
                            })
                        } else {

                            let chemin_fichier = dossier2basePhoto + req.file.filename
                            console.log(chemin_fichier)

                            InfoFichierClient.replaceByOneField(fichierclient.codeClient, "chemin_photo_profil", chemin_fichier, (msg) => {
                                console.log(msg)

                                let message = "Votre photo de profil a bien été modifiée. Svp recharger la page."

                                res.render('client/gdata/modata/photoprofil', {
                                    title: 'GeassCard: Compte Client => Gestion Données',
                                    active: 2,
                                    ident: ident,
                                    role: role,
                                    page: page,
                                    message: message,
                                    actuclient: client,
                                    infofichier: fichierclient,
                                    tabside: tabside
                                })

                            })
                        }
                    })

                } else {
                    res.redirect('/')
                }
            })
        } else {
            res.redirect('/')
        }
    })

})

router.get('/modifdata/cvprofil', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {

            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"

            Client.findByOneField('code_client', sess.codeUser, (futclient) => {
                if (futclient.length > 0) {
                    let client = futclient[0]
                    console.log(client);

                    InfoFichierClient.findByOneField('code_client', sess.codeUser, (futfichier) => {
                        if (futfichier.length > 0 && futfichier.length == 1) {
                            let fichierclient = futfichier[0]

                            res.render('client/gdata/modata/cvprofil', {
                                title: 'GeassCard: Compte Client => Gestion Données',
                                active: 2,
                                ident: ident,
                                role: role,
                                page: page,
                                actuclient: client,
                                infofichier: fichierclient,
                                tabside: tabside
                            })

                        } else {
                            res.redirect('/')
                        }
                    })
                } else {
                    res.redirect('/')
                }
            })
        } else {
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"
            res.render('client/gdata/modata/cvprofil', {
                title: 'GeassCard: Compte Client => Gestion Données',
                active: 2,
                ident: ident,
                role: role,
                page: page,
                tabside: tabside
            })
        }

    } else {
        res.redirect('/')
    }
})

router.post('/modifdata/cvprofil', (req, res, next) => {
    let sess = req.session
    let ident = sess.nom
    let role = sess.status
    let page = "Gestion Données"
    Client.findByOneField('code_client', sess.codeUser, (futclient) => {
        if (futclient.length > 0) {
            let client = futclient[0]
            console.log(client);

            InfoFichierClient.findByOneField('code_client', sess.codeUser, (futfichier) => {
                if (futfichier.length > 0 && futfichier.length == 1) {
                    let fichierclient = futfichier[0]

                    uploadCvProfil(req, res, (err) => {
                        if (err) {
                            let error = err
                            console.log(req.file)
                            res.render('client/gdata/modata/cvprofil', {
                                title: 'GeassCard: Compte Client => Gestion Données',
                                active: 2,
                                ident: ident,
                                role: role,
                                page: page,
                                error: error,
                                tabside: tabside
                            })
                        } else {

                            let chemin_fichier = dossier2baseCv + req.file.filename
                            console.log(chemin_fichier)

                            InfoFichierClient.replaceByOneField(fichierclient.codeClient, "chemin_cv", chemin_fichier, (msg) => {
                                console.log(msg)

                                let message = "Votre curriculum vitae a bien été modifié. Svp recharger la page."

                                res.render('client/gdata/modata/cvprofil', {
                                    title: 'GeassCard: Compte Client => Gestion Données',
                                    active: 2,
                                    ident: ident,
                                    role: role,
                                    page: page,
                                    message: message,
                                    actuclient: client,
                                    infofichier: fichierclient,
                                    tabside: tabside
                                })

                            })
                        }
                    })
                } else {
                    res.redirect('/')
                }
            })
        } else {
            res.redirect('/')
        }
    })

})

router.get('/modifdata', (req, res, next) => {
    let sess = req.session
    console.log(sess)
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"
            res.render('client/gdata/modifdata', {
                title: 'GeassCard: Compte Client => Gestion Données',
                active: 2,
                ident: ident,
                role: role,
                page: page,
                tabside: tabside
            })
        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }


    } else {
        res.redirect('/')
    }
})

router.get('/modifdata/infoperso', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {

            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"

            Client.findByOneField('code_client', sess.codeUser, (futclient) => {
                if (futclient.length > 0) {
                    let client = futclient[0]
                    console.log(client);

                    res.render('client/gdata/modata/infoperso', {
                        title: 'GeassCard: Compte Client => Gestion Données',
                        active: 2,
                        ident: ident,
                        role: role,
                        page: page,
                        actuclient: client,
                        tabside: tabside
                    })
                } else {
                    res.redirect('/')
                }
            })
        } else {
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"
            res.render('client/gdata/attention', {
                title: 'GeassCard: Compte Client => Gestion Données',
                active: 2,
                ident: ident,
                role: role,
                page: page,
                tabside: tabside
            })
        }

    } else {
        res.redirect('/')
    }
})

router.post('/modifdata/infoperso', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        /*
        User.findByOneField('code_user', sess.codeUser, (futuser) => {
            if (futuser.length == 1) {
                let user = futuser[0]
                if (user.autorisation == 1) {
                    
                } else {
                    res.redirect('/logout')
                }
            } else {
                res.redirect('/logout')
            }
        })
        */

        if (sess.permitData === 1) {
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"
            let data = req.body

            console.log(data);

            let nom = Initial.create(data.nom, "")
            let prenom = Initial.create(data.prenom, "")
            let email = Initial.create(data.email, "")
            let adresse = Initial.create(data.adresse, "")
            let datenaissance = Initial.create(data.datenaissance, "")
            let phone1 = Initial.create(data.phone1, 0)
            let phone2 = Initial.create(data.phone2, 0)
            let phone3 = Initial.create(data.phone3, 0)
            let siteinternet = Initial.create(data.siteinternet, "")
            let autodescription = Initial.create(data.autodescription, "")





            let error = ""
            let info = data



            //console.log(data)

            if (nom && prenom && email && adresse && phone1) {
                if (emailValidator.validate(email)) {
                    if (validator.isInt(phone1)) {

                        User.replaceByOneField(sess.codeUser, 'nom', nom, (msg) => {
                            console.log(msg)
                        })
                        User.replaceByOneField(sess.codeUser, 'prenom', prenom, (msg) => {
                            console.log(msg)
                        })


                        Client.replaceByOneField(sess.codeUser, 'nom', nom, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'prenom', prenom, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'email', email, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'adresse', adresse, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'date_naissance', datenaissance, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'phone1', phone1, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'phone2', phone2, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'phone3', phone3, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'site_internet', siteinternet, (msg) => {
                            console.log(msg)
                        })
                        Client.replaceByOneField(sess.codeUser, 'auto_description', autodescription, (msg) => {
                            console.log(msg)
                        })

                        res.redirect('/compte/client/gestion_data/modifdata/infoperso')
                    } else {
                        error = "Format numéro de téléphone 1 incorrecte"
                        res.render('client/gdata/modata/infoperso', {
                            title: 'GeassCard: Compte Client => Gestion Données',
                            active: 2,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            error: error,
                            tabside: tabside
                        })
                    }
                } else {
                    error = "Format Email est obligatoire"
                    res.render('client/gdata/modata/infoperso', {
                        title: 'GeassCard: Compte Client => Gestion Données',
                        active: 2,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info,
                        error: error,
                        tabside: tabside
                    })
                }
            } else {
                error = "Nom, Prenom, Email, Addresse, Numéro de téléphone 1"
                res.render('client/gdata/modata/infoperso', {
                    title: 'GeassCard: Compte Client => Gestion Données',
                    active: 2,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    error: error,
                    tabside: tabside
                })
            }
        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }
    } else {
        res.redirect('/')
    }
})

router.get('/modifdata/inforesociaux', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"

            InfoResocialClient.findByOneField('code_client', sess.codeUser, (futinfo) => {
                if (futinfo.length > 0) {
                    let infosocial = futinfo[0]

                    res.render('client/gdata/modata/inforesociaux', {
                        title: 'GeassCard: Compte Client => Gestion Données',
                        active: 2,
                        ident: ident,
                        role: role,
                        page: page,
                        infosocial: infosocial,
                        tabside: tabside
                    })
                } else {
                    res.redirect('/')
                }
            })
        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }
    } else {
        res.redirect('/')
    }
})

router.post('/modifdata/inforesociaux', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {

            let data = req.body

            let nptwitter = Initial.create(data.nptwitter, "")
            let lptwitter = Initial.create(data.lptwitter, "")
            let npfacebook = Initial.create(data.npfacebook, "")
            let lpfacebook = Initial.create(data.lpfacebook, "")
            let npskype = Initial.create(data.npskype, "")
            let lpskype = Initial.create(data.lpskype, "")
            let nplinkedin = Initial.create(data.nplinkedin, "")
            let lplinkedin = Initial.create(data.lplinkedin, "")
            let npgoogleplus = Initial.create(data.npgoogleplus, "")
            let lpgoogleplus = Initial.create(data.lpgoogleplus, "")
            let npinstagram = Initial.create(data.npinstagram, "")
            let lpinstagram = Initial.create(data.lpinstagram, "")
            let npviadeo = Initial.create(data.npviadeo, "")
            let lpviadeo = Initial.create(data.lpviadeo, "")


            InfoResocialClient.replaceByOneField(sess.codeUser, 'nom_profil_twitter', nptwitter, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'lien_twitter', lptwitter, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'nom_profil_facebook', npfacebook, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'lien_facebook', lpfacebook, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'nom_profil_skype', npskype, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'lien_skype', lpskype, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'nom_profil_linkedin', nplinkedin, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'lien_linkedin', lplinkedin, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'nom_profil_google_plus', npgoogleplus, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'lien_google_plus', lpgoogleplus, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'nom_profil_instagram', npinstagram, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'lien_instagram', lpinstagram, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'nom_profil_viadeo', npviadeo, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.replaceByOneField(sess.codeUser, 'lien_viadeo', lpviadeo, (msg) => {
                console.log(msg)
            })

            res.redirect('/compte/client/gestion_data/modifdata/inforesociaux')
        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }
    } else {
        res.redirect('/')
    }
})

router.get('/modifdata/infosociete', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Données"

            InfoSocieteClient.findByOneField('code_client', sess.codeUser, (futinfo) => {
                if (futinfo.length > 0) {
                    let infosociete = futinfo[0]
                    console.log(infosociete);

                    res.render('client/gdata/modata/infosociete', {
                        title: 'GeassCard: Compte Client => Gestion Données',
                        active: 2,
                        ident: ident,
                        role: role,
                        page: page,
                        infosociete: infosociete,
                        tabside: tabside
                    })
                } else {
                    res.redirect('/')
                }
            })
        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }
    } else {
        res.redirect('/')
    }
})

router.post('/modifdata/infosociete', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {

            let data = req.body

            let nom = Initial.create(data.nom, "")
            let poste = Initial.create(data.poste, "")
            let email = Initial.create(data.email, "")
            let adresse = Initial.create(data.adresse, "")
            let phone1 = Initial.create(data.phone1, 0)
            let phone2 = Initial.create(data.phone2, 0)
            let phone3 = Initial.create(data.phone3, 0)
            let site = Initial.create(data.siteinternet, "")

            InfoSocieteClient.replaceByOneField(sess.codeUser, 'nom', nom, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.replaceByOneField(sess.codeUser, 'poste', poste, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.replaceByOneField(sess.codeUser, 'email', email, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.replaceByOneField(sess.codeUser, 'adresse', adresse, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.replaceByOneField(sess.codeUser, 'phone1', phone1, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.replaceByOneField(sess.codeUser, 'phone2', phone2, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.replaceByOneField(sess.codeUser, 'phone3', phone3, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.replaceByOneField(sess.codeUser, 'site_internet', site, (msg) => {
                console.log(msg)
            })

            res.redirect('/compte/client/gestion_data/modifdata/infosociete')

        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }

    } else {
        res.redirect('/')
    }
})


router.get('/abonnement', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Données"

        if (sess.permitData === 1) {
            let info = "yes"

        } else {
            let info = "no"

        }

        res.render('client/gdata/abonnement', {
            title: 'GeassCard: Compte Client => Gestion Données',
            active: 2,
            ident: ident,
            role: role,
            page: page,
            tabside: tabside
        })
    } else {
        res.redirect('/')
    }
})

router.get('/template', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Données"

        if (sess.permitData === 1) {
            let info = "yes"

            let listeTemplate = Constante.listeTemplate()

            Client.findByOneField('code_client', sess.codeUser, (futclient) => {
                if (futclient.length == 1) {
                    let client = futclient[0]

                    let nomTemplate = client.lienTemplate

                    res.render('client/gdata/template', {
                        title: 'GeassCard: Compte Client => Gestion Données',
                        active: 2,
                        ident: ident,
                        role: role,
                        page: page,
                        nomTemplate: nomTemplate,
                        listeTemplate: listeTemplate,
                        tabside: tabside
                    })

                }
            })

        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }
    } else {
        res.redirect('/')
    }
})

router.get('/template/:action/:type_template/:id_template', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {

        if (sess.permitData === 1) {
            let info = "yes"

            let type_template = req.params.type_template
            let id_template = req.params.id_template
            let action = req.params.action

            let lien_template = type_template + "/" + id_template

            if (Constante.verifierExistenceLienTemplate(lien_template) == false) {
                res.redirect('/compte/client/gestion_data/template')
            } else {
                if (action == "active" || action == "deactive") {

                    let nom_template = type_template + " " + id_template

                    Client.findByOneField('code_client', sess.codeUser, (futclient) => {
                        if (futclient.length == 1) {
                            Client.replaceByOneField(sess.codeUser, "lien_template", nom_template, (message) => {
                                console.log(message)
                                res.redirect('/compte/client/gestion_data/template')
                            })
                        }
                    })

                } else {
                    res.redirect('/compte/client/gestion_data/template')
                }
            }
        } else {
            res.redirect('/compte/client/gestion_data/abonnement')
        }
    } else {
        res.redirect('/')
    }
})

module.exports = router