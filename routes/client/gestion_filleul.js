const express = require('express')
const router = express.Router()
const Parrain = require('../../models/Parrain')
const Filleul = require('../../models/Filleul')

const html_specialchars = require('html-specialchars')
const emailValidator = require('email-validator')
const mailer = require('nodemailer')

const env = require('../../config/config').env
const URLBASE = env.domaine

const { activeSidebare } = require('../../config/sidebare')
const { dclient } = require('../../config/tabsidebase')
const Constante = require('../../config/constante')
const { informationEmailHello } = require('../../config/constante')
const idpage = 3
const tabside = dclient()
activeSidebare(tabside, idpage)

const infoEmail = informationEmailHello()

/* GET home page. */
router.get('/', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Filleuls"
        Filleul.findByOneField('code_parrain', sess.codeUser, (filleuls) => {
            if (filleuls.length > 0) {
                let info = "yes"
                res.render('client/gestion_filleul', 
                { 
                    title: 'GeassCard: Compte Client => Gestion Filleuls',
                    active: 4,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info,
                    filleuls : filleuls,
                    tabside: tabside
                })
            } else {
                let info =  "no"
                res.render('client/gestion_filleul', 
                { 
                    title: 'GeassCard: Compte Client => Gestion Filleuls',
                    active: 4,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info,
                    tabside: tabside
                })
            }
        })        
    } else {
        res.redirect('/')
    }
    
})

router.get('/new', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Filleuls"
        res.render('client/new_filleul', 
        { 
            title: 'GeassCard: Compte Client => Gestion Filleuls',
            active: 4,
            ident : ident,
            role : role,
            page : page,
            tabside: tabside
        })
    } else {
        res.redirect('/')
    }
})

router.post('/new', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let data = req.body
        let email= html_specialchars.escape(data.email)

        let error =""
        let info  = data

        if (email) {
            if (emailValidator.validate(email)) {
                Parrain.findByOneField('code_parrain', sess.codeUser, (xparrain) => {
                    console.log(sess.codeUser);
                    
                    if (xparrain.length > 0) {
                        let parrain = xparrain[0]
                        let hachac  = parrain.hachac
                        let urlval  = URLBASE+"/register/"+hachac

                        const emailSubject   = 'GeassCard : Parrainage => Invitation du client ' + sess.codeUser +'(' + sess.username + ')'
                        let smtpTransport = mailer.createTransport({
                            //service: 'gmail',
                            host: "node9-fr.n0c.com",
                            secure: true,
                            auth: {
                                user: infoEmail.username,
                                pass: infoEmail.password
                            }
                        })
                        let mail = {
                            from: infoEmail.username,
                            to: email,
                            subject: emailSubject,
                            html: urlval
                        }

                        smtpTransport.sendMail(mail, (error, response) => {
                            if(error){
                                console.log("Erreur lors de l'envoi du mail!")
                                console.log(error)
                                let msgerror = "Erreur lors de l'envoi du mail!"
                                let ident = sess.nom
                                let role = sess.status
                                let page = "Gestion Filleuls"
                                res.render('client/new_filleul', 
                                    { 
                                        title: 'GeassCard: Compte Client => Gestion Filleuls',
                                        active: 4,
                                        ident : ident,
                                        role : role,
                                        page : page,
                                        msgerror  : msgerror,
                                        tabside: tabside
                                    })
                            }else{
                                console.log("Mail envoyé avec succès!")
                                console.log(response)
                                let msgsuccess = "Un email a été envoyé avec succès"
                                let ident = sess.nom
                                let role = sess.status
                                let page = "Gestion Filleuls"
                                res.render('client/new_filleul', 
                                    { 
                                        title: 'GeassCard: Compte Client => Gestion Filleuls',
                                        active: 4,
                                        ident : ident,
                                        role : role,
                                        page : page,
                                        msgsuccess  : msgsuccess,
                                        tabside: tabside
                                    })
                            }
                            smtpTransport.close()
                        })
                        
                        
                        

                    } else {
                        res.redirect('/compte/client/gestion_filleul')
                    }
                })

                
            } else {
                error = "Format Email Incorrecte"
                let ident = sess.nom
                let role = sess.status
                let page = "Gestion Filleuls"
                res.render('client/new_filleul', 
                    { 
                        title: 'GeassCard: Compte Client => Gestion Filleuls',
                        active: 4,
                        ident : ident,
                        role : role,
                        page : page,
                        info : info,
                        error: error,
                        tabside: tabside
                    })
            }
        } else {
            error = "Email obligatoire"
            let ident = sess.nom
            let role = sess.status
            let page = "Gestion Filleuls"
            res.render('client/new_filleul', 
                { 
                    title: 'GeassCard: Compte Client => Gestion Filleuls',
                    active: 4,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info,
                    error: error,
                    tabside: tabside
                })
        }
    } else  {
        res.redirect('/')
    }
})

module.exports = router
