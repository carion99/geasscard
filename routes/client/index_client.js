const express = require('express')

const router = express.Router()

const ContactClient = require('../../models/ContactClient')
const Filleul = require('../../models/Filleul')

const {
    activeSidebare
} = require('../../config/sidebare')
const {
    dclient
} = require('../../config/tabsidebase')
const idpage = 0
const tabside = dclient()
activeSidebare(tabside, idpage)

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Bienvenue"

        let totalContacts = 0
        let totalFilleuls = 0


        ContactClient.findCountByOneField('code_client', sess.codeUser, (futcount) => {
            console.log(futcount)

            totalContacts = futcount

            Filleul.findCountByOneField('code_parrain', sess.codeUser, (futfilleul) => {
                console.log(futfilleul)

                totalFilleuls = futfilleul

                res.render('client/index_client', {
                    title: 'GeassCard: Compte Gestionnaire => Accueil',
                    active: 1,
                    ident: ident,
                    role: role,
                    page: page,
                    tabside: tabside,
                    totalContacts: totalContacts,
                    totalFilleuls: totalFilleuls
                })
            })
        })


    } else {
        res.redirect('/')
    }

})

module.exports = router