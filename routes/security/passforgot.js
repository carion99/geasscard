const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('security/passforgot', { title: 'GeassCard: Mot de passe oublié' })
})

module.exports = router
