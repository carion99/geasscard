const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('security/register_presta', { title: 'GeassCard: Inscription' })
})

module.exports = router
