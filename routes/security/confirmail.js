const express = require('express')
const router = express.Router()

const User = require('../../models/User')
const AttenteValidation = require('../../models/AttenteValidation')
const Client = require('../../models/Client')
const InfoFichierClient = require('../../models/InfoFichierClient')
const InfoSocieteClient = require('../../models/InfoSocieteClient')
const InfoResocialClient= require('../../models/InfoResocialClient')
const Parrain = require('../../models/Parrain')

const Qr = require('../../config/qr')
const QRCode = require('qrcode')
const Hashub    = require('../../config/hashub')
const env    = require('../../config/config').env

const Constante = require('../../config/constante')

/* GET home page. */
router.get('/wellregister', function(req, res, next) {
  res.render('security/confirmail', 
        { 
            title: 'GeassCard: Confirmation',
            nb : 1
        })
})

router.get('/:hachar', function(req, res, next) {
    let hachar = req.params.hachar
    AttenteValidation.findByOneField('hachar', hachar, (attente) => {
        if (attente.length === 1) {
            let codeuser = attente[0].codeUser

            User.findByOneField('code_user', codeuser, (futuser) => {
                let user = futuser[0]
                let emailuser   = user.username
                let passuser    = user.password

                let msg = codeuser+emailuser+passuser
                let message = Hashub.crypt(msg)
                let liencard= env.domaine+'/portfolio/'+message

                QRCode.toDataURL(liencard, { errorCorrectionLevel: 'H' },  (err, qrval) => {
                    
                    User.replaceByOneField(codeuser, 'status', 'client', (msg) => {
                        console.log(msg)
                    })    


                    let client = {
                        codeClient  : codeuser,
                        nom     : user.nom,
                        prenom  : user.prenom,
                        sexe: user.genre,
                        dateNaissance : "",
                        email   : user.username,
                        adresse : "",
                        phone1  : 0,
                        phone2  : 0,
                        phone3  : 0,
                        siteInternet : "",
                        autoDescription : "",
                        lienCard: liencard,
                        lienTemplate: Constante.listeTemplate()[0].texte,
                        hashcc  : message
                    }

                    let infoFichierClient = {
                        codeClient: codeuser,
                        cheminLogo: "",
                        cheminPhotoProfil: "",
                        cheminCv : "",
                        cheminQr : qrval
                    }

                    let infoSocieteClient = {
                        codeClient: codeuser,
                        nom   : "",
                        poste : "",
                        email : "",
                        adresse  : "",
                        phone1: 0,
                        phone2: 0,
                        phone3: 0,
                        siteInternet : ""
                    }

                    let infoResocialClient = {
                        codeClient : codeuser,
                        nomProfilTwitter : "",
                        lienTwitter: "",
                        nomProfilFacebook: "",
                        lienFacebook: "",
                        nomProfilSkype : "",
                        lienSkype : "",
                        nomProfilLinkedin : "",
                        lienLinkedin: "",
                        nomProfilGooglePlus: "",
                        lienGooglePlus : "",
                        nomProfilInstagram: "",
                        lienInstagram: "",
                        nomProfilViadeo: "",
                        lienViadeo : ""
                    }

                    let parrain = {
                        codeParrain: codeuser,
                        nom     : "",
                        prenom  : "",
                        hachac  : hachar
                    }

                    Client.create(client, (msg) => {
                        console.log(msg)
                    })
                    
                    InfoFichierClient.create(infoFichierClient, (msg) => {
                        console.log(msg)
                    })

                    InfoSocieteClient.create(infoSocieteClient, (msg) => {
                        console.log(msg)
                    })

                    InfoResocialClient.create(infoResocialClient, (msg) => {
                        console.log(msg)
                    })
                    
                    AttenteValidation.remove(attente[0], (msg) => {
                        console.log(msg)
                    })

                    Parrain.create(parrain, (msg) => {
                        console.log(msg)
                    })

                    res.render('security/confirmail', 
                        { 
                            title: 'GeassCard: Confirmation',
                            nb : 2
                        })
                    
                })

                
                
            })

            


        } else {
            res.redirect('/')
        }
    })
    
  })


  router.get('/supervisor/:hachar', function(req, res, next) {
    let hachar = req.params.hachar
    AttenteValidation.findByOneField('hachar', hachar, (attente) => {
        if (attente.length === 1) {
            let codeuser = attente[0].codeUser

            User.findByOneField('code_user', codeuser, (futuser) => {
                let user = futuser[0]
                let emailuser   = user.username
                let passuser    = user.password

                User.replaceByOneField(codeuser, 'status', 'superviseur', (msg) => {
                    console.log(msg)
                }) 

                AttenteValidation.remove(attente[0], (msg) => {
                    console.log(msg)
                })

                res.render('security/confirmail', 
                    { 
                        title: 'GeassCard: Confirmation',
                        nb : 2
                    })
                
            })

            


        } else {
            res.redirect('/')
        }
    })
    
  })

module.exports = router
