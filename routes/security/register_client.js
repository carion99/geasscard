const express = require('express')
const router = express.Router()
const html_specialchars = require('html-specialchars')
const emailValidator = require('email-validator')
const mailer = require('nodemailer')

const Hashub = require('../../config/hashub')

const User = require('../../models/User')
const Client = require('../../models/Client')
const AttenteValidation = require('../../models/AttenteValidation')
const Parrain = require('../../models/Parrain')
const Filleul = require('../../models/Filleul')

const env = require('../../config/config').env
const Constante = require('../../config/constante')
const {
    informationEmailSupport,
    getHostmail
} = require('../../config/constante')
//const URLBASE = env.domaine
const URLBASE = Constante.informationUrl().url
const infoEmail = informationEmailSupport()
const hostmail = getHostmail()

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser) {
        if (sess.status === "root") {
            res.redirect('/compte/root')
        }
        if (sess.status === "client") {
            res.redirect('/compte/client')
        }
        if (sess.status === "presta") {
            res.redirect('/compte/presta')
        }
    } else {
        res.render('security/register_client', {
            title: 'GeassCard: Inscription'
        })
    }
})

router.get('/:code', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser) {
        if (sess.status === "root") {
            res.redirect('/compte/root')
        }
        if (sess.status === "client") {
            res.redirect('/compte/client')
        }
        if (sess.status === "presta") {
            res.redirect('/compte/presta')
        }
    } else {
        let code = req.params.code
        Parrain.findByOneField('hachac', code, (xparrain) => {
            if (xparrain.length === 1) {
                res.render('security/register_client', {
                    title: 'GeassCard: Inscription',
                    code: code
                })
            } else {
                res.redirect('/')
            }
        })

    }
})

router.post('/', (req, res, next) => {
    let data = req.body
    let nom = html_specialchars.escape(data.nom)
    let prenom = html_specialchars.escape(data.prenom)
    let genre = html_specialchars.escape(data.genre)
    let email = html_specialchars.escape(data.email)
    let pass = html_specialchars.escape(data.motdepasse)
    let cfpass = html_specialchars.escape(data.cfmotdepasse)

    let error = ""

    if (nom && prenom && genre && email && pass && cfpass) {

        if (emailValidator.validate(email)) {
            let nbpass = 8
            console.log('ok');
            User.findByOneField('username', email, (futuser) => {
                console.log(futuser);
                if (futuser.length === 0) {
                    if (pass.length >= nbpass) {
                        if (cfpass === pass) {
                            let code = Client.genCodeClient()
                            let hashpass = Hashub.crypt(pass)
                            let status = "noclient"
                            let user = {
                                codeUser: code,
                                nom: nom,
                                prenom: prenom,
                                genre: genre,
                                username: email,
                                password: hashpass,
                                status: status
                            }

                            User.create(user, (msg) => {
                                console.log(msg)
                            })

                            let strmel = code + email + pass

                            let codelien = Hashub.crypt(strmel)
                            let urlval = URLBASE + "/confirm/" + codelien

                            let codav = AttenteValidation.genCodeAv()

                            let attente = {
                                codeAv: codav,
                                codeUser: code,
                                hachar: codelien,
                                status: "noclient"
                            }

                            AttenteValidation.create(attente, (msg) => {
                                console.log(msg)
                            })


                            const emailSubject = 'GeassCard: Confirmation Compte Client'
                            let smtpTransport = mailer.createTransport({
                                //service: 'gmail',
                                host: hostmail,
                                secure: true,
                                auth: {
                                    user: infoEmail.username,
                                    pass: infoEmail.password
                                }
                            })
                            let mail = {
                                from: infoEmail.username,
                                to: email,
                                subject: emailSubject,
                                html: urlval
                            }

                            smtpTransport.sendMail(mail, (err, response) => {
                                if (err) {
                                    console.log(err)
                                    error = "Erreur lors de l'envoi du mail!"
                                    let info = req.body
                                    console.log(error)
                                    res.render('security/register_client', {
                                        title: 'GeassCard: Inscription',
                                        info: info,
                                        error: error
                                    })
                                } else {
                                    console.log("Mail envoyé avec succès!")
                                    res.redirect('/confirm/wellregister')
                                }
                                smtpTransport.close()
                            })


                        } else {
                            error = "Mot de passe non correspondant"
                            let info = req.body
                            console.log(error)
                            res.render('security/register_client', {
                                title: 'GeassCard: Inscription',
                                info: info,
                                error: error
                            })
                        }
                    } else {
                        error = "Mot de passe doit contenir au moins 8 caractères"
                        let info = req.body
                        console.log(error)
                        res.render('security/register_client', {
                            title: 'GeassCard: Inscription',
                            info: info,
                            error: error
                        })
                    }
                } else {
                    error = "Email Déja Utilisé"
                    let info = req.body
                    console.log(error)
                    res.render('security/register_client', {
                        title: 'GeassCard: Inscription',
                        info: info,
                        error: error
                    })
                }
            })
        } else {
            error = "Format Email Incorrecte"
            let info = req.body
            console.log(error)
            res.render('security/register_client', {
                title: 'GeassCard: Inscription',
                info: info,
                error: error
            })
        }
    } else {
        error = "Tous les champs sont obligatoires"
        let info = req.body
        console.log(error)
        res.render('security/register_client', {
            title: 'GeassCard: Inscription',
            info: info,
            error: error
        })
    }



})

router.post('/:codinscription', (req, res, next) => {
    let codinscription = req.params.codinscription
    Parrain.findByOneField('hachac', codinscription, (xparrain) => {
        if (xparrain.length === 1) {
            let data = req.body
            let nom = html_specialchars.escape(data.nom)
            let prenom = html_specialchars.escape(data.prenom)
            let genre = html_specialchars.escape(data.genre)
            let email = html_specialchars.escape(data.email)
            let pass = html_specialchars.escape(data.motdepasse)
            let cfpass = html_specialchars.escape(data.cfmotdepasse)

            let error = ""

            if (nom && prenom && genre && email && pass && cfpass) {

                if (emailValidator.validate(email)) {
                    let nbpass = 8
                    User.findByOneField('username', email, (futuser) => {
                        if (futuser.length === 0) {
                            if (pass.length >= nbpass) {
                                if (cfpass === pass) {
                                    let code = Client.genCodeClient()
                                    let hashpass = Hashub.crypt(pass)
                                    let status = "noclient"
                                    let user = {
                                        codeUser: code,
                                        nom: nom,
                                        prenom: prenom,
                                        genre: genre,
                                        username: email,
                                        password: hashpass,
                                        status: status
                                    }

                                    let codepar = xparrain[0].codeParrain
                                    let codefi = Filleul.genCodeFilleul(codepar)


                                    let filleul = {
                                        codeFilleul: codefi,
                                        codeParrain: codepar,
                                        email: email,
                                    }

                                    User.create(user, (msg) => {
                                        console.log(msg)
                                    })

                                    Filleul.create(filleul, (msg) => {
                                        console.log(msg)
                                    })

                                    let strmel = code + email + pass

                                    let codelien = Hashub.crypt(strmel)
                                    let urlval = URLBASE + "/confirm/" + codelien

                                    let codav = AttenteValidation.genCodeAv()

                                    let attente = {
                                        codeAv: codav,
                                        codeUser: code,
                                        hachar: codelien,
                                        status: "noclient"
                                    }

                                    AttenteValidation.create(attente, (msg) => {
                                        console.log(msg)
                                    })

                                    const emailSubject = 'GeassCard: Confirmation Compte Client'
                                    let smtpTransport = mailer.createTransport({
                                        //service: 'gmail',
                                        host: hostmail,
                                        secure: true,
                                        auth: {
                                            user: infoEmail.username,
                                            pass: infoEmail.password
                                        }
                                    })
                                    let mail = {
                                        from: emailUser,
                                        to: email,
                                        subject: emailSubject,
                                        html: urlval
                                    }

                                    smtpTransport.sendMail(mail, (err, response) => {
                                        if (err) {
                                            console.log(err)
                                            error = "Erreur lors de l'envoi du mail!"
                                            let info = req.body
                                            console.log(error)
                                            res.render('security/register_client', {
                                                title: 'GeassCard: Inscription',
                                                info: info,
                                                error: error
                                            })
                                        } else {
                                            console.log("Mail envoyé avec succès!")
                                            res.redirect('/confirm/wellregister')
                                        }
                                        smtpTransport.close()
                                    })

                                } else {
                                    error = "Mot de passe non correspondant"
                                    let info = req.body
                                    console.log(error)
                                    res.render('security/register_client', {
                                        title: 'GeassCard: Inscription',
                                        info: info,
                                        error: error
                                    })
                                }
                            } else {
                                error = "Mot de passe doit contenir au moins 8 caractères"
                                let info = req.body
                                console.log(error)
                                res.render('security/register_client', {
                                    title: 'GeassCard: Inscription',
                                    info: info,
                                    error: error
                                })
                            }
                        } else {
                            error = "Email Déja Utilisé"
                            let info = req.body
                            console.log(error)
                            res.render('security/register_client', {
                                title: 'GeassCard: Inscription',
                                info: info,
                                error: error
                            })
                        }
                    })
                } else {
                    error = "Format Email Incorrecte"
                    let info = req.body
                    console.log(error)
                    res.render('security/register_client', {
                        title: 'GeassCard: Inscription',
                        info: info,
                        error: error
                    })
                }
            } else {
                error = "Tous les champs sont obligatoires"
                let info = req.body
                console.log(error)
                res.render('security/register_client', {
                    title: 'GeassCard: Inscription',
                    info: info,
                    error: error
                })
            }
        } else {
            res.redirect('/')
        }
    })




})

module.exports = router