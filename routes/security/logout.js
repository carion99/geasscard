const express = require('express')
const router = express.Router()
const config = require('../../config/config')

/* GET home page. */
router.get('/', function(req, res, next) {
    if (req.session.codeUser) {
        req.session.destroy()
        res.redirect('/login')
    } else {
        
        let url = config.env.domaine + req.baseUrl 
        if (req.baseUrl === "/logout") {
            res.redirect('/')
        } else {
            res.redirect(url)
        }        
    }
})

module.exports = router
