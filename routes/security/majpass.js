const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('security/majpass', { title: 'GeassCard: Réinitialisation du mot de passe' })
})

module.exports = router
