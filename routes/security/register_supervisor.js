const express = require('express')
const router = express.Router()
const html_specialchars = require('html-specialchars')
const emailValidator = require('email-validator')
const mailer = require('nodemailer')

const Hashub = require('../../config/hashub')

const User = require('../../models/User')
const Superviseur = require('../../models/Superviseur')
const AttenteValidation = require('../../models/AttenteValidation')
const Parrain = require('../../models/Parrain')
const Filleul = require('../../models/Filleul')

const env = require('../../config/config').env
const { informationEmailSupport } = require('../../config/constante')
const URLBASE = env.domaine
const infoEmail = informationEmailSupport()

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser) {
        if (sess.status === "root") {
            res.redirect('/compte/root')
        }
        if (sess.status === "client") {
            res.redirect('/compte/client')
        }
        if (sess.status === "presta") {
            res.redirect('/compte/presta')
        }
        if (sess.status === "superviseur") {
            res.redirect('/compte/superviseur')
        }
    } else {
        res.render('security/register_supervisor', {
            title: 'GeassCard: Inscription'
        })
    }
})

router.get('/:code', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser) {
        if (sess.status === "root") {
            res.redirect('/compte/root')
        }
        if (sess.status === "client") {
            res.redirect('/compte/client')
        }
        if (sess.status === "presta") {
            res.redirect('/compte/presta')
        }
        if (sess.status === "superviseur") {
            res.redirect('/compte/superviseur')
        }
    } else {
        let code = req.params.code
        Parrain.findByOneField('hachac', code, (xparrain) => {
            if (xparrain.length === 1) {
                res.render('security/register_supervisor', {
                    title: 'GeassCard: Inscription',
                    code: code
                })
            } else {
                res.redirect('/')
            }
        })

    }
})

router.post('/', (req, res, next) => {
    let data = req.body

    let nom = html_specialchars.escape(data.nom)
    let prenom = html_specialchars.escape(data.prenom)
    let genre = html_specialchars.escape(data.genre)
    //let telephone = html_specialchars.escape(data.telephone)
    let telephone = "0"
    let email = html_specialchars.escape(data.email)
    let motdepasse = html_specialchars.escape(data.motdepasse)
    let cfmotdepasse = html_specialchars.escape(data.cfmotdepasse)

    let error = ""

    if (nom && prenom && genre && telephone && email && motdepasse && cfmotdepasse) {

        if (emailValidator.validate(email)) {
            let nbpass = 8
            console.log('ok');
            User.findByOneField('username', email, (futuser) => {
                console.log(futuser);
                if (futuser.length === 0) {
                    if (motdepasse.length >= nbpass) {
                        if (cfmotdepasse === motdepasse) {
                            let code = Superviseur.genCodeSuperviseur()
                            let hashpass = Hashub.crypt(motdepasse)
                            let status = "nosuperviseur"
                            let user = {
                                codeUser: code,
                                nom: nom,
                                prenom: prenom,
                                username: email,
                                password: hashpass,
                                status: status
                            }
                            let superviseur = {
                                codeSuperviseur: code,
                                nom: nom,
                                prenom: prenom,
                                genre: genre,
                                email: email,
                                telephone: telephone,
                                photo: ""
                            }

                            Superviseur.create(superviseur, (msg) => {
                                console.log(msg)
                            })

                            User.create(user, (msg) => {
                                console.log(msg)
                            })

                            let strmel = code + email + motdepasse

                            let codelien = Hashub.crypt(strmel)
                            let urlval = URLBASE + "/confirm/supervisor/" + codelien

                            let codav = AttenteValidation.genCodeAv()

                            let attente = {
                                codeAv: codav,
                                codeUser: code,
                                hachar: codelien,
                                status: "nosuperviseur"
                            }

                            AttenteValidation.create(attente, (msg) => {
                                console.log(msg)
                            })

                            const emailSubject = 'GeassCard: Confirmation Compte Superviseur'
                            let smtpTransport = mailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: infoEmail.username,
                                    pass: infoEmail.password
                                }
                            })
                            let mail = {
                                from: infoEmail.username,
                                to: email,
                                subject: emailSubject,
                                html: urlval
                            }

                            smtpTransport.sendMail(mail, (error, response) => {
                                if (error) {
                                    console.log("Erreur lors de l'envoi du mail!")
                                    console.log(error)
                                } else {
                                    console.log("Mail envoyé avec succès!")
                                }
                                smtpTransport.close()
                            })

                            res.redirect('/confirm/wellregister')

                        } else {
                            error = "Mot de passe non correspondant"
                            let info = req.body
                            console.log(error)
                            res.render('security/register_supervisor', {
                                title: 'GeassCard: Inscription',
                                info: info,
                                error: error
                            })
                        }
                    } else {
                        error = "Mot de passe doit contenir au moins 8 caractères"
                        let info = req.body
                        console.log(error)
                        res.render('security/register_supervisor', {
                            title: 'GeassCard: Inscription',
                            info: info,
                            error: error
                        })
                    }
                } else {
                    error = "Email Déja Utilisé"
                    let info = req.body
                    console.log(error)
                    res.render('security/register_supervisor', {
                        title: 'GeassCard: Inscription',
                        info: info,
                        error: error
                    })
                }
            })
        } else {
            error = "Format Email Incorrecte"
            let info = req.body
            console.log(error)
            res.render('security/register_supervisor', {
                title: 'GeassCard: Inscription',
                info: info,
                error: error
            })
        }
    } else {
        error = "Tous les champs sont obligatoires"
        let info = req.body
        console.log(error)
        res.render('security/register_supervisor', {
            title: 'GeassCard: Inscription',
            info: info,
            error: error
        })
    }



})

router.post('/:code', (req, res, next) => {
    let code = req.params.code
    Parrain.findByOneField('hachac', code, (xparrain) => {
        if (xparrain.length === 1) {
            let data = req.body
            let email = html_specialchars.escape(data.email)
            let pass = html_specialchars.escape(data.pass)
            let cfpass = html_specialchars.escape(data.cfpass)

            let error = ""

            if (email && pass && cfpass) {

                if (emailValidator.validate(email)) {
                    let nbpass = 8
                    User.findByOneField('username', email, (futuser) => {
                        if (futuser.length === 0) {
                            if (pass.length >= nbpass) {
                                if (cfpass === pass) {
                                    let code = Superviseur.genCodeSuperviseur()
                                    let hashpass = Hashub.crypt(pass)
                                    let status = "noclient"
                                    let nom = ""
                                    let prenom = ""
                                    let user = {
                                        codeUser: code,
                                        nom: nom,
                                        prenom: prenom,
                                        username: email,
                                        password: hashpass,
                                        status: status
                                    }

                                    let codepar = xparrain[0].codeParrain
                                    let codefi = Filleul.genCodeFilleul(codepar)


                                    let filleul = {
                                        codeFilleul: codefi,
                                        codeParrain: codepar,
                                        email: email,
                                    }

                                    User.create(user, (msg) => {
                                        console.log(msg)
                                    })

                                    Filleul.create(filleul, (msg) => {
                                        console.log(msg)
                                    })

                                    let strmel = code + email + pass

                                    let codelien = Hashub.crypt(strmel)
                                    let urlval = URLBASE + "/confirm/" + codelien

                                    let codav = AttenteValidation.genCodeAv()

                                    let attente = {
                                        codeAv: codav,
                                        codeUser: code,
                                        hachar: codelien,
                                        status: "noclient"
                                    }

                                    AttenteValidation.create(attente, (msg) => {
                                        console.log(msg)
                                    })

                                    const emailUser = 'mediyann99@gmail.com'
                                    const emailPass = 'yann57357598'
                                    const emailSubject = 'GeassCard: Confirmation Compte Superviseur'
                                    let smtpTransport = mailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: emailUser,
                                            pass: emailPass
                                        }
                                    })
                                    let mail = {
                                        from: emailUser,
                                        to: email,
                                        subject: emailSubject,
                                        html: urlval
                                    }

                                    smtpTransport.sendMail(mail, (error, response) => {
                                        if (error) {
                                            console.log("Erreur lors de l'envoi du mail!")
                                            console.log(error)
                                        } else {
                                            console.log("Mail envoyé avec succès!")
                                        }
                                        smtpTransport.close()
                                    })

                                    res.redirect('/confirm/wellregister')

                                } else {
                                    error = "Mot de passe non correspondant"
                                    let info = req.body
                                    console.log(error)
                                    res.render('security/register_supervisor', {
                                        title: 'GeassCard: Inscription',
                                        info: info,
                                        error: error
                                    })
                                }
                            } else {
                                error = "Mot de passe doit contenir au moins 8 caractères"
                                let info = req.body
                                console.log(error)
                                res.render('security/register_supervisor', {
                                    title: 'GeassCard: Inscription',
                                    info: info,
                                    error: error
                                })
                            }
                        } else {
                            error = "Email Déja Utilisé"
                            let info = req.body
                            console.log(error)
                            res.render('security/register_supervisor', {
                                title: 'GeassCard: Inscription',
                                info: info,
                                error: error
                            })
                        }
                    })
                } else {
                    error = "Format Email Incorrecte"
                    let info = req.body
                    console.log(error)
                    res.render('security/register_supervisor', {
                        title: 'GeassCard: Inscription',
                        info: info,
                        error: error
                    })
                }
            } else {
                error = "Tous les champs sont obligatoires"
                let info = req.body
                console.log(error)
                res.render('security/register_supervisor', {
                    title: 'GeassCard: Inscription',
                    info: info,
                    error: error
                })
            }
        } else {
            res.redirect('/')
        }
    })




})

module.exports = router