const express = require('express')
const router = express.Router()

const Sidebare = require('../../config/sidebare')
const TabSideBase = require('../../config/tabsidebase')

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 5
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Prestataire"
        res.render('root/gestion_prestataire', {
            title: 'GeassCard: Compte Client => Gestion Prestataire',
            idpage: idpage,
            tabside: tabside,
            ident: ident,
            role: role,
            page: page
        })
    } else {
        res.redirect('/')
    }

})

module.exports = router