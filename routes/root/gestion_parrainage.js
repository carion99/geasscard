const express = require('express')
const router = express.Router()

const Parrain = require('../../models/Parrain')
const Filleul = require('../../models/Filleul')
const User = require('../../models/User')

const Sidebare = require('../../config/sidebare')
const TabSideBase = require('../../config/tabsidebase')

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 6
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Parrainage"
        res.render('root/gestion_parrainage', {
            title: 'GeassCard: Compte Root => Gestion Parrainage',
            idpage: idpage,
            tabside: tabside,
            ident: ident,
            role: role,
            page: page,
            tabside: tabside
        })
    } else {
        res.redirect('/')
    }

})

router.get('/liste_parrains', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        Parrain.all((futparrains) => {

            if (futparrains.length > 0) {
                let info = "yes"

                var idpage = 6
                var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

                let ident = sess.nom
                let role = sess.status
                let page = "Gestion Parrainage"

                res.render('root/gparrain/liste_parrain', {
                    title: 'GeassCard: Compte Client => Gestion Parrainage',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    parrains: futparrains
                })


            } else {
                let info = "no"

                var idpage = 6
                var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

                let ident = sess.nom
                let role = sess.status
                let page = "Gestion Parrainage"
                res.render('root/gparrain/liste_parrain', {
                    title: 'GeassCard: Compte Client => Gestion Parrainage',
                    active: 4,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    tabside: tabside,
                })
            }
        })

    } else {
        res.redirect('/')
    }
})

router.get('/liste_filleuls', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        Filleul.all((futfilleuls) => {

            if (futfilleuls.length > 0) {
                let info = "yes"

                var idpage = 6
                var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

                let ident = sess.nom
                let role = sess.status
                let page = "Gestion Parrainage"

                res.render('root/gparrain/liste_filleul', {
                    title: 'GeassCard: Compte Client => Gestion Parrainage',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    filleuls: futfilleuls
                })


            } else {
                let info = "no"

                var idpage = 6
                var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

                let ident = sess.nom
                let role = sess.status
                let page = "Gestion Parrainage"
                res.render('root/gparrain/liste_filleul', {
                    title: 'GeassCard: Compte Client => Gestion Parrainage',
                    active: 4,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    idpage: idpage,
                    tabside: tabside,
                })
            }
        })

    } else {
        res.redirect('/')
    }
})

router.get('/:codeparrain/liste_filleuls', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        let codeparrain = req.params.codeparrain
        User.findByOneField('code_user', codeparrain, (futuser) => {
            if (futuser.length === 1) {
                Filleul.findByOneField('code_parrain', codeparrain, (futfilleuls) => {

                    if (futfilleuls.length > 0) {
                        let info = "yes"

                        var idpage = 6
                        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

                        let ident = sess.nom
                        let role = sess.status
                        let page = "Gestion Parrainage"

                        res.render('root/gparrain/filleul_parrain', {
                            title: 'GeassCard: Compte Client => Gestion Parrainage',
                            idpage: idpage,
                            tabside: tabside,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            codeParrain: futuser[0].codeUser,
                            filleuls: futfilleuls
                        })


                    } else {
                        let info = "no"

                        var idpage = 6
                        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

                        let ident = sess.nom
                        let role = sess.status
                        let page = "Gestion Parrainage"
                        res.render('root/gparrain/filleul_parrain', {
                            title: 'GeassCard: Compte Client => Gestion Parrainage',
                            active: 4,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            codeParrain: futuser[0].codeUser,
                            idpage: idpage,
                            tabside: tabside,
                        })
                    }
                })
            } else {
                res.redirect('/compte/root/gestion_parrainage')
            }
        })

    } else {
        res.redirect('/')
    }
})

module.exports = router