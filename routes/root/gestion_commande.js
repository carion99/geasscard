const express = require('express')
const router = express.Router()

const Commande = require('../../models/Commande')
const PanierCommande = require('../../models/PanierCommande')
const LignePanierCommande = require('../../models/LignePanierCommande')
const Client = require('../../models/Client')
const Root = require('../../models/Root')

const Sidebare = require('../../config/sidebare')
const TabSideBase = require('../../config/tabsidebase')

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        Commande.all((futcommande) => {
            if (futcommande.length !== 0) {

                let commandes = []

                futcommande.forEach(commande => {
                    if ((commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") && commande.confirmationClient === 1) {
                        commandes.push(commande)
                    }
                });

                if (commandes.length !== 0) {
                    let info = "yes"

                    res.render('root/gestion_commande', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        commandes: commandes,
                        info: info
                    })
                } else {
                    let info = "no"
                    res.render('root/gestion_commande', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info
                    })
                }


            } else {
                let info = "no"
                res.render('root/gestion_commande', {
                    title: 'GeassCard: Compte Root => Gestion Commandes',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }

        })


    } else {
        res.redirect('/')
    }

})

router.get('/nouvelles_commandes', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        Commande.all((futcommande) => {
            if (futcommande.length !== 0) {

                let commandes = []

                futcommande.forEach(commande => {
                    var date = new Date(commande.dateRegister)
                    var today = new Date();

                    if ((commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") && commande.confirmationClient === 1) {
                        if (date.getDate() == today.getDate() && date.getMonth() == today.getMonth() && date.getFullYear() == today.getFullYear()) {
                            commandes.push(commande)
                        }
                    }
                });

                if (commandes.length !== 0) {
                    let info = "yes"

                    res.render('root/gcommande/nouvelles_commandes', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        commandes: commandes,
                        info: info
                    })
                } else {
                    let info = "no"
                    res.render('root/gcommande/nouvelles_commandes', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info
                    })
                }


            } else {
                let info = "no"
                res.render('root/gcommande/nouvelles_commandes', {
                    title: 'GeassCard: Compte Root => Gestion Commandes',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }

        })


    } else {
        res.redirect('/')
    }

})


router.get('/toutes_commandes', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        Commande.all((futcommande) => {
            if (futcommande.length !== 0) {

                let commandes = []

                futcommande.forEach(commande => {
                    if ((commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") && commande.confirmationClient === 1) {
                        commandes.push(commande)
                    }
                });

                if (commandes.length !== 0) {
                    let info = "yes"

                    res.render('root/gcommande/toutes_commandes', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        commandes: commandes,
                        info: info
                    })
                } else {
                    let info = "no"
                    res.render('root/gcommande/toutes_commandes', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info
                    })
                }


            } else {
                let info = "no"
                res.render('root/gcommande/toutes_commandes', {
                    title: 'GeassCard: Compte Root => Gestion Commandes',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }

        })


    } else {
        res.redirect('/')
    }

})


router.get('/statut1', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        Commande.all((futcommande) => {
            if (futcommande.length !== 0) {

                let commandes = []

                futcommande.forEach(commande => {

                    if ((commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") && commande.confirmationClient === 1) {
                        if (commande.statut === "1") {
                            commandes.push(commande)
                        }
                    }
                });

                if (commandes.length !== 0) {
                    let info = "yes"

                    res.render('root/gcommande/statut1', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        commandes: commandes,
                        info: info
                    })
                } else {
                    let info = "no"
                    res.render('root/gcommande/statut1', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info
                    })
                }


            } else {
                let info = "no"
                res.render('root/gcommande/statut1', {
                    title: 'GeassCard: Compte Root => Gestion Commandes',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }

        })


    } else {
        res.redirect('/')
    }

})


router.get('/statut2', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        Commande.all((futcommande) => {
            if (futcommande.length !== 0) {

                let commandes = []

                futcommande.forEach(commande => {

                    if ((commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") && commande.confirmationClient === 1) {
                        if (commande.statut === "2") {
                            commandes.push(commande)
                        }
                    }
                });

                if (commandes.length !== 0) {
                    let info = "yes"

                    res.render('root/gcommande/statut2', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        commandes: commandes,
                        info: info
                    })
                } else {
                    let info = "no"
                    res.render('root/gcommande/statut2', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info
                    })
                }


            } else {
                let info = "no"
                res.render('root/gcommande/statut2', {
                    title: 'GeassCard: Compte Root => Gestion Commandes',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }

        })


    } else {
        res.redirect('/')
    }

})


router.get('/statut3', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        Commande.all((futcommande) => {
            if (futcommande.length !== 0) {

                let commandes = []

                futcommande.forEach(commande => {

                    if ((commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") && commande.confirmationClient === 1) {
                        if (commande.statut === "3") {
                            commandes.push(commande)
                        }
                    }
                });

                if (commandes.length !== 0) {
                    let info = "yes"

                    res.render('root/gcommande/statut3', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        commandes: commandes,
                        info: info
                    })
                } else {
                    let info = "no"
                    res.render('root/gcommande/statut3', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info
                    })
                }


            } else {
                let info = "no"
                res.render('root/gcommande/statut3', {
                    title: 'GeassCard: Compte Root => Gestion Commandes',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }

        })


    } else {
        res.redirect('/')
    }

})


router.get('/statut4', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        Commande.all((futcommande) => {
            if (futcommande.length !== 0) {

                let commandes = []

                futcommande.forEach(commande => {

                    if ((commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") && commande.confirmationClient === 1) {
                        if (commande.statut === "4") {
                            commandes.push(commande)
                        }
                    }
                });

                if (commandes.length !== 0) {
                    let info = "yes"

                    res.render('root/gcommande/statut4', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        commandes: commandes,
                        info: info
                    })
                } else {
                    let info = "no"
                    res.render('root/gcommande/statut4', {
                        title: 'GeassCard: Compte Root => Gestion Commandes',
                        idpage: idpage,
                        tabside: tabside,
                        ident: ident,
                        role: role,
                        page: page,
                        info: info
                    })
                }


            } else {
                let info = "no"
                res.render('root/gcommande/statut4', {
                    title: 'GeassCard: Compte Root => Gestion Commandes',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }

        })


    } else {
        res.redirect('/')
    }

})

router.post('/vers_statut/:codestatut/:codecommande', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)


        let codestatut = req.params.codestatut
        let codecommande = req.params.codecommande

        let passpecial = req.body.password


        if (codestatut == '2' || codestatut == '3' || codestatut == '4') {
            Commande.findByTwoField('code_commande', codecommande, 'confirmation_client', 1, (futcommande) => {
                if (futcommande.length !== 0) {
                    let commande = futcommande[0]
                    if (commande.moyenPaiement === "À La Livraison" || commande.moyenPaiement === "Mobile Money" || commande.moyenPaiement === "Solution Bancaire") {
                        Root.findByTwoField('code_root', sess.codeUser, 'passpecial', passpecial, (futroot) => {
                            if (futroot.length !== 0) {
                                switch (codestatut) {
                                    case '2':
                                        if (commande.statut === "1") {
                                            Commande.replaceByOneField(codecommande, 'statut', "2", (msg) => {
                                                console.log(msg)
                                                res.redirect('/compte/root/gestion_commande/voir/' + codecommande)
                                            })
                                        } else {
                                            console.log("Migration impossible");

                                            res.redirect('/compte/root/gestion_commande/voir/' + codecommande)
                                        }
                                        break;
                                    case '3':
                                        if (commande.statut === "1") {
                                            Commande.replaceByOneField(codecommande, 'statut', "3", (msg) => {
                                                console.log(msg)
                                                res.redirect('/compte/root/gestion_commande/voir/' + codecommande)
                                            })
                                        } else {
                                            console.log("Migration impossible");

                                            res.redirect('/compte/root/gestion_commande/voir/' + codecommande)
                                        }
                                        break;
                                    case '4':
                                        if (commande.statut === "2" || commande.statut === "3") {
                                            Commande.replaceByOneField(codecommande, 'statut', "4", (msg) => {
                                                console.log(msg)
                                                res.redirect('/compte/root/gestion_commande/voir/' + codecommande)
                                            })
                                        } else {
                                            console.log("Migration impossible");

                                            res.redirect('/compte/root/gestion_commande/voir/' + codecommande)
                                        }
                                        break;

                                    default:
                                        res.redirect('/compte/root/gestion_commande/voir/' + codecommande)
                                        break;
                                }
                            } else {
                                console.log("Root inconnu");

                                res.redirect('/compte/root/gestion_commande')
                            }
                        })
                    } else {
                        console.log("Code Commande invalide");

                        res.redirect('/compte/root/gestion_commande')
                    }
                } else {
                    console.log("Commande Inconnue");

                    res.redirect('/compte/root/gestion_commande')
                }
            })
        } else {
            console.log("Code Statut invalide : choix possible => 2 ou 3 ou 4");

            res.redirect('/compte/root/gestion_commande')
        }
    } else {
        res.redirect('/')
    }
})


router.get('/voir/:codecommande', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 1
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Commandes"

        let codecommande = req.params.codecommande

        Commande.findByOneField('code_commande', codecommande, (futcommande) => {
            if (futcommande.length === 1) {

                let commande = futcommande[0]

                PanierCommande.findByOneField('code_panier_commande', commande.codePanier, (futpanier) => {
                    let panier = futpanier[0]
                    console.log(commande.codePanier)
                    console.log(panier)
                    //let panier = futpanier[0]

                    LignePanierCommande.findByOneField('code_panier_commande', panier.codePanierCommande, (futlignepanier) => {
                        let touteslignes = futlignepanier

                        Client.findByOneField('code_client', commande.codeClient, (futclient) => {
                            let client = futclient[0]

                            res.render('root/gcommande/info_commande', {
                                title: 'GeassCard: Compte Root => Gestion Commandes',
                                idpage: idpage,
                                tabside: tabside,
                                ident: ident,
                                role: role,
                                page: page,
                                infoclient: client,
                                panier: panier,
                                commande: commande,
                                touteslignes: touteslignes
                            })
                        })
                    })
                })

            } else {
                res.redirect('/compte/root/gestion_commande')
            }
        })

    } else {
        res.redirect('/')
    }
})

module.exports = router