const express = require('express')
const router = express.Router()
const html_specialchars = require('html-specialchars')

const BonDeReduction = require('../../models/BonDeReduction')

const Sidebare = require('../../config/sidebare')
const TabSideBase = require('../../config/tabsidebase')

/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 2
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Bon de Reduction"

        BonDeReduction.all((futreductions) => {
            if (futreductions.length > 0) {
                let info = "yes"
                res.render('root/gestion_reduction', {
                    title: 'GeassCard: Compte Root => Gestion Bon de Reduction',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info,
                    reductions: futreductions
                })
            } else {
                let info = "no"
                res.render('root/gestion_reduction', {
                    title: 'GeassCard: Compte Root => Gestion Bon de Reduction',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    info: info
                })
            }
        })


    } else {
        res.redirect('/')
    }

})

router.get('/new', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 2
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Bon de Reduction"
        res.render('root/new_bon_de_reduction', {
            title: 'GeassCard: Compte Root => Gestion Bon de Reduction',
            idpage: idpage,
            tabside: tabside,
            ident: ident,
            role: role,
            page: page
        })
    } else {
        res.redirect('/')
    }
})

router.post('/new', (req, res, next) => {
    let sess = req.session

    if (sess.codeUser && sess.status === "root") {

        var idpage = 2
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Bon de Reduction"

        let data = req.body

        let error = ""
        let info = data

        let code = html_specialchars.escape(data.code)
        let somme = html_specialchars.escape(data.somme)

        if (somme) {
            let codebdr = BonDeReduction.genCodeBdr()

            if (code !== "") {
                BonDeReduction.findByOneField('code_reduction', code, (futreduction) => {

                    if (futreduction.length === 0) {
                        let reduction = {
                            codeBdr: codebdr,
                            codeReduction: code,
                            sommeReduction: somme,
                            activeReduction: 0
                        }
                        console.log(reduction);

                        BonDeReduction.create(reduction, (msg) => {
                            console.log(msg)
                        })

                        let msgsuccess = "Bon De Reduction bien ajouté"
                        res.render('root/new_bon_de_reduction', {
                            title: 'GeassCard: Compte Root => Gestion Bon de Reduction',
                            idpage: idpage,
                            tabside: tabside,
                            ident: ident,
                            role: role,
                            page: page,
                            msgsuccess: msgsuccess
                        })
                    } else {
                        let error = "Ce code de réduction existe déja"
                        res.render('root/new_bon_de_reduction', {
                            title: 'GeassCard: Compte Root => Gestion Bon de Reduction',
                            idpage: idpage,
                            tabside: tabside,
                            ident: ident,
                            role: role,
                            page: page,
                            info: info,
                            error: error
                        })
                    }
                })
            } else {
                let codereduct = BonDeReduction.genCodeReduction()
                let reduction = {
                    codeBdr: codebdr,
                    codeReduction: codereduct,
                    sommeReduction: somme,
                    activeReduction: 0
                }
                console.log(reduction);

                BonDeReduction.create(reduction, (msg) => {
                    console.log(msg)
                })

                let msgsuccess = "Bon De Reduction bien ajouté"
                res.render('root/new_bon_de_reduction', {
                    title: 'GeassCard: Compte Root => Gestion Bon de Reduction',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    msgsuccess: msgsuccess
                })
            }


        } else {
            let error = "Somme Obligatoire"
            res.render('root/new_bon_de_reduction', {
                title: 'GeassCard: Compte Root => Gestion Bon de Reduction',
                idpage: idpage,
                tabside: tabside,
                ident: ident,
                role: role,
                page: page,
                info: info,
                error: error
            })
        }




    } else {
        res.redirect('/')
    }
})


/**
 *  Permet d'effectuer les actions d'activation, désactivation et suppression sur les bon de reductions
 */
router.get('/:codebdr/:ordre', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 2
        var tabside = Sidebare.activeSidebare(TabSideBase.root(), idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Bon de Reduction"

        let codebdr = req.params.codebdr
        let ordre = req.params.ordre

        BonDeReduction.findByOneField('code_bdr', codebdr, (futreduction) => {
            if (futreduction.length === 1) {
                switch (ordre) {
                    case 'active':
                        BonDeReduction.replaceByOneField(codebdr, 'active_reduction', 1, (msg) => {
                            console.log(msg)
                        })
                        res.redirect('/compte/root/gestion_reduction')
                        break;
                    case 'deactive':
                        BonDeReduction.replaceByOneField(codebdr, 'active_reduction', 0, (msg) => {
                            console.log(msg)
                        })
                        res.redirect('/compte/root/gestion_reduction')
                        break;
                    case 'remove':
                        BonDeReduction.remove(futreduction[0], (msg) => {
                            console.log(msg)
                        })
                        res.redirect('/compte/root/gestion_reduction')
                        break;
                    default:
                        res.redirect('/compte/root/gestion_reduction')
                        break;
                }
            } else {
                res.redirect('/compte/root/gestion_reduction')
            }
        })
    } else {
        res.redirect('/')
    }
})

module.exports = router