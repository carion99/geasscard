const express = require('express')
const router = express.Router()
const VerifUser = require('../../config/verifuser')

const User = require('../../models/User')
const Client = require('../../models/Client')
const Commande = require('../../models/Commande')

const Sidebare = require('../../config/sidebare')
const TabSideBase = require('../../config/tabsidebase')


/* GET home page. */
router.get('/', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 0
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Bienvenue"


        res.render('root/index_root', {
            title: 'GeassCard: Compte Root => Accueil',
            idpage: idpage,
            tabside: tabside,
            ident: ident,
            role: role,
            page: page,
        })
    } else {
        res.redirect('/')
    }

})

router.get('/actu_clients', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 0
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Actualité Clients"


        User.findByOneField('status', 'client', (clients) => {
            let totalClients = 0
            let totalVeilleClients = 0
            let totalNouveauClients = 0
            let augmentation = 0
            if (clients !== null) {

                totalClients = clients.length
                clients.forEach(client => {
                    var date = new Date(client.dateRegister)
                    var today = new Date()
                    var yesterday = new Date() // récupère la date actuelle

                    yesterday.setTime(yesterday.getTime() - 86400000);

                    if (date.getDate() == today.getDate() && date.getMonth() == today.getMonth() && date.getFullYear() == today.getFullYear()) {
                        totalNouveauClients += 1
                    }

                    if (date.getDate() == yesterday.getDate() && date.getMonth() == yesterday.getMonth() && date.getFullYear() == yesterday.getFullYear()) {
                        totalVeilleClients += 1
                    }
                })

                if (totalVeilleClients !== 0) {
                    augmentation = (totalNouveauClients / totalVeilleClients) * 100 + "%"
                } else {
                    augmentation = "+ " + totalNouveauClients + " Clients"
                }

                res.render('root/actu/client', {
                    title: 'GeassCard: Compte Root => Accueil',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    totalClients: totalClients,
                    totalNouveauClients: totalNouveauClients,
                    augmentation: augmentation
                })
            } else {
                res.render('root/actu/client', {
                    title: 'GeassCard: Compte Root => Accueil',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    totalClients: totalClients,
                    totalNouveauClients: totalNouveauClients,
                    augmentation: augmentation
                })
            }
        })
    } else {
        res.redirect('/')
    }
})


router.get('/actu_commandes', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 0
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Actualité Commandes"

        Commande.findByOneField('confirmation_client', 1, (commandes) => {
            let totalCommandes = 0
            let totalCommandesStatut1 = 0
            let totalCommandesStatut2 = 0
            let totalCommandesStatut3 = 0
            let totalCommandesStatut4 = 0
            let totalVeilleCommandes = 0
            let totalNouvelleCommandes = 0
            let augmentation = 0
            if (commandes !== null) {

                totalCommandes = commandes.length
                commandes.forEach(commande => {
                    var date = new Date(commande.dateRegister)
                    var today = new Date()
                    var yesterday = new Date() // récupère la date actuelle

                    yesterday.setTime(yesterday.getTime() - 86400000);

                    if (date.getDate() == today.getDate() && date.getMonth() == today.getMonth() && date.getFullYear() == today.getFullYear()) {
                        totalNouvelleCommandes += 1
                    }

                    if (date.getDate() == yesterday.getDate() && date.getMonth() == yesterday.getMonth() && date.getFullYear() == yesterday.getFullYear()) {
                        totalVeilleCommandes += 1
                    }

                    if (commande.statut == "1") {
                        totalCommandesStatut1 += 1
                    }

                    if (commande.statut == "2") {
                        totalCommandesStatut2 += 1
                    }

                    if (commande.statut == "3") {
                        totalCommandesStatut3 += 1
                    }

                    if (commande.statut == "4") {
                        totalCommandesStatut4 += 1
                    }
                })

                if (totalVeilleCommandes !== 0) {
                    augmentation = (totalNouvelleCommandes / totalVeilleCommandes) * 100 + "%"
                } else {
                    augmentation = "+ " + totalNouvelleCommandes + " Commandes"
                }

                res.render('root/actu/commande', {
                    title: 'GeassCard: Compte Root => Accueil',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    totalCommandes: totalCommandes,
                    totalCommandesStatut1: totalCommandesStatut1,
                    totalCommandesStatut2: totalCommandesStatut2,
                    totalCommandesStatut3: totalCommandesStatut3,
                    totalCommandesStatut4: totalCommandesStatut4,
                    totalNouvelleCommandes: totalNouvelleCommandes,
                    augmentation: augmentation
                })
            } else {
                res.render('root/actu/commande', {
                    title: 'GeassCard: Compte Root => Accueil',
                    idpage: idpage,
                    tabside: tabside,
                    ident: ident,
                    role: role,
                    page: page,
                    totalCommandes: totalCommandes,
                    totalNouvelleCommandes: totalNouvelleCommandes,
                    augmentation: augmentation
                })
            }
        })
    } else {
        res.redirect('/')
    }
})

router.get('/comptabilite', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        var idpage = 0
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Bienvenue"


        res.render('root/actu/comptabilite', {
            title: 'GeassCard: Compte Root => Accueil',
            idpage: idpage,
            tabside: tabside,
            ident: ident,
            role: role,
            page: page,
        })
    } else {
        res.redirect('/')
    }
})

module.exports = router