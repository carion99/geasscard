const express = require('express')
const router = express.Router()

const User = require('../../models/User')
const Client = require('../../models/Client')
const InfoSocieteClient = require('../../models/InfoSocieteClient')
const InfoFichierClient = require('../../models/InfoFichierClient')
const InfoResocialClient= require('../../models/InfoResocialClient')
const Parrain = require('../../models/Parrain')

const Sidebare = require('../../config/sidebare')
const TabSideBase = require('../../config/tabsidebase')

router.get('/', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 4
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Client"

        res.render('root/gestion_client', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Client',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                })
        
    }        
})

/* GET home page. */
router.get('/nouveaux', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 4
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Client"
        User.findByOneField('status', 'client', (clients) => {
            //console.log(clients);
            let nouveaux = []
            let nouveau = {}

            clients.forEach(client => {
                var date = new Date(client.dateRegister)
                var today = new Date();
                if(date.getDate() == today.getDate() && date.getMonth()  == today.getMonth() && date.getFullYear() == today.getFullYear()) {
                    nouveau = client
                    nouveaux.push(nouveau)
                }
            });
            
            if (nouveaux.length > 0) {
                let info = "yes"
                res.render('root/gclient/nouveau', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Client',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    nouveaux : nouveaux,
                    info : info
                })
            } else {
                let info = "no"
                res.render('root/gclient/nouveau', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Client',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info
                })
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})


/* GET home page. */
router.get('/inscrits', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 4
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Client"
        User.findByOneField('status', 'client', (clients) => {
            console.log(clients);
            
            if (clients.length > 0) {
                let info = "yes"
                res.render('root/gclient/inscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Client',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    clients : clients,
                    info : info
                })
            } else {
                let info = "no"
                res.render('root/gclient/inscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Client',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info
                })
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})

/* GET home page. */
router.get('/noinscrits', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 4
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Client"
        User.findByOneField('status', 'noclient', (clients) => {
            console.log(clients);
            
            if (clients.length > 0) {
                let info = "yes"
                res.render('root/gclient/noinscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Client',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    clients : clients,
                    info : info
                })
            } else {
                let info = "no"
                res.render('root/gclient/noinscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Client',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info
                })
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})

router.get('/voir/:codeclient', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 4
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Client"

        let codeclient = req.params.codeclient

        Client.findByOneField('code_client', codeclient, (futclient) => {
            if (futclient.length === 1) {
                let infoperso = futclient[0]
                InfoSocieteClient.findByOneField('code_client', codeclient, (futinfosociete) => {
                    let infosociete = futinfosociete[0]
                    InfoResocialClient.findByOneField('code_client', codeclient, (futinforeso) => {
                        let inforeso = futinforeso[0]
                        InfoFichierClient.findByOneField('code_client', codeclient, (futinfofichier) => {
                            let infofichier = futinfofichier[0]
                            User.findByOneField('code_user', codeclient, (futuser) => {
                                let infouser = futuser[0]

                                console.log(infouser.permissionModificationData)

                                res.render('root/info_client', 
                                { 
                                    title: 'GeassCard: Compte Root => Gestion Client',
                                    idpage: idpage,
                                    tabside: tabside,
                                    ident : ident,
                                    role : role,
                                    page : page,
                                    infouser: infouser,
                                    infoperso   : infoperso,
                                    inforeso    : inforeso,
                                    infosociete : infosociete,
                                    infofichier : infofichier
                                })

                            })
                        })
                    })
                })
            } else {
                console.log("client inexistant")
                res.redirect('/compte/root/gestion_client')
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})


router.get('/supp/:code', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
      let codeclient = req.params.code
      User.findByOneField('code_user', codeclient, (futclient) => {
          if (futclient.length === 1) {
            let user = futclient[0]
            let client = {
                codeClient: codeclient
            }
            let infoSocieteClient = {
                codeClient: codeclient
            }
            let infoFichierClient = {
                codeClient: codeclient
            }
            let infoResocialClient = {
                codeClient: codeclient
            }
            let parrain = {
                codeParrain: codeclient
            }

            User.remove(user, (msg) => {
                console.log(msg);
            })
            Client.remove(client, (msg) => {
                console.log(msg)
            })
            Parrain.remove(parrain, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.remove(infoSocieteClient, (msg) => {
                console.log(msg);
            })
            InfoFichierClient.remove(infoFichierClient, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.remove(infoResocialClient, (msg) => {
                console.log(msg)
            })

            res.redirect('/compte/root/gestion_client')

          } else {
            res.redirect('/compte/root/gestion_client')
          }
      })  
    } else {
        res.redirect('/')
    }
})

router.get('/effect_operation/:codeclient/:type_operation', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {

        let codeclient = req.params.codeclient
        let type_operation = req.params.type_operation

        console.log(codeclient)
        console.log(type_operation)

        User.findByOneField('code_user', codeclient, (futuser) => {
            if(futuser.length == 1){
                let user = futuser[0]
                if(
                    type_operation == "active_autorisation" || type_operation == "deactive_autorisation" 
                    || type_operation == "active_abonnement" || type_operation == "deactive_abonnement"
                ) {

                    switch (type_operation) {
                        case "active_autorisation":
                            User.replaceByOneField(user.codeUser, 'autorisation', 1, (msg) => {
                                console.log(msg)
                                res.redirect('/compte/root/gestion_client/voir/'+codeclient)
                            })
                            break;
                        case "deactive_autorisation":
                            User.replaceByOneField(user.codeUser, 'autorisation', 0, (msg) => {
                                console.log(msg)
                                res.redirect('/compte/root/gestion_client/voir/'+codeclient)
                            })
                            break;
                        case "active_abonnement":
                            User.replaceByOneField(user.codeUser, 'permission_modification_data', 1, (msg) => {
                                console.log(msg)
                                res.redirect('/compte/root/gestion_client/voir/'+codeclient)
                            })
                            break;
                        case "deactive_abonnement":
                            User.replaceByOneField(user.codeUser, 'permission_modification_data', 0, (msg) => {
                                console.log(msg)
                                res.redirect('/compte/root/gestion_client/voir/'+codeclient)
                            })
                            break;
                    
                        default:
                            let msg = "Opération non comprise..."
                            console.log(msg)
                            res.redirect('/compte/root/gestion_client/voir/'+codeclient)
                            break;
                    }

                } else {
                    res.redirect('/compte/root/gestion_client/voir/'+codeclient)
                }
            } else {
                res.redirect('/compte/root/gestion_client')
            }
        })
    } 
})

module.exports = router
