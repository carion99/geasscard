const express = require('express')
const router = express.Router()

const User = require('../../models/User')
const Superviseur = require('../../models/Superviseur')
const InfoSocieteClient = require('../../models/InfoSocieteClient')
const InfoFichierClient = require('../../models/InfoFichierClient')
const InfoResocialClient= require('../../models/InfoResocialClient')
const Parrain = require('../../models/Parrain')

const Sidebare = require('../../config/sidebare')
const TabSideBase = require('../../config/tabsidebase')

router.get('/', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 3
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Superviseur"

        res.render('root/gestion_superviseur', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                })
        
    }        
})

router.get('/new_superviseur', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 3
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Superviseur"

        res.render('root/gsuperviseur/new_superviseur', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                })
        
    }        
})



/* GET home page. */
router.get('/nouveaux', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 3
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Superviseur"
        User.findByOneField('status', 'superviseur', (superviseurs) => {
            //console.log(superviseurs);
            let nouveaux = []
            let nouveau = {}

            superviseurs.forEach(superviseur => {
                var date = new Date(superviseur.dateRegister)
                var today = new Date();
                if(date.getDate() == today.getDate() && date.getMonth()  == today.getMonth() && date.getFullYear() == today.getFullYear()) {
                    nouveau = superviseur
                    nouveaux.push(nouveau)
                }
            });
            
            if (nouveaux.length > 0) {
                let info = "yes"
                res.render('root/gsuperviseur/nouveau', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    nouveaux : nouveaux,
                    info : info
                })
            } else {
                let info = "no"
                res.render('root/gsuperviseur/nouveau', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info
                })
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})


/* GET home page. */
router.get('/inscrits', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 3
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Superviseur"
        User.findByOneField('status', 'superviseur', (superviseurs) => {
            console.log(superviseurs);
            
            if (superviseurs.length > 0) {
                let info = "yes"
                res.render('root/gsuperviseur/inscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    superviseurs : superviseurs,
                    info : info
                })
            } else {
                let info = "no"
                res.render('root/gsuperviseur/inscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info
                })
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})

/* GET home page. */
router.get('/noinscrits', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 3
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Superviseur"
        User.findByOneField('status', 'noclient', (superviseurs) => {
            console.log(superviseurs);
            
            if (superviseurs.length > 0) {
                let info = "yes"
                res.render('root/gsuperviseur/noinscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    superviseurs : superviseurs,
                    info : info
                })
            } else {
                let info = "no"
                res.render('root/gsuperviseur/noinscrit', 
                { 
                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                    idpage: idpage,
                    tabside: tabside,
                    ident : ident,
                    role : role,
                    page : page,
                    info : info
                })
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})

router.get('/voir/:codesuperviseur', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        
        var idpage = 3
        var tabside = Sidebare.activeSidebare(TabSideBase.root() , idpage)

        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Superviseur"

        let codesuperviseur = req.params.codesuperviseur

        Superviseur.findByOneField('code_client', codesuperviseur, (futclient) => {
            if (futclient.length === 1) {
                let infoperso = futclient[0]
                InfoSocieteClient.findByOneField('code_client', codesuperviseur, (futinfosociete) => {
                    let infosociete = futinfosociete[0]
                    InfoResocialClient.findByOneField('code_client', codesuperviseur, (futinforeso) => {
                        let inforeso = futinforeso[0]
                        InfoFichierClient.findByOneField('code_client', codesuperviseur, (futinfofichier) => {
                            let infofichier = futinfofichier[0]

                            res.render('root/info_client', 
                                { 
                                    title: 'GeassCard: Compte Root => Gestion Superviseur',
                                    idpage: idpage,
                    tabside: tabside,
                                    ident : ident,
                                    role : role,
                                    page : page,
                                    infoperso   : infoperso,
                                    inforeso    : inforeso,
                                    infosociete : infosociete,
                                    infofichier : infofichier
                                })
                        })
                    })
                })
            } else {
                console.log("superviseur inexistant")
                res.redirect('/compte/root/gestion_superviseur')
            }
        })
        
    } else {
        res.redirect('/')
    }
    
})


router.get('/supp/:code', (req, res, next) => {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
      let codesuperviseur = req.params.code
      User.findByOneField('code_user', codesuperviseur, (futclient) => {
          if (futclient.length === 1) {
            let user = futclient[0]
            let superviseur = {
                codesuperviseur: codesuperviseur
            }
            let infoSocieteClient = {
                codesuperviseur: codesuperviseur
            }
            let infoFichierClient = {
                codesuperviseur: codesuperviseur
            }
            let infoResocialClient = {
                codesuperviseur: codesuperviseur
            }
            let parrain = {
                codeParrain: codesuperviseur
            }

            User.remove(user, (msg) => {
                console.log(msg);
            })
            Superviseur.remove(superviseur, (msg) => {
                console.log(msg)
            })
            Parrain.remove(parrain, (msg) => {
                console.log(msg)
            })
            InfoSocieteClient.remove(infoSocieteClient, (msg) => {
                console.log(msg);
            })
            InfoFichierClient.remove(infoFichierClient, (msg) => {
                console.log(msg)
            })
            InfoResocialClient.remove(infoResocialClient, (msg) => {
                console.log(msg)
            })

            res.redirect('/compte/root/gestion_superviseur')

          } else {
            res.redirect('/compte/root/gestion_superviseur')
          }
      })  
    } else {
        res.redirect('/')
    }
})

module.exports = router
