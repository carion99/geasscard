const express = require('express')
const router = express.Router()

const html_specialchars = require('html-specialchars')
const emailValidator = require('email-validator')

const MessageContact = require('../models/MessageContact')

/* GET home page. */
router.get('/', function (req, res, next) {
    let title = 'GeassCard: Accueil'

    console.log(req.originalUrl)

    res.render('index', {
        title: title
    })

})

router.post('/', (req, res, next) => {
    let title = 'GeassCard: Accueil'
    let data = req.body

    console.log(data)

    if (data.submit && data.submit == "ok") {

        let nomPrenom = html_specialchars.escape(data.nomprenom)
        let destinateur = data.email
        let sujet = html_specialchars.escape(data.sujet)
        let message = html_specialchars.escape(data.message)

        if (
            nomPrenom != undefined && destinateur != undefined && sujet != undefined && message != undefined
        ) {
            console.log(nomPrenom)
            console.log(destinateur)
            console.log(sujet)
            console.log(message)

            if (nomPrenom != "" && destinateur != "" && sujet != "" && message != "") {
                if (emailValidator.validate(destinateur)) {

                    let destinataire 

                    let messageContact = {
                        codeMessage: MessageContact.genCodeMessageContact(),
                        nomPrenom: nomPrenom,

                    }

                    let succes = "Votre message a bien été expédié."
                    res.render('index', {
                        title: title,
                        messageSucces: succes
                    })
                } else {
                    let erreur = "Votre email est invalide"
                    res.render('index', {
                        title: title,
                        messageErreur: erreur
                    })
                }
            } else {
                let erreur = "Tous les champs sont obligatoires."
                res.render('index', {
                    title: title,
                    messageErreur: erreur
                })
            }
        } else {
            res.redirect('/')
        }
    } else {
        res.redirect('/')
    }

})

module.exports = router