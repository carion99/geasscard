const express = require('express')
const router = express.Router()
const env = require('../config/config').env
const stripe = require('stripe')(env.stripe.cle_privee)

/* GET users listing. */
router.get('/', function(req, res, next) {
  let keyp  = env.stripe.cle_publique
  res.render('users', {keyp})
})

router.post('/charge', (req, res, next) => {
  let amount = 1003

  stripe.customers.create({
    email   : req.body.stripeEmail,
    source  : req.body.stripeToken
  })
  .then(customer => stripe.charges.create({
    amount,
    description : "Sample Charge",
    currency  : "usd",
    customer  : customer.id
  }))
  .then(charge => res.render("charge"))
})

module.exports = router
