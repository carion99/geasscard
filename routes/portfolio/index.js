const express = require('express')
const router = express.Router()

const html_specialchars = require('html-specialchars')
const emailValidator = require('email-validator')
const mailer = require('nodemailer')

const Client = require('../../models/Client')
const InfoResocialClient = require('../../models/InfoResocialClient')
const InfoFichierClient = require('../../models/InfoFichierClient')
const InfoSocieteClient = require('../../models/InfoSocieteClient')
const Constante = require('../../config/constante')
const {
    informationEmailMessagePortfolio
} = require('../../config/constante')
const MessageContact = require('../../models/MessageContact')


const infoEmail = informationEmailMessagePortfolio()


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('portfolio/index', {
        title: 'GeassCard: Page Personnelle'
    })
    console.log(req.originalUrl)
})

router.get('/moi', function (req, res, next) {
    res.render('portfolio/moi', {
        title: 'GeassCard: Page Personnelle => Parlons de moi'
    })
    console.log(req.originalUrl)
})

router.get('/contact', function (req, res, next) {
    res.render('portfolio/contact', {
        title: 'GeassCard: Page Personnelle => Contactez Moi'
    })
    console.log(req.originalUrl)
})


router.get('/modele/:type_template/:id_template', function (req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "client") {
        let ident = sess.nom
        let role = sess.status
        let page = "Gestion Données"

        let type_template = req.params.type_template
        let id_template = req.params.id_template

        let lien = type_template + "/" + id_template

        if (Constante.verifierExistenceLienTemplate(lien) == false) {
            res.redirect('/compte/client/gestion_data/template')
        } else {
            Client.findByOneField('code_client', sess.codeUser, (futclient) => {
                if (futclient.length == 1) {
                    let client = futclient[0]
                    InfoSocieteClient.findByOneField('code_client', client.codeClient, (futinfosociete) => {
                        let societeclient = futinfosociete[0]
                        InfoResocialClient.findByOneField('code_client', client.codeClient, (futinfosocial) => {
                            let socialclient = futinfosocial[0]
                            InfoFichierClient.findByOneField('code_client', client.codeClient, (futfinfofichier) => {
                                let fichierclient = futfinfofichier[0]
                                //let liencard = '/portfolio/' + code
                                let lienTemplate = lien
                                let typeTemplate = Constante.typeTemplate(type_template + " " + id_template)

                                res.render('portfolio/bcv/index', {
                                    title: 'GeassCard: Page Personnelle',
                                    actuclient: client,
                                    societeclient: societeclient,
                                    socialclient: socialclient,
                                    fichierclient: fichierclient,
                                    //liencard: liencard,
                                    lienTemplate: lienTemplate,
                                    typeTemplate: typeTemplate
                                })
                                //console.log(client)
                            })
                        })
                    })
                } else {
                    res.redirect('/')
                }
            })

        }

    } else {
        res.redirect('/')
    }
})

router.post('/contact', (req, res, next) => {
    let data = req.body

    console.log(data);

    let nomprenom = html_specialchars.escape(data.name)
    let sujet = html_specialchars.escape(data.subject)
    let email = html_specialchars.escape(data.email)
    let message = html_specialchars.escape(data.message)
    let hashcode = html_specialchars.escape(data.hashcode)

    if (nomprenom && sujet && email && message && hashcode) {
        console.log(data);

        if (emailValidator.validate(email)) {

            Client.findByOneField('hashcc', hashcode, (futclient) => {
                if (futclient.length == 1) {
                    let client = futclient[0]
                    let codeMessage =  MessageContact.genCodeMessageContact()
                    let messageContact = {
                        codeMessage: codeMessage,
                        nomPrenom: nomprenom,
                        sujet: sujet,
                        destinateur: email,
                        destinataire: client.email,
                        message: message
                    }
                    MessageContact.create(messageContact, (msg) => {
                        console.log(msg)
                    })
                    res.redirect('/portfolio/' + hashcode)
                } else {
                    res.redirect('/')
                }
            })

        } else {
            res.redirect('/portfolio/' + hashcode)
        }
    } else {
        res.redirect('/portfolio/' + hashcode)
    }
})


router.get('/:hash', function (req, res, next) {
    let code = req.params.hash
    Client.findByOneField('hashcc', code, (futclient) => {
        if (futclient.length == 1) {
            let client = futclient[0]
            InfoSocieteClient.findByOneField('code_client', client.codeClient, (futinfosociete) => {
                let societeclient = futinfosociete[0]
                InfoResocialClient.findByOneField('code_client', client.codeClient, (futinfosocial) => {
                    let socialclient = futinfosocial[0]
                    InfoFichierClient.findByOneField('code_client', client.codeClient, (futfnfofichier) => {
                        let fichierclient = futfnfofichier[0]
                        let liencard = '/portfolio/' + code
                        let lienTemplate = Constante.lienTemplate(client.lienTemplate)
                        let typeTemplate = Constante.typeTemplate(client.lienTemplate)
                        res.render('portfolio/bcv/index', {
                            title: 'GeassCard: Page Personnelle',
                            actuclient: client,
                            societeclient: societeclient,
                            socialclient: socialclient,
                            fichierclient: fichierclient,
                            liencard: liencard,
                            lienTemplate: lienTemplate,
                            typeTemplate: typeTemplate,
                            hashcode: code
                        })
                        console.log(client)
                    })
                })
            })
        } else {
            res.redirect('/')
        }
    })
})




module.exports = router