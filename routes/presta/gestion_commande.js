const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "root") {
        let ident = sess.nom
        let role = sess.status
        let page = "Bienvenue"
        res.render('presta/gestion_commande', 
            { 
                title: 'GeassCard: Compte Prestataire => Gestion Commande',
                active: 2,
                ident : ident,
                role : role,
                page : page
            })
    } else {
        res.redirect('/')
    }
    
})

module.exports = router
