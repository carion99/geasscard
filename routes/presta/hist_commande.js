const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "presta") {
        let ident = sess.nom
        let role = sess.status
        let page = "Bienvenue"
        res.render('presta/hist_commande', 
            { 
                title: 'GeassCard: Compte Prestataire => Historique Commande',
                active: 3,
                ident : ident,
                role : role,
                page : page
            })
    } else {
        res.redirect('/')
    }
    
})

module.exports = router
