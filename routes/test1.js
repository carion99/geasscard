const express = require('express')
const router = express.Router()
const QRCode = require('qrcode')
const fs = require('fs')
const pdf = require('pdf').pdf

const Hashub = require('../config/hashub')

const User = require('../models/User')
const Root = require('../models/Root')
const ModeleCarte = require('../models/ModeleCarte')
const TarifCarte = require('../models/TarifCarte')

const env = require('../config/config').env

/* GET home page. */
router.get('/', function (req, res, next) {

  /*
  let code = Root.genCodeRoot()
  let nom  = "medi"
  let prenom  = "yann michael"
  let email= "root@gmail.com"
  let pass = "12345678"
  let hashpass = Hashub.crypt(pass)
  let status  = "root"


  
  let root = {
    codeRoot  : code,
    nom       : nom,
    prenom    : prenom,
    email     : email
  }

  let user = {
    codeUser  : code,
    nom       : nom,
    prenom    : prenom,
    username  : email,
    password  : hashpass,
    status    : status
  }

  Root.create(root, (msg) => {
    console.log(msg);
  })

  User.create(user, (msg) => {
    console.log(msg)
  })
  */


  //let lien = "https://placehold.it/400"

  for (let i = 0; i < 4; i++) {
    var letter;
    var type;
    if (i == 0) {
      letter = "a";
      type = "carton"
    }
    if (i == 1) {
      letter = "a";
      type = "pvc"
    }
    if (i == 2) {
      letter = "b";
      type = "carton"
    }
    if (i == 3) {
      letter = "b";
      type = "pvc"
    }



    for (let j = 0; j < 6; j++) {
      var name = letter+(j+1)
      var lien = name+".jpeg"

      let code = ModeleCarte.genCodeModele()

      var nom = "Modèle "+(type+" "+name).toUpperCase()

      let modele = {
        codeModele: code,
        nom: nom,
        typeCarte: type,
        lienFichier: lien
      }

      ModeleCarte.create(modele, (msg) => {
        console.log(msg);

      })
      
    }

  }



  /*
  for (let index = 0; index <= 10; index++) {
    let reste = index % 2
    let carton = "carton"
    let pvc = "pvc"
    let code = ModeleCarte.genCodeModele()
    if (reste == 0) {
      let nom = "carte " + carton + " " + index
      let modele = {
        codeModele: code,
        nom: nom,
        typeCarte: carton,
        lienFichier: lien
      }

      ModeleCarte.create(modele, (msg) => {
        console.log(msg);

      })
    } else {
      let nom = "carte " + pvc + " " + index
      let modele = {
        codeModele: code,
        nom: nom,
        typeCarte: pvc,
        lienFichier: lien
      }

      ModeleCarte.create(modele, (msg) => {
        console.log(msg);

      })
    }


  }

  */


  /*

  for (let i = 0; i < 2; i++) {
    for (let j = 0; j < 4; j++) {
      let code = TarifCarte.genCodeTarif()
      if (i === 0) {
        let type = "carton"
        let tarifp  = env.val.tarifperso.carton
        let tarif = {
          codeTarif : code,
          typeCarte : type,
          nbCarte   : env.val.pack[j],
          tarifCarte: env.val.tarifcarte[i][j],
          tarifPerso: tarifp,
          tarifPack : env.val.pack[j]*env.val.tarifcarte[i][j]+tarifp
        }
        console.log(env.val.pack[j]*env.val.tarifcarte[i][j]+tarifp);
        
        TarifCarte.create(tarif, (msg) => {
          console.log(msg);
          
        })
      } else {
        let type = "pvc"
        let tarifp  = env.val.tarifperso.pvc
        let tarif = {
          codeTarif : code,
          typeCarte : type,
          nbCarte   : env.val.pack[j],
          tarifCarte: env.val.tarifcarte[i][j],
          tarifPerso: tarifp,
          tarifPack : env.val.pack[j]*env.val.tarifcarte[i][j]+tarifp
        }
        console.log(env.val.pack[j]*env.val.tarifcarte[i][j]+tarifp);
        
        TarifCarte.create(tarif, (msg) => {
          console.log(msg);
          
        })
      }
      

      
    }
  } 
  

*/

  res.render('test1', {
    title: 'Geasscard :  Test 1'
  })


})

module.exports = router