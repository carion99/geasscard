const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', function(req, res, next) {
    let sess = req.session
    if (sess.codeUser && sess.status === "sclient") {
        let ident = sess.nom
        let role = sess.status
        let page = "Bienvenue"
        res.render('superclient/index_sclient', 
            { 
                title: 'GeassCard: Compte Gestionnaire => Accueil',
                active: 1,
                ident : ident,
                role : role,
                page : page
            })
    } else {
        res.redirect('/')
    }
    
})

module.exports = router
