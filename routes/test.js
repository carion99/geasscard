const express = require('express')
const router = express.Router()
const QRCode = require('qrcode')
const path   = require('path')
const multer = require('multer')
const storage= multer.diskStorage({
  destination :  (req, file, cb) => {
    cb(null, './public/upload/photoprofil/')
  },
  filename    : (req, file, cb) => {
    cb(null, file.fieldname + '-' + 
    Date.now() + path.extname(file.originalname))
  }
})

const upload = multer({
  storage : storage,
  fileFilter  : (req, file, cb) => {
    checkFileType(file, cb)
  }
}).single('myImage')

function checkFileType(file, cb) {
  const filetypes = /jpeg|jpg|png|gif/

  console.log(path.extname(file.originalname.toLowerCase()));

  const extname   = filetypes.test(path.extname(file.originalname.toLowerCase()))

  const mimetype = filetypes.test(file.mimetype)

  if (mimetype && extname) {
    return cb(null, true)
  } else {
    cb('Attention : Images Seulement!!!')
  }
}




/* GET home page. */
router.get('/', function(req, res, next) {
    res.render(
      'test', {
        title : "Test Upload With Express-FileUpload"
    })
})

router.post('/', (req, res, next) => {
  upload(req, res, (err) => {
    if(err) {
      res.render('test', {
        title : "Test Upload With Express-FileUpload",
        msg : err
      })
    } else {
      console.log(req.file)
      res.send('uploaded with success')
    }
  })
})



module.exports = router
