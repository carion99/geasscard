const redis = require('redis')

const env = "dev"

const dev = {
    'host': 'localhost',
    'user': 'carion',
    'password': 'root',
    'database': 'geasscard',
    'socketPath': '/var/run/mysqld/mysqld.sock',
    'port': 3306
}


const prod1 = {
    'host': 'us-cdbr-iron-east-05.cleardb.net',
    'user': 'bb63dc365c6662',
    'password': '37ddee33',
    'database': 'heroku_14bc68a8e0e1122',
    'socketPath': '/var/lib/mysql/mysql.sock',
    'port': 3306
}
/*
mysql://jxqw3agdomphxsfp:xk7f9h44u065al2q@qf5dic2wzyjf1x5x.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/j6zprxibssryxczx */
const prod2 = {
    'host': 'qf5dic2wzyjf1x5x.cbetxkdyhwsb.us-east-1.rds.amazonaws.com',
    'user': 'jxqw3agdomphxsfp',
    'password': 'xk7f9h44u065al2q',
    'database': 'j6zprxibssryxczx',
    'socketPath': '/var/lib/mysql/mysql.sock',
    'port': 3306
}

/*
const client_prod = redis.createClient({
  port: 18149,
  host: 'ec2-3-232-159-117.compute-1.amazonaws.com',
  password: 'pdf95cabee24609475e7a93d8b183084dc81cc1c13d1b06eb86127d360ad08ad8'
});
*/
const secret_dev = 'yannmicke080521'
const secret_prod = 'esti290322'

class Env {

    static actuEnv(){
        return env
    }

    static databaseEnv() {
        switch (env) {
            case "dev":
                return dev
                break;
            case "prod":
                return prod2
                break;

            default:
                return dev
                break;
        }
    }

    static redisEnv() {
        switch (env) {
            case "dev":
                return redis.createClient()
                break;
            case "prod":
                return redis.createClient({
                    port: 18149,
                    host: 'ec2-3-232-159-117.compute-1.amazonaws.com',
                    password: 'pdf95cabee24609475e7a93d8b183084dc81cc1c13d1b06eb86127d360ad08ad8'
                });
                break;

            default:
                return redis.createClient()
                break;
        }
    }

    static redisSecret() {
        switch (env) {
            case "dev":
                return secret_dev
                break;
            case "prod":
                return secret_prod
                break;

            default:
                return secret_dev
                break;
        }
       
    }

}

module.exports = Env