const Sidebare = require('../config/sidebare')
const ElementSidebare = require('../config/element_sidebare')

class TabSidebase {

    constructor() {

    }

    static root() {
        return [
            ElementSidebare.nouveauElement("Bienvenue", "/compte/root", "dashboard", 0),
            ElementSidebare.nouveauElement("Gestion Commandes", "/compte/root/gestion_commande", "shopping_cart", 0),
            ElementSidebare.nouveauElement("Gestion Bon Reduction", "/compte/root/gestion_reduction", "redeem", 0),
            ElementSidebare.nouveauElement("Gestion Superviseur", "/compte/root/gestion_superviseur", "supervisor_account", 0),
            ElementSidebare.nouveauElement("Gestion Client", "/compte/root/gestion_client", "supervised_user_circle", 0),
            ElementSidebare.nouveauElement("Gestion Prestataire", "/compte/root/gestion_prestataire", "group_work", 0),
            ElementSidebare.nouveauElement("Gestion Parrainage", "/compte/root/gestion_parrainage", "people_alt", 0),
            ElementSidebare.nouveauElement("Configuration Système", "/compte/root/configuration_systeme", "settings", 0),
            ElementSidebare.nouveauElement("Historique Système", "/compte/root/historique_systeme", "history", 0),
        ]
        //return tabsidebase
    }

    static superclient() {
        return [
            ElementSidebare.nouveauElement("Tableau de bord", "/compte/sclient", "dashboard", 0),
            ElementSidebare.nouveauElement("Gestion Individus", "", "people", 0),
            ElementSidebare.nouveauElement("Tracking", "", "gps_fixed", 0),
            ElementSidebare.nouveauElement("Service Com.", "", "rss_feed", 0),
            ElementSidebare.nouveauElement("Service Mapping", "", "place", 0)
        ]
    }

    static eclient() {
        return [
            ElementSidebare.nouveauElement("Tableau de bord", "/root/index", "dashboard", 0),
            ElementSidebare.nouveauElement("Gestion Individus", "/root/gestion_individus", "people", 0),
            ElementSidebare.nouveauElement("Tracking", "/root/tracking", "gps_fixed", 0),
            ElementSidebare.nouveauElement("Service Com.", "/root/service_com", "rss_feed", 0),
            ElementSidebare.nouveauElement("Service Mapping", "/root/service_mapping", "place", 0)
        ]
    }

    static dclient() {
        return [
            ElementSidebare.nouveauElement("Tableau de bord", "/compte/client", "dashboard", 0),
            ElementSidebare.nouveauElement("Gestion Données", "/compte/client/gestion_data", "storage", 0),
            ElementSidebare.nouveauElement("Gestion Contacts", "/compte/client/gestion_contact", "contact_page", 0),
            ElementSidebare.nouveauElement("Gestion Filleuls", "/compte/client/gestion_filleul", "groups", 0),
            ElementSidebare.nouveauElement("Passer Commande", "/compte/client/passcommand", "shopping_cart", 0)
        ]
    }

}



module.exports = TabSidebase