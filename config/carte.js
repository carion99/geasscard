class Carte {
    /**
     * 
     * @param {valeur par defaut si la valeur principale existe} valeur
     * @param {valeur par defaut si la valeur principale n'existe pas} defaut 
     */
	static nbcarte(valeur) {
        switch (valeur) {
            case "add50":
                return 50;
                break;
            case "add100":
                return 100;
                break;
            case "add250":
                return 250;
                break;
            case "add500":
                return 500;
                break;
        
            default:
                return 0;
                break;
        }
    }

    static coefTarifCarte(typecarte, nbpack) {
        if (typecarte === "carton") {
            switch (nbpack) {
                case 50:
                    return {coefTypeCarte : 0, coefNbPack : 0};
                    break;
                case 100:
                    return {coefTypeCarte : 0, coefNbPack : 1};
                    break;
                case 250:
                    return {coefTypeCarte : 0, coefNbPack : 2};
                    break;
                case 500:
                    return {coefTypeCarte : 0, coefNbPack : 3};
                    break;
            
                default:
                    break;
            }
        } 
        if (typecarte === "pvc") {
            switch (nbpack) {
                case 50:
                    return {coefTypeCarte : 1, coefNbPack : 0};
                    break;
                case 100:
                    return {coefTypeCarte : 1, coefNbPack : 1};
                    break;
                case 250:
                    return {coefTypeCarte : 1, coefNbPack : 2};
                    break;
                case 500:
                    return {coefTypeCarte : 1, coefNbPack : 3};
                    break;
            
                default:
                    break;
            }
        }
    }
}

module.exports = Carte

