class Initial {
    /**
     * 
     * @param {valeur par defaut si la valeur principale existe} valeur
     * @param {valeur par defaut si la valeur principale n'existe pas} defaut 
     */
	static create(valeur, defaut) {
        if(valeur) {
            return valeur
        } else {
            return defaut
        }
    }
}

module.exports = Initial

