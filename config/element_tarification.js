class ElementTarification {
    
    static  nouveauElement(titreOffre, montant, duree, pourcentageReduction, nbMoisOffert, routePaiement){

        var nouveau = {
            titreOffre: titreOffre,
            montant: montant,
            duree: duree,
            pourcentageReduction: pourcentageReduction,
            nbMoisOffert: nbMoisOffert,
            routePaiement: routePaiement
        }

        return nouveau

    }
}

module.exports = ElementTarification