const vCardsJS = require('vcards-js');

class Gvcard {
    /**
     *
     */
    static createTest() {
        

        //create a new vCard
        var vCard = vCardsJS();

        //set properties
        vCard.firstName = 'Eric';
        vCard.middleName = 'J';
        vCard.lastName = 'Nesser';
        vCard.organization = 'ACME Corporation';
        vCard.photo.attachFromUrl('https://avatars2.githubusercontent.com/u/5659221?v=3&s=460', 'JPEG');
        vCard.workPhone = '312-555-1212';
        vCard.birthday = new Date(1985, 0, 1);
        vCard.title = 'Software Developer';
        vCard.url = 'https://github.com/enesser';
        vCard.note = 'Notes on Eric';

        //save to file
        //vCard.saveToFile('../public/gvcard/eric-nesser.vcf');
        vCard.saveToFile('/home/carion/Documents/eric-nesser.vcf');

        //get as formatted string
        console.log(vCard.getFormattedString());
    }

    static create(infoPerso, infoResocial, infoSociete, dossierStockage) {
        

        //create a new vCard
        var vCard = vCardsJS();

        console.log(infoPerso.photo)

        //vCard.photo.embedFromFile(infoPerso.photo)
        vCard.source = infoPerso.lienCard

        //set properties
        vCard.firstName = infoPerso.prenom;
        vCard.lastName = infoPerso.nom;
        vCard.birthday = new Date(infoPerso.dateNaissance);
        vCard.url = infoPerso.siteInternet;

        vCard.cellPhone = [
            infoPerso.phone1
        ]

        if(infoPerso.sexe == "masculin") {
            vCard.namePrefix = "Mr"
        }
        if(infoPerso.sexe == "feminin") {
            vCard.namePrefix = "Mme"
        }

        vCard.socialUrls['facebook'] = infoResocial.lienFacebook
        vCard.socialUrls['linkedIn'] = infoResocial.lienLinkedin
        vCard.socialUrls['twitter'] = infoResocial.lienTwitter

        vCard.organization = infoSociete.nom;
        vCard.title = infoSociete.poste;
        vCard.workPhone = infoSociete.phone1.toString();
        vCard.workUrl = infoSociete.siteInternet
        vCard.workEmail = infoSociete.email
        
        
        
        
        
        vCard.note = infoPerso.autoDescription;

        var nomfichier = infoPerso.nom+".vcf"

        var cheminFinal = dossierStockage+nomfichier 
        

        //save to file
        vCard.saveToFile(cheminFinal);

        //get as formatted string
        console.log(vCard.getFormattedString());
    }
}

module.exports = Gvcard