const typroto = 'http'
const ideip = '127.0'
const host1 = 1
const host2 = 1

const val = {
    tva  : 0.18,
    pack : [50, 100, 250, 500],
    tarifcarte  : [
        [65, 50, 40, 35],[75, 70, 60, 55]
    ],
    tarifperso  : {
        carton  : 5000,
        pvc     : 6000
    }

}

let dev = {
    nom : 'dev',
    domaine : typroto+'://'+ideip+'.'+host1+'.'+host2+':3000',
    //domaine : typroto+'://'+'10.42.0.1:3000',
    heroku  : {
        host : 'localhost',
        port : 6379
    },
    bdd : {
        host : 'localhost',
        user : 'root',
        pass : 'root',
        data : ''
    },
    val : val,
    stripe : {
        cle_publique : 'pk_test_BNnwVnKcUKRT1fGrHNsFCJHj00ucNXAyfO',
        cle_privee     : 'sk_test_fAz8tqLFimwsQEAC74Kr0YUy00sg4IobCF'
    }
}
let prod = {
    nom : 'prod',
    domaine : 'https://geasscard.herokuapp.com',
    bdd : {
        host : 'us-cdbr-iron-east-05.cleardb.net',
        user : 'bb63dc365c6662',
        pass : '37ddee33',
        data : ''
    }, 
    val : val
}
let config = {
    env : dev
}

module.exports = config