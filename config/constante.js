const ElementTemplate = require('./element_template')
const ElementTarification = require('./element_tarification')

const PORT_SYSTEM = 3000
const HOSTMAIL = "node9-fr.n0c.com"


const tarifs = [
    ElementTarification.nouveauElement("basique", "")
]

const infoUrlDev = {
    //url: "http://10.42.0.1:"+PORT_SYSTEM
    url: "http://127.0.1.1:"+PORT_SYSTEM
}

const infoUrlProd = {
    url: "https://mygeasscard.com"
}




const infoEmailDev = {
    username: "yannmicke@gmail.com",
    password: ""
}

const infoEmailProd_ServiceSupport = {
    username: "servicesupport@mygeasscard.com",
    password: "75Zg5kRs6kat4Xf"
}
const infoEmailProd_ServiceHello = {
    username: "hello@mygeasscard.com",
    password: "O6N@bGlf7j36S*"
}
const infoEmailProd_ServiceAdmin = {
    username: "admin@mygeasscard.com",
    password: "MYP3,9oyPt42.w"
}
const infoEmailProd_ServiceNewsLetter = {
    username: "newsletter@mygeasscard.com",
    password: "9dcPj60bC!J(1B"
}
const infoEmailProd_ServiceMessagePortfolio = {
    username: "messageportfolio@mygeasscard.com",
    password: "EfE1p0SP8l=u2%"
}

const typeTemplateDark = "dark";
const typeTemplateLight = "light";



const listeTemplate = [
    ElementTemplate.nouveauElement("dark 1", "dark/1", "dark"),
    ElementTemplate.nouveauElement("dark 2", "dark/2", "dark"),
    ElementTemplate.nouveauElement("dark 3", "dark/3", "dark"),
    ElementTemplate.nouveauElement("dark 4", "dark/4", "dark"),
    ElementTemplate.nouveauElement("dark 5", "dark/5", "dark"),
    ElementTemplate.nouveauElement("dark 6", "dark/6", "dark"),
    ElementTemplate.nouveauElement("light 1", "light/1", "light"),
    ElementTemplate.nouveauElement("light 2", "light/2", "light"),
    ElementTemplate.nouveauElement("light 3", "light/3", "light"),
    ElementTemplate.nouveauElement("light 4", "light/4", "light"),
    ElementTemplate.nouveauElement("light 5", "light/5", "light"),
    ElementTemplate.nouveauElement("light 6", "light/6", "light"),
]

const infoEmailSupport = infoEmailProd_ServiceSupport
const infoEmailHello = infoEmailProd_ServiceHello
const infoEmailAdmin = infoEmailProd_ServiceAdmin
const infoEmailNewsLetter = infoEmailProd_ServiceNewsLetter
const infoEmailMessagePortfolio = infoEmailProd_ServiceMessagePortfolio

const infoUrl = infoUrlDev

class Constante {

    constructor(){

    }

    static getPort() {
        return PORT_SYSTEM
    }

    static getHostmail() {
        return HOSTMAIL
    }

    static informationUrl() {
        return infoUrl
    }

    static informationEmailSupport(){
        return infoEmailSupport
    }
    static informationEmailHello(){
        return infoEmailHello
    }
    static informationEmailAdmin(){
        return infoEmailAdmin
    }
    static informationEmailNewsLetter(){
        return infoEmailNewsLetter
    }
    static informationEmailMessagePortfolio(){
        return infoEmailMessagePortfolio
    }

    static listeTemplate() {
        return listeTemplate
    }

    static lienTemplate(texte){
        let lien = texte.toLowerCase()
        lien = lien.replace(/[\s]{1,}/g,"/");

        return lien
    }

    static typeTemplate(texte){
        if(texte.toLowerCase().indexOf(typeTemplateDark) != -1){
            return typeTemplateDark
        }
        if(texte.toLowerCase().indexOf(typeTemplateLight) != -1){
            return typeTemplateLight
        }
    }

    static camelize(str) {
        return str.replace(/\W+(.)/g, function(match, chr)
         {
              return chr.toUpperCase();
          });
      }

    static verifierExistenceLienTemplate(strlien){
        let confirmation = false
        listeTemplate.forEach(template => {
            if(strlien == template.route){
                confirmation = true
                //break;
            }
            return confirmation
        });
    }

    

    

    
}
module.exports = Constante