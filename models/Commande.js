let connection = require('../config/connection')
const moment = require('moment')

class Commande {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_commande'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'commande'
	}


	get id () {
		return this.row.id
	}
	get codeCommande () {
		return this.row.code_commande
	}

	get codeClient () {
		return this.row.code_client
	}

	get codePanier () {
		return this.row.code_panier
	}

	get codePresta () {
		return this.row.code_presta
	}
    
    get coutTotal () {
		return this.row.cout_total
	}
	
	get moyenPaiement () {
		return this.row.moyen_paiement
	}
    
    get statut () {
		return this.row.statut
	}
	
	get confirmationClient () {
		return this.row.confirmation_client
	}


	get dateRegister () {
		return moment(this.row.date_register)
	}

	static genCodeCommande(codeUser) {
		let min = 1000000
		let max = 9999999
		let text= "commande-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = codeUser+"-"+text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= Commande.ID()
		let TABLE 	= Commande.TABLE()
		let codeCommande 	= content.codeCommande
		let codeClient  = content.codeClient
		let codePresta 	= "Aucun Prestataire Choisi"
		let codePanier = content.codePanier
		let coutTotal  	= content.coutTotal
		let moyenPaiement = "Aucun Pour L'instant"
		let statut 	    = "1"
		let confirmationClient = 0
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_client = ?, code_presta = ?,  code_panier = ?,  '
		+ 'cout_total = ?, moyen_paiement = ?,  statut = ?, confirmation_client = ?, date_register = ?'
		connection.query(sql, [codeCommande, codeClient, codePresta, codePanier, coutTotal, moyenPaiement, statut,  confirmationClient, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "Commande bien ajoutée"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Commande.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Commande(row)))
			})
	}

	/**
	 * trouver une ligne de la table
	 * @param  {string}   field1 [premier Champ à viser]
	 * @param  {string or any}   value1 [valeur du premier champ à viser]
	 * @param  {string}   field2 [second Champ à viser]
	 * @param  {string or any}   value2 [valeur du second champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByTwoField(field1, value1, field2, value2, cb) {
		let TABLE = Commande.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field1 +' = ? AND '+ field2 +' = ?'
		connection.query(sql, [value1, value2], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Commande(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Commande.TABLE()
		let ID = Commande.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * trouver une ligne de la table
	 * @param  {string}   field1 [premier Champ à viser]
	 * @param  {string or any}   value1 [valeur du premier champ à viser]
	 * @param  {string}   field2 [second Champ à viser]
	 * @param  {string or any}   value2 [valeur du second champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByThreeField(field1, value1, field2, value2, field3, value3,cb) {
		let TABLE = Commande.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field1 +' = ? AND '+field2 +' = ? AND '+ field3 +' = ?'
		connection.query(sql, [value1, value2, value3], 
			(err, rows) => {
				console.log(sql);

				if (err) throw err
				cb(rows.map((row) => new Commande(row)))
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Commande.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Commande(row)))
		})
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeCommande = row.codeCommande
		let TABLE = Commande.TABLE()
		let ID = Commande.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeCommande], (err, result) => {
			if (err) throw err
			let msg = "Commande supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Commande