let connection = require('../config/connection')

class SuperClient {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_superclient'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'superclient'
	}


	get id () {
		return this.row.id
	}
	get codeSuperClient () {
		return this.row.code_superclient
	}

	get nom () {
		return this.row.nom
	}

	get prenom () {
		return this.row.prenom
	}
	
	get sexe () {
		return this.row.sexe
	}
    
    get email () {
		return this.row.email
    }
    
    get phone () {
		return this.row.phone
	}
    	
	get photo () {
		return this.row.photo
	}

	get dateRegister () {
		return this.row.date_register
	}

	static genCodeSuperClient() {
		let min = 1000
		let max = 9999
		let text= "superclient-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= SuperClient.ID()
		let TABLE 	= SuperClient.TABLE()
		let codeSuperClient 	= content.codeSuperClient
		let nom 		= content.nom
		let prenom  	= content.prenom
		let sexe = content.sexe
        let email  	    = content.email
        let phone  = content.phone
        let photo  = ""
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, prenom = ?, sexe =?, email = ?, email = ?,'
		+'phone = ?, photo = ?, date_register = ?'
        connection.query(sql, [codeSuperClient, nom, prenom, sexe, email, phone, photo, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "SuperClient bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = SuperClient.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new SuperClient(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = SuperClient.TABLE()
		let ID = SuperClient.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = SuperClient.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new SuperClient(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeSuperClient = row.codeSuperClient
		let TABLE = SuperClient.TABLE()
		let ID = SuperClient.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeSuperClient], (err, result) => {
			if (err) throw err
			let msg = "SuperClient supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = SuperClient