let connection = require('../config/connection')

class BonDeReduction {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_bdr'
	}
	/**
	 * Constante contenant le codeReduction de la table actuelle
	 */
	static TABLE() {
		return 'bon_de_reduction'
	}


	get id () {
		return this.row.id
	}
	get codeBdr () {
		return this.row.code_bdr
    }
    
    get codeReduction () {
		return this.row.code_reduction
	}

	get sommeReduction () {
		return this.row.somme_reduction
	}
	
	get activeReduction () {
		return this.row.active_reduction
	}
    
	get dateRegister () {
		return this.row.date_register
	}

	static genCodeBdr() {
		let min = 10000
		let max = 99999
		let text= "BonDeReduction-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static genCodeReduction() {
		let min = 10000000
		let max = 99999999
		let text= "BonDeReduction-GeassCard-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= BonDeReduction.ID()
		let TABLE 	= BonDeReduction.TABLE()
		let codeBdr 	= content.codeBdr
		let codeReduction 		= content.codeReduction
		let sommeReduction  	= content.sommeReduction
		let activeReduction = content.activeReduction
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_reduction = ?, somme_reduction = ?, ' 
		+'active_reduction = ?, date_register = ?'
		connection.query(sql, [codeBdr, codeReduction, sommeReduction, activeReduction, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "Bon De Reduction bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = BonDeReduction.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new BonDeReduction(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = BonDeReduction.TABLE()
		let ID = BonDeReduction.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = BonDeReduction.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new BonDeReduction(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeBdr = row.codeBdr
		let TABLE = BonDeReduction.TABLE()
		let ID = BonDeReduction.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeBdr], (err, result) => {
			if (err) throw err
			let msg = "BonDeReduction supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = BonDeReduction