-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  jeu. 02 jan. 2020 à 15:44
-- Version du serveur :  8.0.18-0ubuntu0.19.10.1
-- Version de PHP :  7.3.13-1+ubuntu19.10.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `geasscard`
--

-- --------------------------------------------------------

--
-- Structure de la table `attente_validation`
--

CREATE TABLE `attente_validation` (
  `id` int(11) NOT NULL,
  `code_av` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hachar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(2555) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `attente_validation`
--

INSERT INTO `attente_validation` (`id`, `code_av`, `code_user`, `hachar`, `status`, `date_register`) VALUES
(5, 'av-2612', 'client-1912', 'fe65365d4f68a4d69d6261272d816a5da7051722a3488f2686d21d1b6913efee2d362afba0096dd5417c7f5c8bd32005ed835d19325e952c99130d14900dbcec', 'noclient', '2019-12-28 13:45:28'),
(7, 'av-3690', 'client-4115', '4a6e6750db90af2404b504db6b575644c7f18ceb6653a8a212bfb3326e370758b12d1fc634d650d2028d1d60277d10af1bb79487f5dfa77afe2c4addd5df1f82', 'noclient', '2019-12-28 13:49:57'),
(8, 'av-3111', 'client-2393', '6489d3783a00af4e9f4ebe8cceffc7e4126373aa43de293e47eb3f4e55ec1b89f13d8bb2b4d794a3a573fabdd97638994087c459872c44db59cd86a55bc7cb75', 'noclient', '2019-12-28 13:53:21');

-- --------------------------------------------------------

--
-- Structure de la table `bon_de_reduction`
--

CREATE TABLE `bon_de_reduction` (
  `id` int(11) NOT NULL,
  `code_bdr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_reduction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `somme_reduction` double NOT NULL,
  `active_reduction` int(11) NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `bon_de_reduction`
--

INSERT INTO `bon_de_reduction` (`id`, `code_bdr`, `code_reduction`, `somme_reduction`, `active_reduction`, `date_register`) VALUES
(1, 'BonDeReduction-22269', 'BonDeReduction-GeassCard-15064959', 5000, 0, '2020-01-02 14:39:27'),
(2, 'BonDeReduction-21484', 'JANVIER2019', 10000, 0, '2020-01-02 15:14:56');

-- --------------------------------------------------------

--
-- Structure de la table `citation`
--

CREATE TABLE `citation` (
  `id` int(11) NOT NULL,
  `code_citation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_naissance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` int(11) NOT NULL,
  `phone2` int(11) NOT NULL,
  `phone3` int(11) NOT NULL,
  `site_internet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auto_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_card` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashcc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `code_client`, `nom`, `prenom`, `date_naissance`, `email`, `adresse`, `phone1`, `phone2`, `phone3`, `site_internet`, `auto_description`, `lien_card`, `hashcc`, `date_register`) VALUES
(26, 'client-1724', '', '', '', '', '', 0, 0, 0, '', '', 'http://127.0.1.1:3000/portfolio/e205efe4dde4793266c2bca093752500d4657367a5dad0873d4043931f1c177f183896a517bd477be816e1c02334bcc9182467a4f747eaf2e06ab3be3ec8cb79', 'e205efe4dde4793266c2bca093752500d4657367a5dad0873d4043931f1c177f183896a517bd477be816e1c02334bcc9182467a4f747eaf2e06ab3be3ec8cb79', '2019-12-26 14:54:51'),
(27, 'client-8748', '', '', '', '', '', 0, 0, 0, '', '', 'http://127.0.1.1:3000/portfolio/b76a3bae7e2a267fd81dbcfe06a4ade02f1eb0cf854a05623b8f8d8654a8d77dfb4aeec5598190a11f4d94b5272d88237e7bb0f170bb083d630a5168d7d88586', 'b76a3bae7e2a267fd81dbcfe06a4ade02f1eb0cf854a05623b8f8d8654a8d77dfb4aeec5598190a11f4d94b5272d88237e7bb0f170bb083d630a5168d7d88586', '2019-12-26 16:15:19'),
(28, 'client-8785', '', '', '', '', '', 0, 0, 0, '', '', 'http://127.0.1.1:3000/portfolio/1e6b14870505df97a77ed4b2019149d3255ddafd12b4ea87922839e6b2707eb37b826ab71dde44a5faddfb0fd4b0577b8284fbd457142ee3570eac6bff77a63d', '1e6b14870505df97a77ed4b2019149d3255ddafd12b4ea87922839e6b2707eb37b826ab71dde44a5faddfb0fd4b0577b8284fbd457142ee3570eac6bff77a63d', '2019-12-28 13:54:13');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `code_commande` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_panier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cout_total` double NOT NULL,
  `statut` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL,
  `code_presta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `moyen_paiement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `code_commande`, `code_client`, `code_panier`, `cout_total`, `statut`, `date_register`, `code_presta`, `moyen_paiement`) VALUES
(36, 'client-8785-commande-7181152', 'client-8785', 'panier-5034438', 21000, 'non réglée', '2019-12-28 14:04:34', 'Aucun Prestataire Choisi', 'À La Livraison');

-- --------------------------------------------------------

--
-- Structure de la table `contact_client`
--

CREATE TABLE `contact_client` (
  `id` int(11) NOT NULL,
  `code_contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contact_client`
--

INSERT INTO `contact_client` (`id`, `code_contact`, `code_client`, `nom`, `prenom`, `email`, `phone`, `commentaire`, `date_register`) VALUES
(1, 'ContactClient-6211219', 'client-5195', 'az', 'ze', 'yannmicke@gmail.com', 0, '', '2019-10-17 22:41:46'),
(2, 'ContactClient-7691073', 'client-5195', 'YANN', 'MEDI', '', 67623971, '', '2019-10-17 22:42:10'),
(3, 'ContactClient-9680672', 'client-1921', 'Suini', 'Koffi Adammo Olive', 'suiniolive58@gmail.com', 58048155, '', '2019-11-07 11:51:49'),
(4, 'ContactClient-6831611', 'client-4787', 'Kouassi', 'Ckeick', 'ti.dev99@gmail.com', 47372856, '', '2019-11-10 01:41:06'),
(5, 'ContactClient-7394204', 'client-6042', 'BAKAYOKO', 'OLIVIA', '', 51148600, '', '2019-12-16 17:09:05'),
(6, 'ContactClient-8843568', 'client-8785', 'Beda ', 'Christ', '', 40654980, '', '2019-12-28 14:00:44');

-- --------------------------------------------------------

--
-- Structure de la table `cursus`
--

CREATE TABLE `cursus` (
  `id` int(11) NOT NULL,
  `code_cursus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_debut` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_fin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diplome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `divers`
--

CREATE TABLE `divers` (
  `id` int(11) NOT NULL,
  `code_divers` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_etudiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hobbie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lv1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lv2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lv3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `association` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int(11) NOT NULL,
  `code_etudiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

CREATE TABLE `experience` (
  `id` int(11) NOT NULL,
  `code_experience` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_etudiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_debut` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_fin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `filleul`
--

CREATE TABLE `filleul` (
  `id` int(11) NOT NULL,
  `code_filleul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_parrain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `filleul`
--

INSERT INTO `filleul` (`id`, `code_filleul`, `code_parrain`, `email`) VALUES
(4, 'client-1724-filleul-8301608', 'client-1724', 'yannmicke@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `info_fichier_client`
--

CREATE TABLE `info_fichier_client` (
  `id` int(11) NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_photo_profil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_cv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chemin_qr` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `info_fichier_client`
--

INSERT INTO `info_fichier_client` (`id`, `code_client`, `chemin_logo`, `chemin_photo_profil`, `chemin_cv`, `chemin_qr`) VALUES
(18, 'client-1724', '', '', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAAE0CAYAAACigc+fAAAAAklEQVR4AewaftIAABcZSURBVO3BQW7s2pLAQFLw/rfM9jBHBxBU5fu+OiPsF2ut9QIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RIXa631EhdrrfUSF2ut9RI/PKTylyomlaliUjmpeELlpGJSOamYVKaKE5WpYlK5o+JE5Y6KSWWqmFTuqHhCZao4UZkqTlT+yyomlb9U8cTFWmu9xMVaa73ExVprvcQPH1bxSSpPVHxTxaQyqXySyhMVd6icVEwqJypTxaRyR8WkclJxUjGpTBUnKicVT6hMFX+p4pNUPulirbVe4mKttV7iYq21XuKHL1O5o+JfUjmpmFROKk5UTiomlZOKE5Wp4qRiUplUvqnijopJZVKZKiaVqWJSmSqeULmj4g6VqeKTVO6o+KaLtdZ6iYu11nqJi7XWeokf/sdV3KEyVUwVk8qk8oTKVDGpnFTcofKEylQxqZxU3FExqZxU3FFxh8pUMalMFZPKScWJyqQyVUwqU8WJylTxv+xirbVe4mKttV7iYq21XuKH/+dUpoqpYlI5qfikihOVb6p4QuUvqdyhclJxh8pUMal8kspUMalMFW92sdZaL3Gx1lovcbHWWi/xw5dVfJPKVDGpTBV3qJxUPKEyVZyoTBWTyknFHSp3VNyhMlXcoTJVTCp3VEwqd1RMKlPFicpUMal8UsUTFf8lF2ut9RIXa631EhdrrfUSP3yYyv8SlanipGJSmSomlaliUjlRmSomlaliUjlRmSpOKiaVE5Wp4g6VqeJfqphUnlCZKiaVqWJSeUJlqjhR+S+7WGutl7hYa62XuFhrrZf44aGK/yUqU8WkMlWcVEwqU8Wk8k0qU8VJxUnFExXfVDGpTBWTylQxqUwVk8pUcVIxqZyonKhMFZPKVDGpTBUnFf9LLtZa6yUu1lrrJS7WWusl7BdfpHJHxaQyVUwqU8Wk8kTFpHJHxRMqU8V/ico3VTyhckfFHSonFXeoTBWTyknFpDJVTCpPVJyo3FHxxMVaa73ExVprvcTFWmu9xA9/rGJSOan4SxVPVJyoTBVPqEwVJyonFZPKJ1VMKneoPFHxX1YxqUwVJypTxaQyVUwqU8WkMqlMFScVk8onXay11ktcrLXWS1ystdZL2C8eUDmpmFSmihOVk4o7VO6oeELlpOIOlZOKSeWkYlKZKiaVqeKTVP5SxaRyUnGHylRxojJVnKhMFZPKVDGpTBWTyknFpHJHxSddrLXWS1ystdZLXKy11kvYL75IZaqYVE4qJpU7KiaVOyomlaniDpWTijtUpooTlaniDpWpYlKZKiaVqWJSmSomlaliUpkqTlSmihOVqeIOlZOKSeWkYlKZKiaVqeIJlaliUpkqvulirbVe4mKttV7iYq21XuKHh1SmihOVk4pJZaqYVO6o+CSVk4qTikllqjipmFROKiaVT6q4Q2WquENlqjhReaJiUjmpmCpOVE4qJpWpYlI5UXmiYlK5Q2WqeOJirbVe4mKttV7iYq21XuKHhyomlaliUrmjYlI5qZhUvqliUplUpoo7VKaKk4pJ5aTiROUOlZOKSeWJihOVk4o7VE4qTlSmiqliUplUpoqTir9UMan8pYu11nqJi7XWeomLtdZ6iR/+WMWJyh0Vd1RMKk+oTBV3qEwVk8qkMlWcVJyoTBUnFZ9UMalMFVPFHRWTyonKScWJyhMqU8WJyn9JxYnKVPFJF2ut9RIXa631EhdrrfUSPzykMlV8UsWkMqlMFVPFpHJHxR0qU8VJxaQyVfwllanim1ROVE4qJpU7VE4qnqiYVO5QeaJiUrmj4kTljopJZap44mKttV7iYq21XuJirbVe4ocvU3lCZaqYVE5UpooTlTtUTlSmiknlk1SmiidUpoqTihOVqeJE5UTlpGJSOak4UblDZaqYVKaKO1SmiknlCZWTiicqPulirbVe4mKttV7iYq21XsJ+8UEqU8WkMlV8kspJxaQyVZyoTBWTyknFEyp3VEwqU8WJyknFicodFZPKVHGickfFpDJVTCp3VEwqU8WkckfFpDJVTCpTxaQyVUwqJxWTyh0VT1ystdZLXKy11ktcrLXWS9gvvkhlqrhD5Y6KSeWOihOVOyomlTsqJpWpYlJ5omJSOam4Q+Wk4kRlqphUpopJ5aTiDpWpYlKZKiaVqeIOlaliUrmjYlKZKk5UTiq+6WKttV7iYq21XuJirbVewn7xgMo3VZyo3FExqUwVk8pJxRMqJxV3qNxR8UkqU8UdKt9UMamcVEwqU8WkMlU8ofJExTepnFT8pYu11nqJi7XWeomLtdZ6CfvFAyonFZPK/ycVd6icVDyhMlVMKlPFpHJScaIyVUwqJxV3qJxUPKFyUjGp3FExqZxUnKhMFXeoTBV3qEwVn3Sx1lovcbHWWi9xsdZaL/HDh1WcVPwllaliUpkqTlSmihOVOyruUJkqJpUnVKaKE5VPqphUpopJ5QmVqeKOipOKO1TuUJkqpopvUrlDZap44mKttV7iYq21XuJirbVewn7xQSpTxaTyRMUdKp9UcaLyTRV3qEwVd6hMFd+kMlVMKlPFpDJVfJLKJ1VMKlPFicpUMamcVEwq31TxTRdrrfUSF2ut9RIXa631EvaLB1Smiv9lKlPFpDJV3KFyUjGpnFTcoTJVTConFZ+k8kkVk8pU8UkqU8WJyhMVk8oTFU+onFScqEwVT1ystdZLXKy11ktcrLXWS/zwUMWJylQxqdxRMancUXFScVIxqUwVd1RMKlPFpHKiMlWcqEwVJypTxaTyRMUTKneo3FExVUwqU8UTFScVk8pUMancoXJScaIyVUwVn3Sx1lovcbHWWi9xsdZaL2G/eEDlpOKTVE4qPknlpGJSOak4UTmpmFSmiknlpGJSuaPiDpWpYlI5qZhUpooTlTsqJpWTik9SmSpOVKaKSeWk4gmVJyqeuFhrrZe4WGutl7hYa62XsF98kMoTFZPKVHGiMlVMKicVk8pJxYnKScUdKicVk8oTFZPKScWkMlVMKlPFHSonFU+oTBUnKlPFpDJVTCpTxRMqU8WkckfFHSpTxTddrLXWS1ystdZLXKy11kv88GEVJyp3VPxLFZ9UMak8UXFHxaRyojJVTConFScVJypPqDxRMamcVDxRMalMFScqU8WkclJxonJHxaRyUvHExVprvcTFWmu9xMVaa73EDw+pTBWTyknFicpU8UTFpDKpTBWTyknFN1VMKicVT1ScVEwqd1ScVEwqd1Q8oTJVnKhMFXeoTBWTyh0qU8WkMqlMFScVJyonFZ90sdZaL3Gx1lovcbHWWi/xw0MVJxWTyh0VJyonKicVk8odFScVk8pJxSepnFTcoXJS8U0Vk8pUMamcVEwqU8WkclLxSSpTxaQyVZyoTBX/kspU8cTFWmu9xMVaa73ExVprvcQPH6YyVUwVk8o3VUwqk8oTKlPFHRXfVPGEyh0qU8WkMlWcqEwVU8WkMlVMKicVk8pUMalMKlPFHRUnKicqJxUnKlPFJ1V808Vaa73ExVprvcTFWmu9xA8PqUwVn1TxlypOKiaVE5WpYlKZKiaVv6RyR8Wk8oTKicpUMVV8k8pUcaIyVUwqU8WkMlU8oTJVTBWTyonKScWkclLxxMVaa73ExVprvcTFWmu9xA9fpvKEyhMVf0llqnii4kRlqphUpoo7KiaVSeVE5UTlpGJSmVTuqDhROak4UZkq7lA5Ubmj4kTlm1Smim+6WGutl7hYa62XuFhrrZf44Y9VTCpTxR0qU8Wk8oTKVDFVTCqTyh0qJxVTxUnFJ1VMKlPFHSp/SWWqmComlROVE5WpYqo4UZkqJpWp4omKO1TuUDmpeOJirbVe4mKttV7iYq21XuKHhyruULlDZao4Ubmj4g6VqeKJiknlROWOiidUpoo7VKaKO1SmihOVO1ROKiaVqWJSeULlROVE5aRiUjlRmSpOKiaVqeKbLtZa6yUu1lrrJS7WWuslfvgwlaniROWk4o6KJ1Q+qWJSmVTuqDhRmVSmijsqnqi4o2JSuaNiUrmjYlKZKiaVqWJSmVSmiqliUnmi4omKO1ROVE4qnrhYa62XuFhrrZe4WGutl/jhIZWp4gmVb1I5qZhUpoqTikllqjhRmSomlanikyomlaniROUJlROVqeKJiknlmyomlTsqnlA5UXmiYlKZKr7pYq21XuJirbVe4mKttV7CfvFBKicVf0nlkyomlScqJpVPqphUTiomlU+qmFSmikllqjhRmSomlaniRGWqmFQ+qWJSmSomlaniRGWqeELlkyqeuFhrrZe4WGutl7hYa62XsF88oPKXKj5JZaqYVE4qTlTuqJhUpoo7VKaKE5WpYlKZKiaVOypOVJ6ouENlqjhROak4Ubmj4kTlpGJSOamYVKaK/5KLtdZ6iYu11nqJi7XWeokfHqqYVKaKSeWOihOVqeKTKu5QmSomlROVv6QyVXxSxYnKVDFV3KFyh8pU8U0qU8WJyqTyhMpUMalMKlPFpDJVTCpTxTddrLXWS1ystdZLXKy11kv88McqJpUTlaniROWkYqqYVJ6omFSmiknlpOKJikllqvhLKlPFHSp3qJxUTConFVPFpHJSMalMFScVn6Ryh8pUMalMFZPKVPFJF2ut9RIXa631EhdrrfUS9osPUjmpeEJlqphUnqiYVKaKSeWkYlKZKk5UTiomlaliUrmjYlJ5ouJEZaq4Q+WbKiaVqWJSuaNiUpkqJpWTikllqphUpooTlaliUrmj4omLtdZ6iYu11nqJi7XWeokfPqxiUnlC5UTljooTlanijopJ5URlqjipeKJiUpkqJpWTijtUvqniDpU7VKaKSeWkYlI5qZhUpopJ5aTipGJSOam4o+KbLtZa6yUu1lrrJS7WWuslfnhIZap4QmWquENlqphUTiqeUPn/ROWJihOVk4pJZaqYVE4qJpWpYlKZKk5U7lCZKiaVO1ROKqaKSWVSmSr+pYu11nqJi7XWeomLtdZ6CfvFP6TySRWTyh0VJypTxaTySRUnKlPFHSpTxR0qU8Wk8kkVJypPVEwqJxWTylTxhMpJxSepnFRMKicVk8pU8UkXa631EhdrrfUSF2ut9RI/PKQyVUwqU8UdFZPKicpJxYnKHSpTxX+JyknFpHJS8U0Vk8qkMlU8UTGpTBV3VEwqd1TcoXJScaJyUjGpnFRMKlPFN12stdZLXKy11ktcrLXWS/zwUMWkMlWcVEwqk8pJxaTyTRWTyonKScUnqUwVJypTxYnKVHFSMamcqEwVd1R8kspJxaQyVUwqU8UdFX9JZao4UblDZap44mKttV7iYq21XuJirbVewn7xQSonFZ+k8kkVk8oTFZPKHRWTylRxojJVTConFZPKVDGp3FFxojJVnKicVEwqU8UdKlPFpDJVTConFZPKScWJylRxh8pUMancUfFJF2ut9RIXa631EhdrrfUS9osHVKaKSeWbKiaVk4pJZaqYVKaKE5U7Kk5UvqliUjmpmFROKiaVqWJSuaNiUpkqJpVPqphUpopJZaqYVE4qTlSmihOVqeKbVE4qnrhYa62XuFhrrZe4WGutl/jhyyo+SWVSmSo+qWJSOamYVL6pYlKZKk5UTipOKp5QOamYVO5QeaLijopJ5Y6KE5WpYqq4o+Jfqviki7XWeomLtdZ6iYu11nqJHx6qmFSmikllqphUPknlpGJSmSo+qWJSOamYVP6SyknFicoTKlPFpDJVPKEyqZxUnFTcoTJVTBV3qEwVJyp3VJyonKhMFU9crLXWS1ystdZLXKy11kv88GUqT1ScqDyh8pdUnqiYVO5QeaLijoo7VKaKSeWbKiaVqeKTVE5UnqiYVO6oOFGZKk4qvulirbVe4mKttV7iYq21XsJ+8Q+p3FFxh8oTFZPKScWJyh0Vk8pJxaQyVZyoTBWTyknFpDJV3KHySRWTylQxqdxRcaIyVZyoTBXfpDJVTCp3VEwqU8UnXay11ktcrLXWS1ystdZL/PCQylRxojJV3KEyVUwqT1ScVEwqJyonFScqd6hMFScqT1RMKlPFpHJS8UkVk8odFZPKVDGpTBVTxYnKVDGpTBUnKlPFScVJxYnKpPKXLtZa6yUu1lrrJS7WWuslfnioYlKZKqaKO1SeqJhUnlCZKiaVqeIOlZOKE5UTlTtUpopPqphUpopJZar4JJWpYqqYVKaKSeUJlW9SeaLipOJEZap44mKttV7iYq21XuJirbVewn7xh1SmijtU7qi4Q+WkYlI5qThRmSomlZOKE5Wp4kTliYpJZaq4Q2WqmFTuqJhU7qg4UZkqTlSmihOVOyomlanik1TuqPiki7XWeomLtdZ6iYu11noJ+8UXqUwVk8oTFZPKVDGpTBWTyjdVnKhMFScqU8WJyknFJ6mcVEwqU8WkMlXcoTJVTCpTxaRyUnGiMlVMKicVn6RyUjGpTBV3qJxUPHGx1lovcbHWWi9xsdZaL2G/+CCVqeIOlaniRGWqOFGZKv6SylRxojJVTCpPVNyhclJxovJJFScq/1LFJ6lMFZPKHRVPqEwVk8pU8U0Xa631EhdrrfUSF2ut9RL2iwdUpopJZaqYVJ6oOFGZKiaVk4oTlZOKSeWk4kRlqjhRmSpOVKaKO1ROKiaVqeKTVJ6omFROKiaVqeIOlanik1ROKiaVOypOVKaKJy7WWuslLtZa6yUu1lrrJewXD6hMFX9JZaq4Q2WqmFSmihOVk4o7VKaKSWWqmFT+pYonVKaKSWWqOFE5qZhUTiruUPmmihOVqWJSmSpOVKaKSWWq+KaLtdZ6iYu11nqJi7XWegn7xQMqU8WJyknFpPJExaRyR8UdKlPFpHJHxR0qd1Q8ofJNFZPKVPGEylRxh8odFXeoTBWfpPJNFX/pYq21XuJirbVe4mKttV7ih4cq7qi4o+JE5ZtUpoqTikllqphUTlQ+qWJSmSomlanipOIOlROVv6QyVdxRcYfKVPGEyhMVd6icqEwVk8pU8cTFWmu9xMVaa73ExVprvcQPD6n8pYqp4kTljopJZVK5o2JSOan4l1SmiidUpoqTiknlRGWqmFROKk5UpoqpYlI5qZgq7lCZKqaKO1ROVKaKk4oTlaniky7WWuslLtZa6yUu1lrrJX74sIpPUnmi4kRlUpkqTlTuqJhUJpVPqnhCZaq4o+KJikllqjipmFQmlZOKSWWqmCpOVE4q7lC5o+KOiidUpopvulhrrZe4WGutl7hYa62X+OHLVO6ouENlqvgklZOKSeVEZaqYVKaKSeWk4o6KT1J5QuUOlTsqJpWpYlKZKiaVk4qp4kTlpOJEZaq4Q+UJlaniL12stdZLXKy11ktcrLXWS/zw/4zKVDGpTBUnKlPFExXfpDJV/JdU3KFyUnFScYfKVPGEylQxqZyoTBWTyknFpDJVTCp3qEwV33Sx1lovcbHWWi9xsdZaL/HDy6hMFVPFScWkMlWcqJxUnKhMFScVk8o3qUwVk8oTKicVd6icVEwqU8WJyh0VU8WkMlVMKlPFScWJyonKVHGi8i9drLXWS1ystdZLXKy11kv88GUV31QxqdyhMlVMFU9UTConFZ9UcYfKScUdFZPKVDGpnKh8ksqJyknFpDJVnKhMFZPKEyonFU+oTBWTyl+6WGutl7hYa62XuFhrrZewXzyg8pcq7lA5qZhUTio+SWWqmFTuqPgklU+qOFGZKiaVk4oTlaniRGWqmFSmihOVqeJEZao4UZkqJpVPqphUpopJZar4pIu11nqJi7XWeomLtdZ6CfvFWmu9wMVaa73ExVprvcTFWmu9xMVaa73ExVprvcTFWmu9xMVaa73ExVprvcTFWmu9xMVaa73ExVprvcTFWmu9xMVaa73ExVprvcTFWmu9xP8B5LHTtRwZPGgAAAAASUVORK5CYII='),
(19, 'client-8748', '', '', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAAE0CAYAAACigc+fAAAAAklEQVR4AewaftIAABbFSURBVO3BQY7Y2JLAQFKo+1+Z42WuHiBI5fbXZIT9wVprfcDFWmt9xMVaa33ExVprfcTFWmt9xMVaa33ExVprfcTFWmt9xMVaa33ExVprfcTFWmt9xMVaa33ExVprfcTFWmt9xMVaa33ExVprfcQPD6n8TRWTyknFpDJVTCp3VJyoTBWTyknFHSonFZPKScWkMlVMKlPFEypTxYnKScWkclJxojJVnKicVJyonFRMKndUTCp/U8UTF2ut9REXa631ERdrrfURP7ys4k0qd1RMKicqv6liUjmpmFSmijsqTiomlUllqphU7lCZKiaVqeJvqphU7lCZKqaKv6niTRVvUnnTxVprfcTFWmt9xMVaa33ED79M5Y6KOyomlTsqJpWTijtU7lB5k8pUcVJxR8UTKlPFicpUcVIxqZyo3FExqTyhMlVMFScqJxVvUrmj4jddrLXWR1ystdZHXKy11kf88D9OZaq4Q2WqmFQmlTsqJpU3qZxUnKhMFZPKScWJylRxovImlScqTlSeULlD5Y6KE5Wp4n/ZxVprfcTFWmt9xMVaa33EDx+jMlVMKndUTCpTxaQyqZxUTConKneo3KFyh8odKm9SmSomlZOKSWVSmSpOKu6omFROKiaVqWJSmSq+7GKttT7iYq21PuJirbU+4odfVvE3VUwqU8WJylQxVTxRMalMFZPKVDGpTBUnKicVk8pUMalMFXeoTBV3qJxU3FFxojJVTCpTxaRyUjGp3KFyR8UTFf+Si7XW+oiLtdb6iIu11vqIH16m8r9EZaqYVKaKSWWqmFT+SypTxaTym1SmijtUpoo7VKaKSWWqmFSmikllqphUpopJ5Y6KSWWqmFROVKaKE5V/2cVaa33ExVprfcTFWmt9hP3B/yMqd1RMKlPFpHJHxaQyVfwmlaniROWk4g6Vk4o7VP6mikllqjhROak4UTmpmFROKv6XXay11kdcrLXWR1ystdZH/PCXqUwVJypTxYnKVPE3VUwqU8WkMlVMKm+quEPlDpUnKp6omFSmikllqphU7qg4UZkqJpVJ5Y6KSWWqmFTuULmjYlI5qXjiYq21PuJirbU+4mKttT7ih4dUpoonVKaKSeWkYlI5qZhU/qaKN1VMKpPKExWTyknFv0TliYo7VO6omFROKu5QuUPlpGJSuaPiTRdrrfURF2ut9REXa631ET/8ZRV3qDxRcaIyVUwqU8UTKlPFpHJHxaQyVZyoTCq/SWWqmFTuqJhUpopJZar4mypOVO5QmSqmiknlROVNKlPFpDJVPHGx1lofcbHWWh9xsdZaH/HDy1Smiknljoo3qUwV/5KKSWWqmFROVKaKk4pJ5QmVqWJS+U0qU8WJylRxojJVTBVPVEwqJypPVDxRcaLymy7WWusjLtZa6yMu1lrrI374ZSpTxaQyVUwqJxWTylRxojJV3KEyVUwVk8qkMlVMFZPKHRWTylQxqdxRMan8popJ5Q6Vk4pJ5aTiRGWqOKmYVKaKO1SmiknlRGWqOFE5qZhU3nSx1lofcbHWWh9xsdZaH/HDyypOVE5UporfVDGpvEnlpGJSmSqmiknlCZWpYlI5UTmpmFSmikllqphU7qh4omJSOVG5Q2WqOFGZKk4q7lC5Q+WkYlKZKt50sdZaH3Gx1lofcbHWWh/xwy9TeULlCZWpYlK5Q+VEZaq4o+JvqjipOKmYVE4qJpUnKiaVJ1TuUDmpeFPFpDJVTCp3VJyo3KEyVUwqU8UTF2ut9REXa631ERdrrfUR9gcvUnmi4kTlpOJE5aTiDpWpYlKZKiaVk4oTlaniROWk4kRlqjhRuaNiUpkqJpWTihOVk4pJZaqYVE4qTlSmihOVqeIJlZOKE5Wp4m+6WGutj7hYa62PuFhrrY+wP3hAZap4QuWJikllqrhDZaqYVJ6omFROKk5U7qiYVE4qJpUnKiaVOyqeUDmpeEJlqphUnqi4Q+U3VUwqU8VvulhrrY+4WGutj7hYa62P+OGXqZxUTBUnKlPFHSpTxaRyonJHxaTyhMpUcVIxqUwqU8WJyknFpHJHxR0qd1Q8oTJVTCpTxUnFicpUMalMFZPKHRUnKlPFScWkclLxxMVaa33ExVprfcTFWmt9xA8PVUwqU8WkMqm8SWWqmFQmlZOKO1SeqDhRmVT+l6lMFZPKExWTylQxqUwqU8WkMlVMKr+p4qRiUrlD5UTljorfdLHWWh9xsdZaH3Gx1lof8cPLKiaVk4r/UsWkcofKicoTKlPFmyomlZOK36TyJpUTlTtUpoo7KiaVk4pJ5aRiUnlTxaQyVUwqk8pU8aaLtdb6iIu11vqIi7XW+gj7g3+YylRxojJVvEllqrhDZao4Ubmj4g6VqWJSeaJiUpkqJpWTijtU3lTxhMpUMan8poonVKaKSWWqmFROKp64WGutj7hYa62PuFhrrY/44R+jMlVMKneoPFFxh8pUcYfKScWJyhMqU8WkMlXcUTGpTBV3qDxRMalMFZPKVHGickfFpDJVTCp3qJxUTCp3VNxR8aaLtdb6iIu11vqIi7XW+ogfXqZyUjGpnKhMFXeonFScqDyhckfFpPKmihOVO1SmijsqTlSmipOKE5VJ5Y6KSeWkYlJ5QmWquENlqphUnlA5qfhNF2ut9REXa631ERdrrfUR9gcvUjmpeJPKScWk8qaKE5U7KiaVJyomlZOKSeU3VdyhckfFpHJHxR0qf1PFHSonFZPKVHGi8kTFExdrrfURF2ut9REXa631ET/8sopJZaqYVE4qpopJZVKZKu5QeaLiTRWTyh0Vf1PFicpUMamcVJyoTBV3qEwVT1TcoTJVTCpPVEwqU8UdFZPK33Sx1lofcbHWWh9xsdZaH2F/8IDKVHGHyknFpDJV/E0qJxUnKlPFpDJVPKHymypOVN5UMamcVPzLVKaKSWWqOFGZKu5QmSpOVO6o+E0Xa631ERdrrfURF2ut9RH2B79IZao4UTmpmFTuqJhU7qiYVKaKSeWkYlI5qZhUTiruUPlNFZPKVHGiMlVMKk9UTConFZPKVHGickfFicr/soonLtZa6yMu1lrrIy7WWusjfnhIZaqYKiaVOyomlTsqJpU7Kt5U8YTKScUdKm+qmFTuUDmpOKm4Q+VvUpkq7lCZKk4q7lCZKiaVqeJE5aTiTRdrrfURF2ut9REXa631EfYHv0hlqrhD5aRiUnmiYlJ5ouJEZaqYVE4qTlT+JRWTyh0Vk8pJxaRyR8WkMlWcqEwVd6hMFW9SmSomlTdVTCpTxRMXa631ERdrrfURF2ut9RH2Bw+onFS8SeVNFXeoTBUnKlPFpDJVPKEyVUwqU8WJylTxJpUnKp5QuaPiROWOiknljooTld9UMalMFScqU8UTF2ut9REXa631ERdrrfUR9gcPqNxRcaJyUnGiMlWcqPymiknlX1YxqUwVk8pUMamcVDyhclIxqfyXKiaVqWJSOal4k8oTFZPKScWbLtZa6yMu1lrrIy7WWusjfnio4g6Vk4oTlZOKSeU3VZyoTBVPqEwVk8pUcaIyqdxRMamcVEwqU8WJyh0qJxVPqEwVk8pJxaQyVdyhMlWcqEwVk8pUMancUfGbLtZa6yMu1lrrIy7WWusj7A8eUDmpmFT+popJ5aTiRGWqOFGZKiaVN1VMKm+qmFR+U8WJylQxqUwVT6hMFZPKVDGpTBUnKndUTCr/kopJZap44mKttT7iYq21PuJirbU+4od/TMUdKlPFpHJSMalMFScq/xKVqeIJlTsq7lC5Q+WOiidU7qiYVKaKE5WpYlJ5omJSmSruUHmi4k0Xa631ERdrrfURF2ut9RE/vKziTSpTxYnKExV3VLyp4kTlpGJSOamYVN6kMlWcVDyhclJxR8WkckfFpDJVnKhMFXdUPKEyVfzLLtZa6yMu1lrrIy7WWusj7A9epDJVnKhMFXeonFQ8ofJExaQyVUwqU8WJylTxJpU7Ku5QuaPib1KZKk5UpopJZap4QuWkYlI5qbhDZaqYVO6oeOJirbU+4mKttT7iYq21PsL+4EUqJxWTypsqJpWTihOVk4pJ5aRiUpkqTlSmikllqphUTipOVP5LFZPKVDGp3FFxh8pUcaIyVUwqd1RMKv+yikllqnjiYq21PuJirbU+4mKttT7ih4dU7lCZKp5QmVSmihOVOyomlaniCZWTijdVnKicVEwqU8UTKr+p4g6VqWJSeVPFExWTyknFEyp3VLzpYq21PuJirbU+4mKttT7C/uABlZOKSeWJiknlpGJSmSomlZOKJ1TeVHGickfFf0llqvgvqTxRMalMFScqJxUnKk9UnKi8qeKJi7XW+oiLtdb6iIu11vqIHx6qmFQmlaliUpkqnqiYVO6omFSeUDmpuENlUnmiYlJ5U8WkMlVMFZPKVHGHyh0VU8WkMlW8SWWq+JsqJpU7KiaVk4o3Xay11kdcrLXWR1ystdZH/PCyijsqTlROKiaVqeIOlROVqWJSmSomlTtUpooTlZOKSWWqOFGZKt6kMlWcqJxUPKEyVdyhcqIyVUwqU8UdFZPKHRWTylRxR8WkMlU8cbHWWh9xsdZaH3Gx1lof8cM/RmWqmFQmlaliUpkqJpWTihOVOyomlZOKSeWkYlK5Q+UJlZOKSeWJiknlDpU3qUwVk8odFZPKHRVTxaRyojJVTCpTxVTxN12stdZHXKy11kdcrLXWR/zwy1ROKqaKSeWJiknlpGJSmSqmiknlRGWqmFQmlaniRGWqmFSmihOVN6ncoXJHxaRyR8WJylTxRMWbKiaVk4qTiidUpoqp4k0Xa631ERdrrfURF2ut9RH2By9SeaLiRGWqmFROKk5UTiomld9UMamcVDyhMlVMKndUnKhMFScqJxUnKlPFHSpTxaRyUjGpnFScqDxR8ZtUTiredLHWWh9xsdZaH3Gx1lof8cNDKlPFm1SmiknlDpWpYqqYVCaVk4pJ5TdVPKFyR8Wk8iaV/5LKVDFV3FExqUwVk8qk8qaKE5U7Ku6omFSmiicu1lrrIy7WWusjLtZa6yPsD/4fUzmpeJPKVHGickfFpDJVPKFyUjGpTBVPqEwVk8pUcYfKVDGpvKliUpkqJpWpYlKZKiaVqeIOlScqftPFWmt9xMVaa33ExVprfcQPD6mcVJyoTBWTyhMVk8pUMamcqJxUnFRMKndUTCqTyr+kYlI5qZhU/qaKSeWk4kTljopJ5UTljoo3VZyoTCpTxZsu1lrrIy7WWusjLtZa6yN+eKhiUjlROVGZKt5UMancUXGHylQxVdyhclJxovIvU7lDZaqYVKaKSeWOihOVJ1ROKiaVqeIJlSdUTiomlaniiYu11vqIi7XW+oiLtdb6CPuDv0hlqjhRmSomlaliUjmpmFTeVDGpnFRMKlPFpDJVTConFZPK31Rxh8odFZPKVDGp3FExqZxUnKjcUXGiclJxh8qbKt50sdZaH3Gx1lofcbHWWh/xw39M5aTiDpWpYlJ5U8WkMqmcVDxRMalMFXdUTConFZPKVHGHylQxVZyo3KEyVUwqJypTxaRyonJHxW9SmSqmiknlpOJvulhrrY+4WGutj7hYa62P+OFlKndUTCqTyhMqU8WkckfFExWTyknFpHJScaJyR8UdFZPKVDGp3KEyVZyoTBV3VDxRcUfFHSonFScqU8WkMlWcVEwqU8WkMlU8cbHWWh9xsdZaH3Gx1lofYX/wi1SmihOVqeIOlTsq7lA5qbhDZaqYVKaKO1SmihOVk4oTlaliUjmpeEJlqphUpoq/SWWqeJPKVDGp3FFxonJHxW+6WGutj7hYa62PuFhrrY+wP3hA5YmKE5WpYlK5o+JEZao4UZkqJpWp4g6Vk4pJZaqYVJ6omFSmit+kMlW8SeWk4kTlN1WcqEwVk8odFScqU8WkclLxpou11vqIi7XW+oiLtdb6iB9eVnGHylQxVdxRcYfKVPG/RGWqmFROKu5QeZPKScVUMalMFScqd1RMKlPFVHGiMlWcqEwqJxWTylQxqZyoTBVTxUnFpDKpTBVPXKy11kdcrLXWR1ystdZH2B+8SGWquEPljooTlaniROWkYlJ5U8WJylTxJpUnKiaVOyomlZOK/5LKVDGpTBUnKndUTCpTxR0qU8WJyh0Vv+lirbU+4mKttT7iYq21PuKHh1SmiknliYoTlZOKSeVNFZPKm1ROVKaKE5Wp4omKSWWqmFSmiknlpGJSuaNiUpkqTlSmikllqphUpoqTiknlDpWTir9JZap408Vaa33ExVprfcTFWmt9hP3Bf0jliYrfpDJV/CaVOyomlZOKSWWqmFSmikllqrhD5aRiUjmpmFSmikllqphUpoonVKaKSeWkYlK5o+JNKm+qeOJirbU+4mKttT7iYq21PsL+4AGVqeIJlaliUjmpmFSmiknlpOIOlZOKSWWqmFSmijepTBUnKlPFpDJVnKjcUTGpTBV3qJxUTCpTxYnKScWJylRxovKmihOVOyp+08Vaa33ExVprfcTFWmt9xA+/TGWqmFROVKaKSWVSeaJiUrmj4k0Vk8pUMalMFZPKicpUcUfFpDJVTBV3qEwVJypvqnii4gmVOypOVKaKJyomlUnlpOKJi7XW+oiLtdb6iIu11voI+4MHVKaK36QyVfwmlaniDpWTikllqjhROak4UTmpOFE5qXhC5YmKJ1ROKiaVN1VMKicVk8pUcYfKVDGpPFHxpou11vqIi7XW+oiLtdb6iB/+MpWTiknlTSonFVPFicpUcVLxhModKm9SuUPliYpJZao4UXmi4jdVTCqTylRxonKi8oTKExWTylTxxMVaa33ExVprfcTFWmt9hP3B/zCVOyruUJkqTlSmihOVk4o7VE4qTlSmihOVqeIOlZOKE5Wp4kRlqphUTiruUPlNFZPKVHGiMlXcoXJHxaQyVTxxsdZaH3Gx1lofcbHWWh/xw0Mqf1PFVDGpTBUnKicVJyp3qEwVd6icVNyhMlWcqNyhMlWcVEwqU8WbVKaKSeUOlaliUjmpmFROVKaKSeUJlanipGJSmVSmijddrLXWR1ystdZHXKy11kf88LKKN6ncUXGiMlVMKndUTCq/qWJSOVGZKqaKE5UnKu5QuaNiUjmpuKNiUpkq7qg4UbmjYlJ5U8UdKv+li7XW+oiLtdb6iIu11voI+4MHVKaKSeWOikllqjhRmSpOVJ6oOFGZKp5QOan4m1TeVDGp3FFxonJHxRMqd1RMKicVk8pUcaLypopJ5Y6KJy7WWusjLtZa6yMu1lrrI374H6cyVZyo3FHxRMWkckfFVDGp3KEyVUwqd1TcoTJVTCpTxR0qJxUnKicqU8WkMlVMKlPFpHJSMamcqEwVU8WJylRxR8Wk8psu1lrrIy7WWusjLtZa6yN++B9XcaIyVdyhMlVMKlPFScUdKneonFRMKicVk8qJylQxVUwqU8UTFZPKicodFZPKicodFb9JZaqYVO5QmSomlaliUnnTxVprfcTFWmt9xMVaa33ED7+s4m9SOVGZKu5QOVE5qXiiYlKZKu6oOFGZKu5QeUJlqphUfpPKScUTKicVk8pUcaJyR8WkMqm8qeJNF2ut9REXa631ERdrrfURP7xM5V9SMancUTGpTBUnKpPKVDGpTBWTym9SeaLiN6mcVLyp4jdVnKicqEwVb6o4UblDZap408Vaa33ExVprfcTFWmt9hP3BWmt9wMVaa33ExVprfcTFWmt9xMVaa33ExVprfcTFWmt9xMVaa33ExVprfcTFWmt9xMVaa33ExVprfcTFWmt9xMVaa33ExVprfcTFWmt9xP8Bmj4yV15FtCsAAAAASUVORK5CYII='),
(20, 'client-8785', '', '', '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAAE0CAYAAACigc+fAAAAAklEQVR4AewaftIAABanSURBVO3BQY7cWhLAQFLo+1+Z42WuHiCoqu2vyQj7g7XWeoGLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeomLtdZ6iYu11nqJi7XWeokfHlL5TRWTyknFicodFScqU8WkMlVMKlPFpDJVTCqfVHGi8kTFpDJVTConFf8lKlPFpPKbKiaV31TxxMVaa73ExVprvcTFWmu9xA8fVvFJKndUnKhMFZPKHSpTxUnFHSonKr9J5Y6KJ1ROKiaVqWJSmSomld9UcVLxL6n4JJVPulhrrZe4WGutl7hYa62X+OHLVO6o+KaKSeUOlaliUpkqTlSmikllqphUpopJ5aTiRGWqOFGZVKaKk4oTlUnlROVE5aTiROWkYlK5Q2WqmFSmikllqvgklTsqvulirbVe4mKttV7iYq21XuKH/7iKO1SmiidUTlSmiqniTSpOVKaKSeWJiknljopJ5Y6KSeWJiicqTlSmiv+yi7XWeomLtdZ6iYu11nqJH/7PVEwqU8WJyknFicodFScVJxWfpPJJKk9UnFTcoXJHxaQyVUwqd6icVEwqJxVvdrHWWi9xsdZaL3Gx1lov8cOXVXyTylQxqUwVT1RMKicqU8Wk8k0qn1QxqUwVd6g8oXJSMalMFScVJypTxSdVnKg8UfFExb/kYq21XuJirbVe4mKttV7ihw9TeROVqeKbKiaVqWJSmSpOKiaVqWJSeUJlqjipmFSmikllqphUpopJZaqYVKaKO1SmikllqphUpoqTiknlRGWqOFH5l12stdZLXKy11ktcrLXWS9gf/IepTBWTyh0VJyrfVHGHylRxojJVTCpTxYnKVHGHylRxojJVTConFZPKVPGEylRxh8onVfw/uVhrrZe4WGutl7hYa62X+OHLVO6omFSmit+kckfFicpUMamcVEwVk8pUMVXcoTJVnKj8TRVPqJxUTCpTxaRyR8UdKlPFpHJSMancUXGickfFExdrrfUSF2ut9RIXa631Ej98WcWkcqIyVUwqU8UdFXdUTCp3qDxRMan8TSonFXeonKicqEwVk8pUcVJxR8VJxR0qd1RMKlPFExUnKicVk8o3Xay11ktcrLXWS1ystdZL/PBhKlPFScWJyh0qT6hMFScVJxWTyhMVk8o3VdyhMlVMKicqJxWTyknFpPKEyknFpPJJFZPKHSpPqEwVT1R80sVaa73ExVprvcTFWmu9hP3BB6ncUXGHyh0Vk8odFScqd1RMKndUTCpTxYnKHRUnKicVJyonFXeoTBWfpHJScaJyUjGpnFR8kspUMancUTGpnFQ8cbHWWi9xsdZaL3Gx1lov8cM/RuWk4g6VqWJSmSpOVKaKO1SmikllqphUpopJ5Y6KSeWOijtUpopJ5URlqpgqJpWTiknlCZU7KiaVqeJE5Y6KSWWqmFSmijtUpopvulhrrZe4WGutl7hYa62XsD94QGWq+CaVqeJE5ZsqnlCZKu5QOamYVKaKSeWJihOVOyomlaliUjmp+E0qU8WkMlVMKlPFHSp3VNyhMlVMKndUPHGx1lovcbHWWi9xsdZaL/HDQxUnKlPFpHJHxYnKVDGpTBWTyjepnKhMFXdUTCpTxUnFEyonFZPKVPGbVD6pYqqYVE5UPqniROVE5aTijopvulhrrZe4WGutl7hYa62XsD94QGWqmFTuqDhRmSpOVJ6omFROKu5QmSomlZOKE5WTijtUpopJZaqYVKaKSeWOijtUpooTlaniCZXfVHGHylQxqZxUTCp3VDxxsdZaL3Gx1lovcbHWWi/xw4epfJLKVDGpnFTcoXJHxaTyhMpUcYfK31RxUjGpnFRMKpPKScVUcaLyhModFU+oTBWTyh0Vk8pJxR0V33Sx1lovcbHWWi9xsdZaL/HDQxWTylQxqdxRcUfFHSpTxRMVJyrfVHGiMqlMFXeoPFFxR8WJyh0qU8WkMqncUXGi8kTFpDJVTCpTxUnFicpU8TddrLXWS1ystdZLXKy11kv88JDKicpUMamcqJxUnKicVNxRcaIyVUwVJyqTylRxh8pUcYfKScWk8oTKScUTKicqU8WJylRxonJSMalMFZPKEypTxaRyUjGpnKhMFZ90sdZaL3Gx1lovcbHWWi9hf/BFKicVn6RyUjGp3FHxhMpJxaQyVUwqJxUnKlPFpDJVPKFyUnGiclIxqUwVd6g8UTGpTBUnKndUfJPKVDGpTBWTyknFExdrrfUSF2ut9RIXa631Ej88pHJSMamcqNxRMVU8UXGHyknFScWkcqJyUvFNKndU3KEyVUwVJypTxR0qJxV3qEwVJypTxaRyonJHxaRyh8pUcVLxTRdrrfUSF2ut9RIXa631EvYHf5HKScWkMlVMKndUnKicVJyoTBWTylRxh8pvqvgklScq7lCZKk5U7qg4UZkqJpVPqrhDZaqYVKaKf8nFWmu9xMVaa73ExVprvcQPH6YyVUwqU8WJylQxqdxRcUfFpDKpPFExqUwVd1RMKlPFb1KZKqaKSeVvUpkqnlB5ouJE5UTlpOJE5UTliYpPulhrrZe4WGutl7hYa62X+OGXVdxRcUfFicpUMancUfE3VUwqJyonFU+ofFPFpDJVnFRMKlPFEyp3qEwVk8pUcVJxonJScaJyR8WJylTxxMVaa73ExVprvcTFWmu9hP3BX6TyRMWkMlXcofJExYnKScWJylRxovJExaQyVUwqU8WkclJxh8pUcaLyRMUdKk9U3KFyR8WkckfFicpJxSddrLXWS1ystdZLXKy11kv88GEqJxVTxR0qJxV3qDxRMamcVEwqd1RMKk9UTCpPVJxUnKj8poo7VJ6oOFGZVKaKOyomlUnlpOJE5Y6Kb7pYa62XuFhrrZe4WGutl/jhwyruUJkqJpUnVKaKk4pJ5UTlpGJS+ZeoTBW/SWWqOFGZKk5UPqliUpkqTlSmipOKOypOKiaVqWJS+SSVqeKTLtZa6yUu1lrrJS7WWuslfvhlKndUTCqTylQxVfymijsqJpWTiqniRGWqmFQmlaliqjhRmSqeUJkqTlSmikllUjmpuEPlk1ROKiaVqWJSOVG5o+IOlUllqnjiYq21XuJirbVe4mKttV7C/uCLVE4qTlSmijtUpooTlTsqTlQ+qeKTVKaKT1KZKr5J5aTiDpWpYlKZKiaVk4pvUrmjYlL5pIpvulhrrZe4WGutl7hYa62X+OEhlanikyomlaliUpkqJpWp4o6KE5U7Kj5J5Y6KSWWqmFROKqaKE5Wp4kRlqpgqTlSmipOKO1Q+SeWk4qTiRGVSOam4Q+U3Xay11ktcrLXWS1ystdZL/PDLKiaVT6qYVE5UvqniRGWqeKLimyqeUJkqTlTuUJkqnlCZKk4qTlROVKaKT1I5qThRmSomlaniRGWqeOJirbVe4mKttV7iYq21XuKHhyq+qWJSeaLiRGWqmFSmijtU7lD5pIo7VKaKE5WTihOVqeKOiidUpopJZao4UZkqJpVPUpkqpopJ5URlqphUpopJ5TddrLXWS1ystdZLXKy11kvYHzyg8i+pmFSeqJhUpopJ5aRiUpkq7lC5o+IOlaliUvmkiknlkyq+SWWqOFGZKj5J5TdV/E0Xa631EhdrrfUSF2ut9RL2Bw+onFScqEwVd6hMFU+oTBUnKlPFicoTFScqJxWTyknFicpUcYfKVPGEyh0Vk8pUcaJyR8WJylTxSSonFXeoTBWTylQxqUwVT1ystdZLXKy11ktcrLXWS/zwUMUdKneoTBUnKk9UPKEyVXySyh0VJxWTyiepTBVPqEwVT6hMFScqU8Wk8kTFpDJVnKhMFVPFpHKiMlWcqJyoTBWfdLHWWi9xsdZaL3Gx1lov8cNDKicVU8WkclJxR8UdKpPKVDGpTBUnKlPFpDJVPKEyqdxR8UkVT6hMFScVk8pUcaIyVUwVk8pUMancoTJVTCpTxVTxSRV3VJyoTCpTxRMXa631EhdrrfUSF2ut9RI//GNUnlCZKk4qJpWTiknlDpUTlTsqfpPKiconVUwqU8UTKlPFN6lMFVPFScWJyhMqT6icVHzTxVprvcTFWmu9xMVaa73EDw9VTConKlPFHSpTxYnKJ6lMFZPKExUnKpPKScU3VZyonFTcUXGiMlXcoTJV3KFyUnGiMlWcqEwVJyonFScqU8UTKlPFExdrrfUSF2ut9RIXa631EvYHD6hMFZPKVHGiclJxojJVnKicVEwqf1PFHSpTxYnKVHGi8kTFHSp3VEwqU8UTKicVk8pUcaJyUvGEyknFicodFd90sdZaL3Gx1lovcbHWWi/xw0MVJxVPVDyhMlWcVJxUTCpTxYnKVPGbVE4qTlTuqDhRuaPiRGVSmSomlanimyr+S1ROKu5QOal44mKttV7iYq21XuJirbVe4ocPU5kqJpWpYqqYVJ6omFSeUJkqTlSmikllqphUTipOKk5U7qiYVO5QmSomlaliUvmkiknlpGKqmFROVKaKT1I5qZgqJpWTiicqvulirbVe4mKttV7iYq21XuKHh1SmiidUpopJ5Q6VqeJEZap4omJSmSomlaliUjlRmSq+qWJSmVSmijtUpoo7VCaVT1KZKiaVqeKTVKaKE5U7Kk5Upoo7VKaKJy7WWuslLtZa6yUu1lrrJX74MpWpYlKZKiaVk4onVKaKSWWqmFSmijtUTlQ+SWWqOFGZKiaVqeJE5aRiUrlDZaq4Q+VEZaqYVE5UpoqTijtUpoqpYlKZKiaVk4pJZar4TRdrrfUSF2ut9RIXa631EvYHH6QyVZyonFRMKk9UTConFZ+kclLxhMpU8YTKN1VMKlPFEypTxR0qd1ScqJxUTCp3VEwqU8VvUjmp+KSLtdZ6iYu11nqJi7XWeokfHlKZKj5J5aTiiYo7VKaKE5U7VO6omComlanijopJ5aRiUjlRmSomlZOKO1SmijsqJpUTlanikyomlTtU7qiYVKaKk4pvulhrrZe4WGutl7hYa62XsD/4IJWp4kRlqjhROamYVKaKSWWqOFGZKj5J5YmKJ1SeqJhU/ssqJpWTikllqphUpopJ5Y6KSeWkYlKZKiaVT6r4pIu11nqJi7XWeomLtdZ6iR8eUnmi4kRlqjhRmSpOKiaVT1I5qTipmFROVJ6oeEJlqjhROak4UTmpuENlUpkqJpU7VKaKOypOVKaKE5UTlaniX3ax1lovcbHWWi9xsdZaL/HDh1VMKlPFicpUMalMFVPFpDJVTCpTxaQyVUwqJxWTyqTyRMWkMlVMKlPFpDJV3FFxR8UdKicVn1QxqTxRcaIyVdxR8UTFicpUcaJyR8UTF2ut9RIXa631EhdrrfUS9gcfpHJSMalMFZPKScWkMlVMKlPFJ6lMFZPKScWJyh0Vk8q/pOJE5aTiCZU7KiaVJyomlTsqJpWp4gmVqeJfdrHWWi9xsdZaL3Gx1lov8cM/ruJE5ZtUnlCZKu5QmSomlanijopJZaqYVKaKSWWqmFQmlaliqphUnlCZKk5UJpWTijtUpoo7VKaKE5U7Kp5QOan4pIu11nqJi7XWeomLtdZ6iR8eUpkqJpVJZao4UZkqpooTlTtUpopJ5aRiUrlD5QmVk4qTiicqJpWpYlI5UfmkihOVqeJE5UTlpGJSmSomlaniRGWqmFSmiknlpGJSmSp+08Vaa73ExVprvcTFWmu9hP3Bf4jKVHGHyt9UMalMFZPKVDGpTBWTylQxqZxUTCpTxYnKVHGHyknFHSpTxW9S+U0Vk8pU8YTKVDGpnFR80sVaa73ExVprvcTFWmu9hP3BAyp3VEwqU8Wk8kTFHSqfVDGpfFLFpHJSMalMFZPKVHGiMlXcofJExaQyVZyoTBWTyh0Vk8pUcaJyUvGEylRxonJS8TddrLXWS1ystdZLXKy11kvYH/wilaniDpWTikllqphUpopJZaqYVO6oOFGZKp5QmSomlZOKSeWOihOVqeJE5aRiUrmj4pNU7qg4UTmpmFROKiaVqeIOlZOKb7pYa62XuFhrrZe4WGutl7A/eEBlqrhDZaqYVE4qJpWTiidUpopJZaqYVL6pYlI5qZhUpoo7VKaKSeWbKiaVv6niDpWp4gmVqeJE5Y6KO1ROKp64WGutl7hYa62XuFhrrZewP3hA5YmKSWWq+E0qJxWTyh0Vd6hMFZPKHRUnKlPFicpUcYfKVHGHyhMVv0nlpOJE5Y6KE5Wp4pNUpopvulhrrZe4WGutl7hYa62X+OGhim9SOam4Q2WqeKJiUjlR+ZtUTiomlZOKO1SmihOVOyomlW9SuaPiROWOin+JyonKScUTF2ut9RIXa631EhdrrfUSP3yYyidVnKhMFZPKN6lMFZ+kckfFpDJVTCqTyknFicoTKlPFicqkclIxqZxUTCpTxR0qJxUnKpPKExWTylQxqUwVU8Wk8psu1lrrJS7WWuslLtZa6yV+eEhlqphUpopJ5UTlpGJSOamYVE4qJpWp4kRlqjhRuaNiUpkqJpVPUrmj4g6Vk4oTlU+qmFROKqaKE5WpYqr4popJZao4UZkqftPFWmu9xMVaa73ExVprvcQPX1ZxUvGEyknFN6ncofJJKlPFExWTyqTymyruUDlRmSqeqJhUTlSmiqliUnmiYlKZKk4qJpWp4kRlqvimi7XWeomLtdZ6iYu11nqJH36ZyknFpHJHxaRyR8UdFXeoTBWTyonK31QxqdyhcofKHRV3qNxRMalMFScVJyp3VEwqJxWTyhMqU8Wk8psu1lrrJS7WWuslLtZa6yXsD/7DVKaKE5WTikllqphUTiomlTsqJpWpYlKZKk5UPqniDpWpYlJ5ouIOlaliUpkqTlSeqDhROamYVE4q7lC5o2JSmSqeuFhrrZe4WGutl7hYa62X+OEhld9UMVWcqNyh8kTFScWkMlVMKt9UMalMFScqJypTxSdVnKjcUXFSMalMFScVd6g8ofKEylRxUnGiMlV80sVaa73ExVprvcTFWmu9xA8fVvFJKk9UnKhMFZPKpHKiclJxonKHyhMq31TxRMWkcqIyVUwqd6hMFXdUTConFd9UMamcVNyhclLxTRdrrfUSF2ut9RIXa631Ej98mcodFXeoTBV3VEwqU8WkMlXcoTJV3KEyVZyonFRMKk+ofJLKicpUcVIxqUwqT6jcUTGpnFTcoXKHyhMVf9PFWmu9xMVaa73ExVprvcQPL6NyR8VUcYfKScVUMak8oTJVTBV3VEwqU8VJxR0qd1RMKneoTBUnKpPKVDGpTBWTyqQyVZyo3FFxR8UnqUwV33Sx1lovcbHWWi9xsdZaL/HDy1VMKneonFQ8UTGpnFScqJxUTConFXeoTBUnFZPKicoTFScqU8WJyonKVHGHylQxqdyhcqJyR8WkMlX8pou11nqJi7XWeomLtdZ6CfuDB1Smik9SmSpOVKaKE5WTihOVqeJE5Y6KJ1Q+qWJSeaJiUjmpOFG5o+JE5YmKSWWqmFROKk5UTiomlaniROWJim+6WGutl7hYa62XuFhrrZf44cNU/qaKSeWOiknlCZWp4gmVk4qpYlKZKu5QOamYVE5UTiomlZOKSWWqOFF5omJSmSruqDhRmSo+SWWqOFE5UZkqPulirbVe4mKttV7iYq21XsL+YK21XuBirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJirbVe4mKttV7iYq21XuJ/9b40H+fVs2EAAAAASUVORK5CYII=');

-- --------------------------------------------------------

--
-- Structure de la table `info_perso`
--

CREATE TABLE `info_perso` (
  `id` int(11) NOT NULL,
  `code_info_perso` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_etudiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` int(11) NOT NULL,
  `phone2` int(11) NOT NULL,
  `phone3` int(11) NOT NULL,
  `statut_matri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_enfant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `info_resocial_client`
--

CREATE TABLE `info_resocial_client` (
  `id` int(11) NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_skype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_skype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_linkedin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_linkedin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_google_plus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_google_plus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_profil_viadeo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_viadeo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `info_resocial_client`
--

INSERT INTO `info_resocial_client` (`id`, `code_client`, `nom_profil_twitter`, `lien_twitter`, `nom_profil_facebook`, `lien_facebook`, `nom_profil_skype`, `lien_skype`, `nom_profil_linkedin`, `lien_linkedin`, `nom_profil_google_plus`, `lien_google_plus`, `nom_profil_instagram`, `lien_instagram`, `nom_profil_viadeo`, `lien_viadeo`) VALUES
(17, 'client-1724', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18, 'client-8748', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19, 'client-8785', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `info_societe_client`
--

CREATE TABLE `info_societe_client` (
  `id` int(11) NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poste` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` int(11) NOT NULL,
  `phone2` int(11) NOT NULL,
  `phone3` int(11) NOT NULL,
  `site_internet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `info_societe_client`
--

INSERT INTO `info_societe_client` (`id`, `code_client`, `nom`, `poste`, `email`, `adresse`, `phone1`, `phone2`, `phone3`, `site_internet`) VALUES
(13, 'client-1724', '', '', '', '', 0, 0, 0, ''),
(14, 'client-8748', '', '', '', '', 0, 0, 0, ''),
(15, 'client-8785', '', '', '', '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `ligne_panier`
--

CREATE TABLE `ligne_panier` (
  `id` int(11) NOT NULL,
  `code_panier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_lp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_modele` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_fichier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite` int(11) NOT NULL,
  `prix` double NOT NULL,
  `nbcarte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ligne_panier`
--

INSERT INTO `ligne_panier` (`id`, `code_panier`, `code_lp`, `code_modele`, `nom`, `type_carte`, `lien_fichier`, `quantite`, `prix`, `nbcarte`) VALUES
(6, 'panier-6114250', 'lignepanier-4476292', 'modele-7080104', 'carte cool', 'carton', 'https://placehold.it/400', 1, 8250, 50),
(7, 'panier-6114250', 'lignepanier-1364534', 'modele-6259036', 'carte cool', 'carton', 'https://placehold.it/400', 1, 10000, 100),
(8, 'panier-6114250', 'lignepanier-8768615', 'modele-6854445', 'carte cool', 'carton', 'https://placehold.it/400', 1, 15000, 250),
(9, 'panier-6114250', 'lignepanier-9549002', 'modele-2585873', 'carte cool', 'pvc', 'https://placehold.it/400', 1, 9750, 50),
(10, 'panier-6114250', 'lignepanier-7200647', 'modele-4339618', 'carte cool', 'pvc', 'https://placehold.it/400', 1, 13000, 100),
(12, 'panier-2721666', 'lignepanier-1432375', 'modele-5952090', 'carte cool', 'pvc', 'https://placehold.it/400', 1, 33500, 500),
(54, 'panier-1927066', 'lignepanier-5903226', 'modele-7080104', 'carte cool', 'carton', 'https://placehold.it/400', 1, 8250, 50),
(55, 'panier-1927066', 'lignepanier-9189521', 'modele-6854445', 'carte cool', 'carton', 'https://placehold.it/400', 1, 15000, 250),
(59, 'panier-5034438', 'lignepanier-5031828', 'modele-7371092', 'carte cool', 'pvc', 'https://placehold.it/400', 1, 21000, 250);

-- --------------------------------------------------------

--
-- Structure de la table `ligne_panier_commande`
--

CREATE TABLE `ligne_panier_commande` (
  `id` int(11) NOT NULL,
  `code_panier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_lp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_modele` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_fichier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite` int(11) NOT NULL,
  `prix` double NOT NULL,
  `nbcarte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191007114020', '2019-10-07 11:40:38');

-- --------------------------------------------------------

--
-- Structure de la table `modele_carte`
--

CREATE TABLE `modele_carte` (
  `id` int(11) NOT NULL,
  `code_modele` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_fichier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `modele_carte`
--

INSERT INTO `modele_carte` (`id`, `code_modele`, `nom`, `type_carte`, `lien_fichier`) VALUES
(18, 'modele-6259036', 'carte carton 0', 'carton', 'https://placehold.it/400'),
(19, 'modele-4339618', 'carte pvc 1', 'pvc', 'https://placehold.it/400'),
(20, 'modele-7080104', 'carte carton 2', 'carton', 'https://placehold.it/400'),
(21, 'modele-2585873', 'carte pvc 3', 'pvc', 'https://placehold.it/400'),
(22, 'modele-6854445', 'carte carton 4', 'carton', 'https://placehold.it/400'),
(23, 'modele-7371092', 'carte pvc 5', 'pvc', 'https://placehold.it/400'),
(24, 'modele-4472099', 'carte carton 6', 'carton', 'https://placehold.it/400'),
(25, 'modele-4433521', 'carte pvc 7', 'pvc', 'https://placehold.it/400'),
(26, 'modele-4748753', 'carte carton 8', 'carton', 'https://placehold.it/400'),
(27, 'modele-5952090', 'carte pvc 9', 'pvc', 'https://placehold.it/400'),
(28, 'modele-3682341', 'carte carton 10', 'carton', 'https://placehold.it/400');

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE `panier` (
  `id` int(11) NOT NULL,
  `code_panier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix_total` double NOT NULL,
  `reduction` double NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`id`, `code_panier`, `prix_total`, `reduction`, `code_client`, `date_register`) VALUES
(25, 'panier-5034438', 21000, 0, 'client-8785', '2019-12-28 14:03:43');

-- --------------------------------------------------------

--
-- Structure de la table `panier_commande`
--

CREATE TABLE `panier_commande` (
  `id` int(11) NOT NULL,
  `code_panier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix_total` double NOT NULL,
  `reduction` double NOT NULL,
  `code_client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `parrain`
--

CREATE TABLE `parrain` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_parrain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hachac` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `parrain`
--

INSERT INTO `parrain` (`id`, `nom`, `prenom`, `code_parrain`, `hachac`) VALUES
(8, '', '', 'client-1724', '63eb0297f0a5f3c19b4c495a88306e665b1b99c3f53ffbe009bb7c6818c9c285b1e9e8f65b0c4bbb0a6846284d591d53cbb71a5066d24109f18876469b26773d'),
(9, '', '', 'client-8748', '15552078b98df8f511942ce258bcc7f9abf896ec768f4af7e00149d84060d17aa98237d76f5c3a74a9d18b5a08e9cf8ec80568107649e3a04c1a1ba2e56cfa1f'),
(10, '', '', 'client-8785', 'c9e0ee2e01fe1c9ce0f0e573b382ecd35879f008036abc54ec9ebf46f461d3a0f97c4028fa1244e8c78fdbac1ecc02dcdfb230fe0a1707818c75cee96a4e9aad');

-- --------------------------------------------------------

--
-- Structure de la table `prestataire`
--

CREATE TABLE `prestataire` (
  `id` int(11) NOT NULL,
  `code_presta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reduction`
--

CREATE TABLE `reduction` (
  `id` int(11) NOT NULL,
  `code_reduction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte_reduction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant_reduction` double NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `root`
--

CREATE TABLE `root` (
  `id` int(11) NOT NULL,
  `code_root` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `root`
--

INSERT INTO `root` (`id`, `code_root`, `nom`, `prenom`, `date_register`) VALUES
(1, 'root-9', 'medi', 'yann michael', '2019-10-17 10:15:08');

-- --------------------------------------------------------

--
-- Structure de la table `tarif_carte`
--

CREATE TABLE `tarif_carte` (
  `id` int(11) NOT NULL,
  `code_tarif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_carte` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_carte` int(11) NOT NULL,
  `tarif_carte` double NOT NULL,
  `tarif_perso` double NOT NULL,
  `date_register` datetime NOT NULL,
  `tarif_pack` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tarif_carte`
--

INSERT INTO `tarif_carte` (`id`, `code_tarif`, `type_carte`, `nb_carte`, `tarif_carte`, `tarif_perso`, `date_register`, `tarif_pack`) VALUES
(17, 'tarif-2565029', 'carton', 50, 65, 5000, '2019-11-16 11:33:49', 8250),
(18, 'tarif-2730151', 'carton', 100, 50, 5000, '2019-11-16 11:33:49', 10000),
(19, 'tarif-8193096', 'carton', 250, 40, 5000, '2019-11-16 11:33:49', 15000),
(20, 'tarif-2433030', 'carton', 500, 35, 5000, '2019-11-16 11:33:49', 22500),
(21, 'tarif-8317822', 'pvc', 50, 75, 6000, '2019-11-16 11:33:49', 9750),
(22, 'tarif-7222448', 'pvc', 100, 70, 6000, '2019-11-16 11:33:49', 13000),
(23, 'tarif-7204073', 'pvc', 250, 60, 6000, '2019-11-16 11:33:49', 21000),
(24, 'tarif-7623904', 'pvc', 500, 55, 6000, '2019-11-16 11:33:49', 33500);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `code_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `code_user`, `username`, `password`, `nom`, `prenom`, `status`, `date_register`) VALUES
(6, 'root-9', 'root@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', 'medi', 'yann michael', 'root', '2019-10-17 10:15:08'),
(28, 'client-1724', 'hyperformix3@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', '', '', 'client', '2019-12-26 14:53:52'),
(29, 'client-8748', 'yannmicke@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', '', '', 'client', '2019-12-26 16:13:59'),
(31, 'client-8785', 'akocedric532@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', '', '', 'client', '2019-12-28 13:48:18'),
(33, 'client-2393', 'christbeda5@gmail.com', '0b9534bc12a76e007fb5506732be897b3152bf206b6357b94c2380d31b45daf7be97daa20661ef6850a5ebc0f899c510cbfb3f774cb52514244e041b9d39ca94', '', '', 'noclient', '2019-12-28 13:53:21');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `attente_validation`
--
ALTER TABLE `attente_validation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bon_de_reduction`
--
ALTER TABLE `bon_de_reduction`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `citation`
--
ALTER TABLE `citation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact_client`
--
ALTER TABLE `contact_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cursus`
--
ALTER TABLE `cursus`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `divers`
--
ALTER TABLE `divers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `filleul`
--
ALTER TABLE `filleul`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_fichier_client`
--
ALTER TABLE `info_fichier_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_perso`
--
ALTER TABLE `info_perso`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_resocial_client`
--
ALTER TABLE `info_resocial_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `info_societe_client`
--
ALTER TABLE `info_societe_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ligne_panier`
--
ALTER TABLE `ligne_panier`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ligne_panier_commande`
--
ALTER TABLE `ligne_panier_commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `modele_carte`
--
ALTER TABLE `modele_carte`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `panier`
--
ALTER TABLE `panier`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `panier_commande`
--
ALTER TABLE `panier_commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `parrain`
--
ALTER TABLE `parrain`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prestataire`
--
ALTER TABLE `prestataire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reduction`
--
ALTER TABLE `reduction`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `root`
--
ALTER TABLE `root`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tarif_carte`
--
ALTER TABLE `tarif_carte`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `attente_validation`
--
ALTER TABLE `attente_validation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `bon_de_reduction`
--
ALTER TABLE `bon_de_reduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `citation`
--
ALTER TABLE `citation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT pour la table `contact_client`
--
ALTER TABLE `contact_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `cursus`
--
ALTER TABLE `cursus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `divers`
--
ALTER TABLE `divers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `experience`
--
ALTER TABLE `experience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `filleul`
--
ALTER TABLE `filleul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `info_fichier_client`
--
ALTER TABLE `info_fichier_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `info_perso`
--
ALTER TABLE `info_perso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `info_resocial_client`
--
ALTER TABLE `info_resocial_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `info_societe_client`
--
ALTER TABLE `info_societe_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `ligne_panier`
--
ALTER TABLE `ligne_panier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT pour la table `ligne_panier_commande`
--
ALTER TABLE `ligne_panier_commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `modele_carte`
--
ALTER TABLE `modele_carte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `panier`
--
ALTER TABLE `panier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `panier_commande`
--
ALTER TABLE `panier_commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `parrain`
--
ALTER TABLE `parrain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `prestataire`
--
ALTER TABLE `prestataire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reduction`
--
ALTER TABLE `reduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `root`
--
ALTER TABLE `root`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tarif_carte`
--
ALTER TABLE `tarif_carte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;