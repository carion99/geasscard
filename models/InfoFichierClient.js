let connection = require('../config/connection')

class InfoFichierClient {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_client'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'info_fichier_client'
	}


	get id () {
		return this.row.id
	}
	get codeClient () {
		return this.row.code_client
	}

	get cheminLogo () {
		return this.row.chemin_logo
	}
	set cheminLogo (cheminLogo) {
		let codeClient = this.codeClient;
		InfoFichierClient.replaceByOneField(codeClient, 'chemin_logo', cheminLogo, (msg) => {
			console.log(msg);
		})
	}

	get cheminPhotoProfil () {
		return this.row.chemin_photo_profil
	}
	set cheminPhotoProfil (cheminPhotoProfil) {
		let codeClient = this.codeClient;
		InfoFichierClient.replaceByOneField(codeClient, 'chemin_photo_profil', cheminPhotoProfil, (msg) => {
			console.log(msg);
		})
	}

	get cheminCv () {
		return this.row.chemin_cv
	}
	set cheminCv (cheminCv) {
		let codeClient = this.codeClient;
		InfoFichierClient.replaceByOneField(codeClient, 'chemin_cv', cheminCv, (msg) => {
			console.log(msg)
		})
    }
    
    get cheminQr () {
		return this.row.chemin_qr
	}
	set cheminQr (cheminQr) {
		let codeClient = this.codeClient;
		InfoFichierClient.replaceByOneField(codeClient, 'chemin_qr', cheminQr, (msg) => {
			console.log(msg)
		})
	}

	static create(content, cb) {
		let ID 		= InfoFichierClient.ID()
		let TABLE 	= InfoFichierClient.TABLE()
		let codeClient 	= content.codeClient
		let cheminLogo 		= content.cheminLogo
		let cheminPhotoProfil  	= content.cheminPhotoProfil
        let cheminCv  	    = content.cheminCv
        let cheminQr  	    = content.cheminQr
        let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, chemin_logo = ?, chemin_photo_profil = ?, chemin_cv = ?,'
        +'chemin_qr = ?' 
		connection.query(sql, [codeClient, cheminLogo, cheminPhotoProfil, cheminCv, cheminQr],
			(err, result) => {
				if (err) throw err
				let msg = "InfoFichierClient bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = InfoFichierClient.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new InfoFichierClient(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = InfoFichierClient.TABLE()
		let ID = InfoFichierClient.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = InfoFichierClient.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new InfoFichierClient(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeClient = row.codeClient
		let TABLE = InfoFichierClient.TABLE()
		let ID = InfoFichierClient.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeClient], (err, result) => {
			if (err) throw err
			let msg = "InfoFichierClient supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = InfoFichierClient