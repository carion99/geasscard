let connection = require('../config/connection')

class Abonnement {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_root'
	}
	/**
	 * Constante contenant le codeClient de la table actuelle
	 */
	static TABLE() {
		return 'root'
	}


	get id () {
		return this.row.id
	}
	get codeAbonnement () {
		return this.row.code_abonnement
	}

	get codeClient () {
		return this.row.code_client
	}

	get typeAbonnement () {
		return this.row.type_abonnement
	}

	get montant () {
		return this.row.montant
	}

	get reduction () {
		return this.row.reduction
    }
    
    get etatAbonnement () {
		return this.row.etat_abonnement
	}

	get cheminFacture () {
		return this.row.chemin_facture
    }
    
    get duree () {
		return this.row.duree
	}

	get dateDebut () {
		return this.row.date_debut
    }
    
    get dateFin () {
		return this.row.date_fin
	}

	get dateRegister () {
		return this.row.date_register
	}

	static genCodeAbonnement() {
		let min = 1000000
		let max = 9999999
		let text= "abonnement-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= Abonnement.ID()
		let TABLE 	= Abonnement.TABLE()
		let codeAbonnement 	= content.codeAbonnement
		let codeClient 		= content.codeClient
		let typeAbonnement  	= content.typeAbonnement
        let montant  = content.montant
        let reduction  = content.reduction
        let etatAbonnement  	= content.etatAbonnement
        let cheminFacture  = content.cheminFacture
        let duree  	    = content.duree
        let dateDebut  = content.dateDebut
		let dateFin  	    = content.dateFin
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_client = ?, type_abonnement = ?, montant = ?, reduction = ?,' 
		+'etat_abonnement = ?, chemin_facture = ?, duree = ?, date_debut = ?, date_fin = ?,  date_register = ?'
		connection.query(sql, [codeAbonnement, codeClient, typeAbonnement, montant, reduction, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "Abonnement bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Abonnement.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Abonnement(row)))
			})
	}

		/**
	 * trouver une ligne de la table
	 * @param  {string}   field1 [premier Champ à viser]
	 * @param  {string or any}   value1 [valeur du premier champ à viser]
	 * @param  {string}   field2 [second Champ à viser]
	 * @param  {string or any}   value2 [valeur du second champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByTwoField(field1, value1, field2, value2, cb) {
		let TABLE = Abonnement.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field1 +' = ? AND '+ field2 +' = ?'
		connection.query(sql, [value1, value2], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Abonnement(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Abonnement.TABLE()
		let ID = Abonnement.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Abonnement.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Abonnement(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeAbonnement = row.codeAbonnement
		let TABLE = Abonnement.TABLE()
		let ID = Abonnement.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeAbonnement], (err, result) => {
			if (err) throw err
			let msg = "Abonnement supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Abonnement