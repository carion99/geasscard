let connection = require('../config/connection')

class Filleul {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_filleul'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'filleul'
	}


	get id () {
		return this.row.id
	}
	get codeFilleul () {
		return this.row.code_filleul
	}

	get codeParrain () {
		return this.row.code_parrain
	}
	set codeParrain (codeParrain) {
		let codeFilleul = this.codeFilleul;
		Filleul.replaceByOneField(codeFilleul, 'nb_filleul', codeParrain, (msg) => {
			console.log(msg);
		})
	}

	get email () {
		return this.row.email
	}
	set email (email) {
		let codeFilleul = this.codeFilleul;
		Root.replaceByOneField(codeFilleul, 'email', email, (msg) => {
			console.log(msg)
		})
	}

	static genCodeFilleul(codeParrain) {
		let min = 1000000
		let max = 9999999
		let text= "filleul-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = codeParrain+'-'+text+nb
  		return code
	}


	static create(content, cb) {
		let ID 		= Filleul.ID()
		let TABLE 	= Filleul.TABLE()
		let codeFilleul = content.codeFilleul
		let codeParrain = content.codeParrain
		let email   = content.email
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_parrain = ?, email = ?' 
		connection.query(sql, [codeFilleul, codeParrain, email],
			(err, result) => {
				console.log(sql);
				
				if (err) throw err
				let msg = "Filleul bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Filleul.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Filleul(row)))
			})
	}

	/**
	 * trouver un nombre de ligne de la table en fonction d'une valeur
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findCountByOneField(field, value, cb) {
		let TABLE = Filleul.TABLE()
		let sql = 'SELECT COUNT(*) AS count FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => row.count))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Filleul.TABLE()
		let ID = Filleul.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Filleul.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Filleul(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeFilleul = row.codeFilleul
		let TABLE = Filleul.TABLE()
		let ID = Filleul.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeFilleul], (err, result) => {
			if (err) throw err
			let msg = "Filleul supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Filleul