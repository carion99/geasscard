let connection = require('../config/connection')

class InfoSocieteClient {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_client'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'info_societe_client'
	}


	get id () {
		return this.row.id
	}
	get codeClient () {
		return this.row.code_client
	}

	get nom () {
		return this.row.nom
	}
	set nom (nom) {
		let codeClient = this.codeClient;
		InfoSocieteClient.replaceByOneField(codeClient, 'nom', nom, (msg) => {
			console.log(msg);
		})
	}

	get poste () {
		return this.row.poste
	}
	set poste (poste) {
		let codeClient = this.codeClient;
		InfoSocieteClient.replaceByOneField(codeClient, 'poste', poste, (msg) => {
			console.log(msg);
		})
	}

	get email () {
		return this.row.email
	}
	set email (email) {
		let codeClient = this.codeClient;
		InfoSocieteClient.replaceByOneField(codeClient, 'email', email, (msg) => {
			console.log(msg)
		})
    }
    
    get adresse () {
		return this.row.adresse
	}
	set adresse (adresse) {
		let codeClient = this.codeClient;
		Client.replaceByOneField(codeClient, 'adresse', adresse, (msg) => {
			console.log(msg);
		})
    }
    
    get phone1 () {
		return this.row.phone1
	}
	set phone1 (phone1) {
		let codeClient = this.codeClient;
		Client.replaceByOneField(codeClient, 'phone1', phone1, (msg) => {
			console.log(msg);
		})
	}
    
    get phone2 () {
		return this.row.phone3
	}
	set phone2 (phone2) {
		let codeClient = this.codeClient;
		Client.replaceByOneField(codeClient, 'phone2', phone2, (msg) => {
			console.log(msg);
		})
	}
    
    get phone3 () {
		return this.row.phone3
	}
	set phone3 (phone3) {
		let codeClient = this.codeClient;
		Client.replaceByOneField(codeClient, 'phone3', phone3, (msg) => {
			console.log(msg);
		})
    }
    
    get siteInternet () {
		return this.row.site_internet
	}
	set siteInternet (siteInternet) {
		let codeClient = this.codeClient;
		Client.replaceByOneField(codeClient, 'site_internet', siteInternet, (msg) => {
			console.log(msg)
		})
    }

	get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		let codeClient = this.codeClient;
		InfoSocieteClient.replaceByOneField(codeClient, 'dateRegister', dateRegister, (msg) => {
			console.log(msg);
		})
	}

	static create(content, cb) {
		let ID 		= InfoSocieteClient.ID()
		let TABLE 	= InfoSocieteClient.TABLE()
		let codeClient 	= content.codeClient
		let nom 	= content.nom
		let poste  	= content.poste
        let email  	= content.email
        let adresse = content.adresse
        let phone1  = content.phone1
        let phone2  = content.phone2
        let phone3  = content.phone3
        let siteInternet= content.siteInternet
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, poste = ?, email = ?, adresse = ?,' 
		+'phone1 = ?, phone2 = ?, phone3 = ?, site_internet = ?'
		connection.query(sql, [codeClient, nom, poste, email, adresse, phone1, phone2, phone3, siteInternet],
			(err, result) => {
				if (err) throw err	
				let msg = "Info Societe Client bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = InfoSocieteClient.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new InfoSocieteClient(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = InfoSocieteClient.TABLE()
		let ID = InfoSocieteClient.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = InfoSocieteClient.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new InfoSocieteClient(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeClient = row.codeClient
		let TABLE = InfoSocieteClient.TABLE()
		let ID = InfoSocieteClient.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeClient], (err, result) => {
			if (err) throw err
			let msg = "InfoSocieteClient supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = InfoSocieteClient