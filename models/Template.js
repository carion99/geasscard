let connection = require('../config/connection')

class Template {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_template'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'template'
	}

	get id () {
		return this.row.id
    }
    
    get codeTemplate () {
		return this.row.code_template
    }

	get nom () {
		return this.row.nom
	}

	get chemin () {
		return this.row.chemin
	}

	get nbUtilisation () {
		return this.row.nb_utilisation
    }
    
    static genCodeTemplate() {
		let min = 100
		let max = 999
		let text= "template-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= Template.ID()
		let TABLE 	= Template.TABLE()
		let codeTemplate = content.codeTemplate
		let nom 	= content.nom
		let chemin  = content.chemin
		let nbUtilisation  = 0
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, chemin = ?, nb_utilisation = ?' 
		connection.query(sql, [codeTemplate, nom, chemin, nbUtilisation],
			(err, result) => {
				if (err) throw err
				let msg = "Template bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Template.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Template(row)))
			})
	}

	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param {string} operation [description operation]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneFieldSupInf(field, operation, value, cb) {
		let TABLE = Template.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' '+operation+' '+' ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Template(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Template.TABLE()
		let ID = Template.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Template.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Template(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeTemplate = row.codeTemplate
		let TABLE = Template.TABLE()
		let ID = Template.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeTemplate], (err, result) => {
			if (err) throw err
			let msg = "Template supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Template