let connection = require('../config/connection')

class ModeleCarte {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_modele'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'modele_carte'
	}


	get id () {
		return this.row.id
	}
	get codeModele () {
		return this.row.code_modele
	}

	get lienFichier () {
		return this.row.lien_fichier
	}

	get nom () {
		return this.row.nom
	}


	get typeCarte () {
		return this.row.type_carte
	}


	static genCodeModele() {
		let min = 1000000
		let max = 9999999
		let text= "modele-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}


	static create(content, cb) {
		let ID 		= ModeleCarte.ID()
		let TABLE 	= ModeleCarte.TABLE()
		let codeModele = content.codeModele
		let lienFichier = content.lienFichier
		let nom		= content.nom
		let typeCarte   = content.typeCarte
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?,'
		+' nom = ?, type_carte = ?, lien_fichier = ?' 
		connection.query(sql, [codeModele, nom, typeCarte, lienFichier],
			(err, result) => {
				console.log(sql);
				
				if (err) throw err
				let msg = "Modele de Carte bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = ModeleCarte.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new ModeleCarte(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = ModeleCarte.TABLE()
		let ID = ModeleCarte.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = ModeleCarte.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new ModeleCarte(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeModele = row.codeModele
		let TABLE = ModeleCarte.TABLE()
		let ID = ModeleCarte.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeModele], (err, result) => {
			if (err) throw err
			let msg = "ModeleCarte supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = ModeleCarte