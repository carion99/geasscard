let connection = require('../config/connection')

class TarifCarte {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_tarif'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'tarif_carte'
	}


	get id () {
		return this.row.id
	}
	get codeTarif () {
		return this.row.code_tarif
	}

	get nbCarte () {
		return this.row.nb_carte
	}
	set nbCarte (nbCarte) {
		let codeTarif = this.codeTarif;
		TarifCarte.replaceByOneField(codeTarif, 'nb_carte', nbCarte, (msg) => {
			console.log(msg);
		})
	}

	get typeCarte () {
		return this.row.type_carte
	}
	set typeCarte (typeCarte) {
		let codeTarif = this.codeTarif;
		Root.replaceByOneField(codeTarif, 'type_carte', typeCarte, (msg) => {
			console.log(msg)
		})
    }
    
    get tarifCarte () {
		return this.row.tarif_carte
	}
	set tarifCarte (tarifCarte) {
		let codeTarif = this.codeTarif;
		Root.replaceByOneField(codeTarif, 'tarif_carte', tarifCarte, (msg) => {
			console.log(msg)
		})
    }
    
    get tarifPerso () {
		return this.row.tarifPerso
	}
	set tarifPerso (tarifPerso) {
		let codeTarif = this.codeTarif;
		Root.replaceByOneField(codeTarif, 'tarif_perso', tarifPerso, (msg) => {
			console.log(msg)
		})
	}
	
	get tarifPack () {
		return this.row.tarif_pack
	}
	set tarifPack (tarifPack) {
		let codeTarif = this.codeTarif;
		Root.replaceByOneField(codeTarif, 'tarif_pack', tarifPack, (msg) => {
			console.log(msg)
		})
    }
    
    get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		let codeRoot = this.codeRoot;
		Root.replaceByOneField(codeRoot, 'dateRegister', dateRegister, (msg) => {
			console.log(msg);
		})
	}

	static genCodeTarif() {
		let min = 1000000
		let max = 9999999
		let text= "tarif-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}


	static create(content, cb) {
		let ID 		= TarifCarte.ID()
		let TABLE 	= TarifCarte.TABLE()
		let codeTarif = content.codeTarif
		let nbCarte = content.nbCarte
        let typeCarte   = content.typeCarte
        let tarifCarte   = content.tarifCarte
		let tarifPerso   = content.tarifPerso
		let tarifPack   = content.tarifPack
        let dateRegister = new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, type_carte = ?,'
		+'nb_carte = ?, tarif_carte = ?, tarif_perso = ?, tarif_pack = ?, date_register = ?' 
		connection.query(sql, [codeTarif, typeCarte, nbCarte, tarifCarte, tarifPerso, tarifPack, dateRegister],
			(err, result) => {
				console.log(sql);
				
				if (err) throw err
				let msg = "Tarif de Carte bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = TarifCarte.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new TarifCarte(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = TarifCarte.TABLE()
		let ID = TarifCarte.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = TarifCarte.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new TarifCarte(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeTarif = row.codeTarif
		let TABLE = TarifCarte.TABLE()
		let ID = TarifCarte.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeTarif], (err, result) => {
			if (err) throw err
			let msg = "TarifCarte supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = TarifCarte