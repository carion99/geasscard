let connection = require('../config/connection')

class InfoResocialClient {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_client'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'info_resocial_client'
	}


	get id () {
		return this.row.id
	}
	get codeClient () {
		return this.row.code_client
	}

	get nomProfilTwitter () {
		return this.row.nom_profil_twitter
	}
	set nomProfilTwitter (nomProfilTwitter) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'nom_profil_twitter', nomProfilTwitter, (msg) => {
			console.log(msg);
		})
	}

	get lienTwitter () {
		return this.row.lien_twitter
	}
	set lienTwitter (lienTwitter) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'lien_twitter', lienTwitter, (msg) => {
			console.log(msg);
		})
    }
    
    get nomProfilViadeo () {
		return this.row.nom_profil_viadeo
	}
	set nomProfilViadeo (nomProfilViadeo) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'nom_profil_viadeo', nomProfilViadeo, (msg) => {
			console.log(msg);
		})
	}

	get lienViadeo () {
		return this.row.lien_viadeo
	}
	set lienViadeo (lienViadeo) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'lien_viadeo', lienViadeo, (msg) => {
			console.log(msg);
		})
    }
    get nomProfilFacebook () {
		return this.row.nom_profil_facebook
	}
	set nomProfilFacebook (nomProfilFacebook) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'nom_profil_facebook', nomProfilFacebook, (msg) => {
			console.log(msg);
		})
	}

	get lienFacebook () {
		return this.row.lien_facebook
	}
	set lienFacebook (lienFacebook) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'lien_facebook', lienFacebook, (msg) => {
			console.log(msg);
		})
    }
    
    get nomProfilSkype () {
		return this.row.nom_profil_skype
	}
	set nomProfilSkype (nomProfilSkype) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'nom_profil_skype', nomProfilSkype, (msg) => {
			console.log(msg);
		})
	}

	get lienSkype () {
		return this.row.lien_skype
	}
	set lienSkype (lienSkype) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'lien_skype', lienSkype, (msg) => {
			console.log(msg);
		})
    }
    
    get nomProfilLinkedin () {
		return this.row.nom_profil_linkedin
	}
	set nomProfilLinkedin (nomProfilLinkedin) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'nom_profil_linkedin', nomProfilLinkedin, (msg) => {
			console.log(msg);
		})
	}

	get lienLinkedin () {
		return this.row.lien_linkedin
	}
	set lienLinkedin (lienLinkedin) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'lien_linkedin', lienLinkedin, (msg) => {
			console.log(msg);
		})
    }
    
    get nomProfilInstagram () {
		return this.row.nom_profil_instagram
	}
	set nomProfilInstagram (nomProfilInstagram) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'nom_profil_instagram', nomProfilInstagram, (msg) => {
			console.log(msg);
		})
	}

	get lienInstagram () {
		return this.row.lien_instagram
	}
	set lienInstagram (lienInstagram) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'lien_instagram', lienInstagram, (msg) => {
			console.log(msg);
		})
    }
    
    get nomProfilGooglePlus () {
		return this.row.nom_profil_google_plus
	}
	set nomProfilGooglePlus (nomProfilGooglePlus) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'nom_profil_google_plus', nomProfilGooglePlus, (msg) => {
			console.log(msg);
		})
	}

	get lienGooglePlus () {
		return this.row.lien_google_plus
	}
	set lienGooglePlus (lienGooglePlus) {
		let codeClient = this.codeClient;
		InfoResocialClient.replaceByOneField(codeClient, 'lien_google_plus', lienGooglePlus, (msg) => {
			console.log(msg);
		})
	}


	static create(content, cb) {
		let ID 		= InfoResocialClient.ID()
		let TABLE 	= InfoResocialClient.TABLE()
		let codeClient 	= content.codeClient
		let nomProfilFacebook= content.nomProfilFacebook
        let lienFacebook= content.lienFacebook
        let nomProfilTwitter= content.nomProfilTwitter
        let lienTwitter= content.lienTwitter
        let nomProfilSkype= content.nomProfilSkype
        let lienSkype= content.lienSkype
        let nomProfilLinkedin= content.nomProfilLinkedin
        let lienLinkedin= content.lienLinkedin
        let nomProfilGooglePlus= content.nomProfilGooglePlus
        let lienGooglePlus= content.lienGooglePlus
        let nomProfilInstagram= content.nomProfilInstagram
        let lienInstagram= content.lienInstagram
        let nomProfilViadeo= content.nomProfilViadeo
		let lienViadeo= content.lienViadeo
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom_profil_facebook = ?, lien_facebook = ?, nom_profil_twitter = ?, lien_twitter = ?,' 
        +' nom_profil_skype = ?, lien_skype = ?, nom_profil_linkedin = ?, lien_linkedin = ?,'
        +' nom_profil_instagram = ?, lien_instagram = ?, nom_profil_viadeo = ?, lien_viadeo = ?,'
        +' nom_profil_google_plus = ?, lien_google_plus = ?'
        connection.query(sql, [codeClient, nomProfilFacebook, lienFacebook, nomProfilTwitter, lienTwitter,
            nomProfilSkype, lienSkype, nomProfilLinkedin, lienLinkedin, nomProfilInstagram, lienInstagram,
            nomProfilViadeo, lienViadeo, nomProfilGooglePlus, lienGooglePlus],
			(err, result) => {
				if (err) throw err
				let msg = "InfoResocialClient bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = InfoResocialClient.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new InfoResocialClient(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = InfoResocialClient.TABLE()
		let ID = InfoResocialClient.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = InfoResocialClient.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new InfoResocialClient(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeClient = row.codeClient
		let TABLE = InfoResocialClient.TABLE()
		let ID = InfoResocialClient.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeClient], (err, result) => {
			if (err) throw err
			let msg = "InfoResocialClient supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = InfoResocialClient