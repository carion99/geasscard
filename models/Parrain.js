let connection = require('../config/connection')

class Parrain {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_parrain'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'parrain'
	}


	get id () {
		return this.row.id
	}
	get codeParrain () {
		return this.row.code_parrain
	}

	get nom () {
		return this.row.nom
	}
	set nom (nom) {
		let codeParrain = this.codeParrain;
		Root.replaceByOneField(codeParrain, 'nom', nom, (msg) => {
			console.log(msg);
		})
	}

	get prenom () {
		return this.row.prenom
	}
	set prenom (prenom) {
		let codeParrain = this.codeParrain;
		Root.replaceByOneField(codeParrain, 'prenom', prenom, (msg) => {
			console.log(msg);
		})
	}

	get hachac () {
		return this.row.hachac
	}
	set hachac (hachac) {
		let codeParrain = this.codeParrain;
		Root.replaceByOneField(codeParrain, 'hachac', hachac, (msg) => {
			console.log(msg);
		})
	}

	static create(content, cb) {
		let ID 		= Parrain.ID()
		let TABLE 	= Parrain.TABLE()
		let codeParrain = content.codeParrain
		let nom 	= content.nom
		let prenom  = content.prenom
		let hachac  = content.hachac
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, prenom = ?, hachac = ?' 
		connection.query(sql, [codeParrain, nom, prenom, hachac],
			(err, result) => {
				if (err) throw err
				let msg = "Parrain bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Parrain.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Parrain(row)))
			})
	}

	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param {string} operation [description operation]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneFieldSupInf(field, operation, value, cb) {
		let TABLE = Parrain.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' '+operation+' '+' ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Parrain(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Parrain.TABLE()
		let ID = Parrain.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Parrain.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Parrain(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeParrain = row.codeParrain
		let TABLE = Parrain.TABLE()
		let ID = Parrain.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeParrain], (err, result) => {
			if (err) throw err
			let msg = "Parrain supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Parrain