let connection = require('../config/connection')

class LignePanierCommande {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_lp_commande'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'ligne_panier_commande'
	}


	get id () {
		return this.row.id
	}
	get codeLpCommande () {
		return this.row.code_lp_commande
	}

	get codeModele () {
		return this.row.code_modele
	}

	get lienFichier () {
		return this.row.lien_fichier
	}

	get nom () {
		return this.row.nom
	}

	get typeCarte () {
		return this.row.type_carte
	}
    
    get nbcarte () {
		return this.row.nbcarte
	}

    
    get quantite () {
		return this.row.quantite
	}

    
    get prix () {
		return this.row.prix
	}


	static genCodeLignePanierCommande() {
		let min = 1000000
		let max = 9999999
		let text= "lignepaniercommande-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}


	static create(content, cb) {
		let ID 		= LignePanierCommande.ID()
		let TABLE 	= LignePanierCommande.TABLE()
        let codeLpCommande = content.codeLpCommande
        let codePanierCommande = content.codePanierCommande
        let codeModele= content.codeModele
		let lienFichier = content.lienFichier
		let nom		= "carte cool" //content.nom
        let typeCarte   = content.typeCarte
        let quantite	= content.quantite
        let nbcarte   = content.nbcarte
        let prix   = content.prix
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_panier_commande = ?,  code_modele = ?, nom = ?, type_carte = ?'
		+',  lien_fichier = ?, quantite = ?, nbcarte = ?, prix = ?' 
		connection.query(sql, [codeLpCommande, codePanierCommande, codeModele, nom, typeCarte, lienFichier, quantite, nbcarte, prix],
			(err, result) => {
				console.log(sql);
				
				if (err) throw err
				let msg = "Une Ligne de PanierCommande bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = LignePanierCommande.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new LignePanierCommande(row)))
			})
	}

	/**
	 * trouver une ligne de la table
	 * @param  {string}   field1 [premier Champ à viser]
	 * @param  {string or any}   value1 [valeur du premier champ à viser]
	 * @param  {string}   field2 [second Champ à viser]
	 * @param  {string or any}   value2 [valeur du second champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByTwoField(field1, value1, field2, value2, cb) {
		let TABLE = LignePanierCommande.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field1 +' = ? AND '+ field2 +' = ?'
		connection.query(sql, [value1, value2], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new LignePanierCommande(row)))
			})
	}

	/**
	 * trouver une ligne de la table
	 * @param  {string}   field1 [premier Champ à viser]
	 * @param  {string or any}   value1 [valeur du premier champ à viser]
	 * @param  {string}   field2 [second Champ à viser]
	 * @param  {string or any}   value2 [valeur du second champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByThreeField(field1, value1, field2, value2, field3, value3,cb) {
		let TABLE = LignePanierCommande.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field1 +' = ? AND '+field2 +' = ? AND '+ field3 +' = ?'
		connection.query(sql, [value1, value2, value3], 
			(err, rows) => {
				console.log(sql);

				if (err) throw err
				cb(rows.map((row) => new LignePanierCommande(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = LignePanierCommande.TABLE()
		let ID = LignePanierCommande.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue1 [valeur de l'identifiant 1, Ex: code_...]
	 * @param  {string}   IdFieldValue2 [valeur de l'identifiant 2, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByTwoField (IdField1, IdFieldValue1, IdField2, IdFieldValue2, field, value, cb) {
		let TABLE = LignePanierCommande.TABLE()
		let ID = LignePanierCommande.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ IdField1 +' = ? AND '+ IdField2 +' = ? '
		connection.query(sql, [value, IdFieldValue1,IdFieldValue2], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue1 [valeur de l'identifiant 1, Ex: code_...]
	 * @param  {string}   IdFieldValue2 [valeur de l'identifiant 2, Ex: code_...]
	 * @param  {string}   IdFieldValue3 [valeur de l'identifiant 3, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByThreeField (IdField1, IdFieldValue1, IdField2, IdFieldValue2, IdField3, IdFieldValue3 , field, value, cb) {
		let TABLE = LignePanierCommande.TABLE()
		let ID = LignePanierCommande.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ IdField1 +' = ? AND '+ IdField2 +' = ? AND '+ IdField3 +' = ? '
		connection.query(sql, [value, IdFieldValue1,IdFieldValue2,IdFieldValue3], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = LignePanierCommande.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new LignePanierCommande(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeLp = row.codeLp
		let TABLE = LignePanierCommande.TABLE()
		let ID = LignePanierCommande.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeLp], (err, result) => {
			if (err) throw err
			let msg = "LignePanierCommande supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = LignePanierCommande