let connection = require('../config/connection')

class MessageContact {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_message'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'message_contact'
	}


	get id () {
		return this.row.id
	}
	get codeMessage () {
		return this.row.code_message
	}

	get nomPrenom () {
		return this.row.nom_prenom
	}

	get destinateur () {
		return this.row.destinateur
	}

	get destinataire () {
		return this.row.destinataire
	}

	get sujet () {
		return this.row.sujet
    }
    
    get message () {
		return this.row.message
	}

	get dateRegister () {
		return this.row.date_register
	}

	static genCodeMessageContact() {
		let min = 0
		let max = 99999999
		let text= "MessageContact-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= MessageContact.ID()
		let TABLE 	= MessageContact.TABLE()
		let codeMessage 	= content.codeMessage
		let nomPrenom 		= content.nomPrenom
		let destinateur  	= content.destinateur
		let destinataire  = content.destinataire
		let sujet  	    = content.sujet
		let message  	    = content.message
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom_prenom = ?, destinateur = ?, destinataire = ?, sujet = ?,' 
		+' message = ?, date_register = ?'
		connection.query(sql, [codeMessage, nomPrenom, destinateur, destinataire, sujet, message, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "MessageContact bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = MessageContact.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new MessageContact(row)))
			})
	}

		/**
	 * trouver une ligne de la table
	 * @param  {string}   field1 [premier Champ à viser]
	 * @param  {string or any}   value1 [valeur du premier champ à viser]
	 * @param  {string}   field2 [second Champ à viser]
	 * @param  {string or any}   value2 [valeur du second champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByTwoField(field1, value1, field2, value2, cb) {
		let TABLE = MessageContact.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field1 +' = ? AND '+ field2 +' = ?'
		connection.query(sql, [value1, value2], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new MessageContact(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = MessageContact.TABLE()
		let ID = MessageContact.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = MessageContact.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new MessageContact(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeMessage = row.codeMessage
		let TABLE = MessageContact.TABLE()
		let ID = MessageContact.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeMessage], (err, result) => {
			if (err) throw err
			let msg = "MessageContact supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = MessageContact