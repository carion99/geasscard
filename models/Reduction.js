let connection = require('../config/connection')

class Reduction {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_reduction'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'reduction'
	}


	get id () {
		return this.row.id
	}
	get codeReduction () {
		return this.row.code_reduction
	}

	get texteReduction () {
		return this.row.texte_reduction
    }
    
    get montantReduction () {
		return this.row.montant_reduction
	}

    
    get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		let codeRoot = this.codeRoot;
		Root.replaceByOneField(codeRoot, 'dateRegister', dateRegister, (msg) => {
			console.log(msg);
		})
	}

	static genCodeReduction() {
		let min = 1000000
		let max = 9999999
		let text= "tarif-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}


	static create(content, cb) {
		let ID 		= Reduction.ID()
		let TABLE 	= Reduction.TABLE()
        let codeReduction = content.codeReduction
		let texteReduction = content.texteReduction
		let montantReduction = content.montantReduction
        let dateRegister = new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, texte_reduction = ?,'
		+'montant_reduction = ?, date_register = ?' 
		connection.query(sql, [codeReduction, texteReduction, montantReduction, dateRegister],
			(err, result) => {
				console.log(sql);
				
				if (err) throw err
				let msg = "Reduction bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Reduction.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Reduction(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Reduction.TABLE()
		let ID = Reduction.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Reduction.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Reduction(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeReduction = row.codeReduction
		let TABLE = Reduction.TABLE()
		let ID = Reduction.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeReduction], (err, result) => {
			if (err) throw err
			let msg = "Reduction supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Reduction