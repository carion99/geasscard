let connection = require('../config/connection')

class ContactClient {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_contact'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'contact_client'
	}


	get id () {
		return this.row.id
    }
    get codeContact () {
		return this.row.code_contact
	}
	get codeClient () {
		return this.row.code_client
	}

	get nom () {
		return this.row.nom
	}
	set nom (nom) {
		let codeContact = this.codeContact;
		ContactClient.replaceByOneField(codeContact, 'nom', nom, (msg) => {
			console.log(msg);
		})
	}

	get prenom () {
		return this.row.prenom
	}
	set prenom (prenom) {
		let codeContact = this.codeContact;
		ContactClient.replaceByOneField(codeContact, 'prenom', prenom, (msg) => {
			console.log(msg);
		})
	}

	get email () {
		return this.row.email
	}
	set email (email) {
		let codeContact = this.codeContact;
		ContactClient.replaceByOneField(codeContact, 'email', email, (msg) => {
			console.log(msg)
		})
    }
    
    get phone () {
		return this.row.phone
	}
	set phone (phone) {
		let codeContact = this.codeContact;
		ContactClient.replaceByOneField(codeContact, 'phone', phone, (msg) => {
			console.log(msg)
		})
    }
    
    get commentaire () {
		return this.row.commentaire
	}
	set commentaire (commentaire) {
		let codeContact = this.codeContact;
		ContactClient.replaceByOneField(codeContact, 'commentaire', commentaire, (msg) => {
			console.log(msg)
		})
	}

	get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		let codeContact = this.codeContact;
		ContactClient.replaceByOneField(codeContact, 'dateRegister', dateRegister, (msg) => {
			console.log(msg);
		})
	}

	static genCodeContact() {
		let min = 1000000
		let max = 9999999
		let text= "ContactClient-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= ContactClient.ID()
		let TABLE 	= ContactClient.TABLE()
		let codeContact = content.codeContact
		let codeClient 	= content.codeClient
		let nom 		= content.nom
		let prenom  	= content.prenom
		let email  	    = content.email
		let phone       = content.phone
		let commentaire = content.commentaire
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_client = ?, nom = ?, prenom = ?, email = ?,' 
		+'phone = ?, commentaire = ?, date_register = ?'
		connection.query(sql, [codeContact, codeClient, nom, prenom, email, phone, commentaire, dateRegister],
			(err, result) => {
				console.log(sql);
				
				if (err) throw err
				let msg = "Contact Client bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = ContactClient.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new ContactClient(row)))
			})
	}

	/**
	 * trouver un nombre de ligne de la table en fonction d'une valeur
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findCountByOneField(field, value, cb) {
		let TABLE = ContactClient.TABLE()
		let sql = 'SELECT COUNT(*) AS count FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => row.count))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = ContactClient.TABLE()
		let ID = ContactClient.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = ContactClient.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new ContactClient(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeContact = row.codeContact
		let TABLE = ContactClient.TABLE()
		let ID = ContactClient.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeContact], (err, result) => {
			if (err) throw err
			let msg = "Contact Client supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = ContactClient