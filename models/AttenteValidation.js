let connection = require('../config/connection')

class AttenteValidation {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_av'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'attente_validation'
	}


	get id () {
		return this.row.id
	}
	get codeAv () {
		return this.row.code_av
	}

	get codeUser () {
		return this.row.code_user
	}

	get hachar () {
		return this.row.hachar
	}
	set hachar (hachar) {
		let codeAv = this.codeAv;
		AttenteValidation.replaceByOneField(codeAv, 'hachar', hachar, (msg) => {
			console.log(msg);
		})
	}

	get status () {
		return this.row.status
	}
	set status (status) {
		let codeAv = this.codeAv;
		AttenteValidation.replaceByOneField(codeAv, 'status', status, (msg) => {
			console.log(msg)
		})
	}

	get dateRegister () {
		return this.row.date_register
	}
	set dateRegister (dateRegister) {
		let codeAv = this.codeAv;
		AttenteValidation.replaceByOneField(codeAv, 'dateRegister', dateRegister, (msg) => {
			console.log(msg);
		})
    }
    
    static genCodeAv() {
		let min = 1000
		let max = 9999
		let text= "av-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= AttenteValidation.ID()
		let TABLE 	= AttenteValidation.TABLE()
		let codeAv 	= content.codeAv
		let codeUser= content.codeUser
		let hachar  = content.hachar
		let status  = content.status
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_user = ?, hachar = ?, status = ?,' 
		+' date_register = ?'
		connection.query(sql, [codeAv, codeUser, hachar, status, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "AttenteValidation bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = AttenteValidation.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new AttenteValidation(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = AttenteValidation.TABLE()
		let ID = AttenteValidation.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = AttenteValidation.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new AttenteValidation(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeAv = row.codeAv
		let TABLE = AttenteValidation.TABLE()
		let ID = AttenteValidation.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeAv], (err, result) => {
			if (err) throw err
			let msg = "AttenteValidation supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = AttenteValidation