let connection = require('../config/connection')

class Historique {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_historique'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'historique'
	}


	get id () {
		return this.row.id
	}
	get codeHistorique () {
		return this.row.code_historique
	}

	get utilisateur () {
		return this.row.utilisateur
	}

	get operation () {
		return this.row.operation
    }
    
    get soperation () {
		return this.row.soperation
    }
    
    get recapitulatif () {
		return this.row.recapitulatif
    }
    
    get dateRegister () {
		return this.row.date_register
	}


	static genCodeHistorique(codeParrain) {
		let min = 1000000000
		let max = 9999999999
		let text= "historique-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = codeParrain+'-'+text+nb
  		return code
	}


	static create(content, cb) {
		let ID 		= Historique.ID()
		let TABLE 	= Historique.TABLE()
		let codeHistorique = content.codeHistorique
		let utilisateur = content.utilisateur
		let operation   = content.operation
        let soperation   = content.soperation
        let recapitulatif = content.recapitulatif
		let dateRegister   = content.dateRegister
        let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, code_historique = ?, utilisateur = ?' 
        +' operation = ?, soperation = ?, recapitulatif = ?, date_register = ?'
		connection.query(sql, [codeHistorique, utilisateur, operation, soperation, recapitulatif, dateRegister],
			(err, result) => {
				console.log(sql);
				
				if (err) throw err
				let msg = "Historique bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Historique.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Historique(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Historique.TABLE()
		let ID = Historique.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Historique.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Historique(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeHistorique = row.codeHistorique
		let TABLE = Historique.TABLE()
		let ID = Historique.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeHistorique], (err, result) => {
			if (err) throw err
			let msg = "Historique supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Historique