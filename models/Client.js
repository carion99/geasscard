let connection = require('../config/connection')

class Client {

	constructor(row) {
		this.row = row
		
	}
	/**
	 * Constante contenant la fictive Clée Primaire de la table
	 */
	static ID() {
		return 'code_client'
	}
	/**
	 * Constante contenant le nom de la table actuelle
	 */
	static TABLE() {
		return 'client'
	}


	get id () {
		return this.row.id
	}
	get codeClient () {
		return this.row.code_client
	}

	get nom () {
		return this.row.nom
	}

	get prenom () {
		return this.row.prenom
	}
	
	get sexe () {
		return this.row.sexe
	}
	
    get dateNaissance () {
		return this.row.date_naissance
	}
    
    get email () {
		return this.row.email
	}

	get adresse () {
		return this.row.adresse
	}
    
    get phone1 () {
		return this.row.phone1
	}
    
    get phone2 () {
		return this.row.phone2
	}
    
    get phone3 () {
		return this.row.phone3
	}

	get siteInternet () {
		return this.row.site_internet
	}
	
	get photo () {
		return this.row.photo
	}
    
    get autoDescription () {
		return this.row.auto_description
	}
    
    get lienCard () {
		return this.row.lien_card
	}
	
	get lienVcard () {
		return this.row.lien_vcard
	}
	
	get lienTemplate () {
		return this.row.lien_template
	}

	get estBoss () {
		return this.row.est_boss
	}
    
    get hashcc () {
		return this.row.hashcc
	}
 
	get dateRegister () {
		return this.row.date_register
	}

	static genCodeClient() {
		let min = 1000
		let max = 9999
		let text= "client-"
		min = Math.ceil(min);
  		max = Math.floor(max);
  		let nb  = Math.floor(Math.random() * (max - min +1)) + min
  		let code = text+nb
  		return code
	}

	static create(content, cb) {
		let ID 		= Client.ID()
		let TABLE 	= Client.TABLE()
		let codeClient 	= content.codeClient
		let nom 		= content.nom
		let prenom  	= content.prenom
		let sexe = content.sexe
        let email  	    = content.email
        let dateNaissance   = content.dateNaissance
        let adresse = content.adresse
        let phone1  = content.phone1
        let phone2  = content.phone2
        let phone3  = content.phone3
        let siteInternet= content.siteInternet
        //let photo  = content.photo
        let photo  = ""
        let autoDescription = content.autoDescription
        let lienCard= content.lienCard
		let lienVcard= ""
		let lienTemplate = content.lienTemplate
		let estBoss = 0
        let hashcc  = content.hashcc
		let dateRegister= new Date()
		let sql = 'INSERT INTO '+ TABLE +' SET '+ ID +' = ?, nom = ?, prenom = ?, sexe = ?, email = ?, date_naissance = ?, adresse = ?,'
		+'phone1 = ?, phone2 = ?, phone3 = ?, site_internet = ?,  photo = ?, auto_description = ?, lien_card = ?, lien_vcard = ?, lien_template = ?, est_boss = ?, hashcc = ?,  date_register = ?'
        connection.query(sql, [codeClient, nom, prenom, sexe, email, dateNaissance, adresse, phone1, phone2, phone3, 
            siteInternet, photo, autoDescription, lienCard, lienVcard, lienTemplate, estBoss, hashcc, dateRegister],
			(err, result) => {
				if (err) throw err
				let msg = "Client bien ajouté"
				cb(msg)
			})

	}


	/**
	 * trouver une ligne de la table
	 * @param  {string}   field [Champ à viser]
	 * @param  {string or any}   value [valeur du champ à viser]
	 * @param  {Function} cb    [CallBack]
	 * @return {Object Array}         [ligne de la table]
	 */
	static findByOneField(field, value, cb) {
		let TABLE = Client.TABLE()
		let sql = 'SELECT * FROM '+ TABLE +' WHERE '+ field +' = ?'
		connection.query(sql, [value], 
			(err, rows) => {
				if (err) throw err
				cb(rows.map((row) => new Client(row)))
			})
	}

	/**
	 * remplacement d'une valeur d'un champ de la table
	 * @param  {string}   IdFieldValue [valeur de l'identifiant, Ex: code_...]
	 * @param  {string}   field   [champ a viser]
	 * @param  {string or any}   value   [valeur du champ à viser]
	 * @param  {Function} cb      [callback]
	 * @return {void}           [confirmation]
	 */
	static replaceByOneField (IdFieldValue, field, value, cb) {
		let TABLE = Client.TABLE()
		let ID = Client.ID()
		let sql = 'UPDATE '+ TABLE +' SET '+ field +' = ? WHERE '+ ID +' = ?'
		connection.query(sql, [value, IdFieldValue], 
			(err, results) => {
				if (err) throw err
				let msg = "mise à jour effectuée avec succès..."
				cb(msg)
			})
	}

	/**
	 * retourner tout le contenu de la table
	 * @param  {Function} cb [callback]
	 * @return {array}      [tableau contenant les lignes de la table]
	 */
	static all(cb) {
		let TABLE = Client.TABLE();
		connection.query('SELECT * FROM '+ TABLE +' ', (err, rows) => {
			if (err) throw err
			cb(rows.map((row) => new Client(row)))
		})
		//connection.end()
	}

	/**
	 * supprimer une ligne de la table
	 * @param  {Object}   row [Objet à viser]
	 * @param  {Function} cb  [CallBack]
	 * @return {void}       [confirmation]
	 */
	static remove(row, cb) {
		let codeClient = row.codeClient
		let TABLE = Client.TABLE()
		let ID = Client.ID()
		let sql = 'DELETE FROM '+ TABLE +' WHERE '+ ID +' = ?'
		connection.query(sql, [codeClient], (err, result) => {
			if (err) throw err
			let msg = "Client supprimé avec succès..."
			cb(msg)
		})
	}
}

module.exports = Client