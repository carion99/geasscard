const express = require('express')
const router = express.Router()

const indexRouter = require('../routes/index')
const usersRouter = require('../routes/users')
const testRouter = require('../routes/test')
const testApiRouter = require('../routes/testapi')
const test1Router = require('../routes/test1')
const loginRouter = require('../routes/security/login')
const logoutRouter = require('../routes/security/logout')
const registerClientRouter = require('../routes/security/register_client')
const registerPrestaRouter = require('../routes/security/register_presta')
const registerSupervisorRouter = require('../routes/security/register_supervisor')
const pforgotRouter = require('../routes/security/passforgot')
const majpassRouter = require('../routes/security/majpass')
const confirmail = require('../routes/security/confirmail')

const index_portfolio = require('../routes/portfolio/index')

router.use('/', indexRouter)
router.use('/users', usersRouter)
router.use('/test', testRouter)
router.use('/testapi', testApiRouter)
router.use('/test1', test1Router)
router.use('/login', loginRouter)
router.use('/logout', logoutRouter)
router.use('/register', registerClientRouter)
router.use('/register_supervisor', registerSupervisorRouter)
router.use('/prestaregister', registerPrestaRouter)
router.use('/passforgot', pforgotRouter)
router.use('/majpass', majpassRouter)
router.use('/confirm', confirmail)

//Affichage Portfolio
router.use('/portfolio', index_portfolio)

module.exports = router