const express = require('express')
const router = express.Router()

const index_client = require('../routes/client/index_client')
const gestion_data = require('../routes/client/gestion_data')
const gestion_contact = require('../routes/client/gestion_contact')
const gestion_filleul = require('../routes/client/gestion_filleul')
const passcommand = require('../routes/client/passcommand')

router.use('/', index_client)
router.use('/gestion_data', gestion_data)
router.use('/gestion_contact', gestion_contact)
router.use('/gestion_filleul', gestion_filleul)
router.use('/passcommand', passcommand)

module.exports = router