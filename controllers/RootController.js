const express = require('express')
const router = express.Router()

const index_root = require('../routes/root/index_root')
const gestion_commande = require('../routes/root/gestion_commande')
const gestion_reduction = require('../routes/root/gestion_reduction')
const gestion_superviseur = require('../routes/root/gestion_superviseur')
const gestion_client = require('../routes/root/gestion_client')
const gestion_prestataire = require('../routes/root/gestion_prestataire')
const gestion_parrainage = require('../routes/root/gestion_parrainage')
const configuration_systeme = require('../routes/root/configuration_systeme')
const historique_systeme = require('../routes/root/historique_systeme')

router.use('/', index_root)
router.use('/gestion_commande', gestion_commande)
router.use('/gestion_reduction', gestion_reduction)
router.use('/gestion_superviseur', gestion_superviseur)
router.use('/gestion_client', gestion_client)
router.use('/gestion_prestataire', gestion_prestataire)
router.use('/gestion_parrainage', gestion_parrainage)
router.use('/configuration_systeme', configuration_systeme)
router.use('/historique_systeme', historique_systeme)

module.exports = router