const express = require('express')
const router = express.Router()

const index_presta = require('../routes/presta/index_presta')
const presta_commande = require('../routes/presta/gestion_commande')
const hist_commande = require('../routes/presta/hist_commande')

router.use('/', index_presta)
router.use('/gestion_commande', presta_commande)
router.use('/hist_commande', hist_commande)

module.exports = router