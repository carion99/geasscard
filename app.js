const createError = require('http-errors')
const express = require('express')
const session = require('express-session')
const redis = require('redis')
const redisStore = require('connect-redis')(session)
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const bodyParser = require('body-parser')
//const upload = require('express-fileupload')
//const multer = require('multer')






const MainController = require('./controllers/MainController')
const ClientController = require('./controllers/ClientController')
const RootController = require('./controllers/RootController')
const PrestaController = require('./controllers/PrestaController')





const User = require('./models/User')


const app = express()

// view engine setup
app.engine('ejs', require('express-ejs-extend'))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

/*
	DECLARATION DES MIDDLESWARES
 */

const age = 3600000 * 3

const secret_dev = 'yannmicke080521'
const secret_prod = 'esti290322'


const client_dev = redis.createClient()
/*
const client_prod = redis.createClient({
    port: 30749,
    host: 'ec2-52-87-17-172.compute-1.amazonaws.com',
    password: 'p72adb8b2e752f6202c20be5a296bc9d31899534c1b588cb2b01991da4d83f619'
});
*/
//const client = require('./config/env').redisEnv()
//const secret = require('./config/env').redisSecret()
const client = client_dev
const secret = secret_dev


//app.use(upload())
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(cookieParser())

app.use(session({
    secret: secret,
    // create new redis store.
    //store: new redisStore({ host: 'localhost', port: 6379, client: client,ttl : 260}),
    store: new redisStore({
        client: client,
        ttl: 260
    }),
    saveUninitialized: true,
    resave: true,
    cookie: {
        secure: false,
        maxAge: age
    }
}));
app.use(express.static(path.join(__dirname, 'public')))
//app.use(upload)

// ROUTAGE GLOBALE APPLICATION

app.use(
    '/',
    (req, res, next) => {
        console.log("_______________________secure main ok________________________")
        next()
    },MainController
)


//Compte Client

app.use(
    '/compte/client', 
    (req, res, next) => {
        let sess = req.session
        if (sess.codeUser && sess.status === "client") {
            User.findByOneField('code_user', sess.codeUser, (futuser) => {
                if (futuser.length == 1) {
                    let user = futuser[0]
                    if (user.autorisation == 1) {
                        console.log("_______________________secure client ok________________________")
                        next()
                    } else {
                        res.redirect('/logout')
                    }
                } else {
                    res.redirect('/logout')
                }
            })
        } else {
            res.redirect('/')
        }
    },ClientController
)


//Compte Root

app.use(
    '/compte/root', 
    (req, res, next) => {
        let sess = req.session
        if (sess.codeUser && sess.status === "root") {
            console.log("_______________________secure root ok_______________________")
            next()
        } else {
            res.redirect('/')
        }
    },  RootController
)



//Compte Prestataire

app.use(
    '/compte/presta', 
    (req, res, next) => {
        let sess = req.session
        if (sess.codeUser && sess.status === "presta") {
            console.log("_______________________secure presta ok_______________________")
            next()
        } else {
            res.redirect('/')
        }
    },  PrestaController
)





// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.render('errore')
})

module.exports = app